#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import breakpoint
from gdbvalue2native import gdb_value_to_native_python, gdb_get_args

from property import RuntimeEvent, CallEvent
from functools import partial
import execution
import gdb

def is_before(event):
    return (not isinstance(event, CallEvent)) or event.before

def val_to_expected_val(p, val):
    if p.pointer:
        val = val.dereference()
    elif p.ref:
        val = val.address

    return gdb_value_to_native_python(val, p.explicit_type)


class EventManager:
    def __init__(self):
        self.bp_by_before_and_evt_name  = {True:{}, False:{}}
        self.evt_by_before_and_evt_name = {True:{}, False:{}}
        self.checkpoints = {}
        self.monitor = None

    def set_callback(self, callback):
        self.callback = callback

    def set_monitor(self, monitor):
        self.monitor = monitor

    def enable_breakpoint(self, event, force=False):
        beforeentry = self.bp_by_before_and_evt_name[is_before(event)]

        if event.name in beforeentry:
            if force:
                beforeentry[event.name].disable()
                beforeentry[event.name] = breakpoint.on(event, self.trigger_breakpoint)
            else:
                breakpoint.enable(beforeentry[event.name])
        else:
            beforeentry[event.name] = breakpoint.on(event, self.trigger_breakpoint)

    def watch(self, event, force=False):
        self.enable_breakpoint(event, force)

        beforeentry = self.evt_by_before_and_evt_name[is_before(event)]
        try:
            b = beforeentry[event.name]
        except KeyError:
            b = set()
            beforeentry[event.name] = b

        b.add(event)

    def unwatch(self, event):
        l = self.evt_by_before_and_evt_name[is_before(event)][event.name]
        l.remove(event)

        if not l:
            breakpoint.disable(self.bp_by_before_and_evt_name[is_before(event)][event.name])

    def handle_event_parameters(self, event, parameters, evt_name, b=None, args=None):
        re = RuntimeEvent(event)
        return_val = None

        for p in event.parameters:
            val = None

            if p.py_value:
                val = eval(p.py_value, self.monitor.root_slice.get_env_copy())
            else:
                try:
                    val = parameters[p.c_value]
                except KeyError:
                    if isinstance(p.c_value, int):
                        try:
                            if not args:
                                args = gdb_get_args()

                            val = args[p.c_value - 1]
                        except Exception as exc:
                            print("Could not get argument " + p.name + " (position " + str(p.c_value) + ") for event " + evt_name, args)
                            raise exc
                    else:
                        val = gdb.parse_and_eval(p.c_value)

                    val = val_to_expected_val(p, val)
                    parameters[p.c_value] = val

            re.parameters[p.name] = val

        if event.return_var:
            if return_val is None:
                return_val = val_to_expected_val(event.return_var, b.get_return_value())
            re.parameters[event.return_var.name] = return_val

        return self.callback(re)


    def trigger_breakpoint(self, b):
        stop = execution.CONTINUE
        before = b.event_before
        evt_name = b.event_name
        events  = set(self.evt_by_before_and_evt_name[before][evt_name])
        parameters = dict()
        args = []

        for event in events:
            stop = breakpoint.combine_stops(stop, self.handle_event_parameters(event, parameters, evt_name, b, args))

        return stop

    def get_event_list(self):
        l = []

        for b in (True, False):
            bentry = self.evt_by_before_and_evt_name[b]

            for (name, eventList) in bentry.items():
                l += eventList
        return l

    def checkpoint(self, cid):
        self.checkpoints[cid] = self.get_event_list()

    def restore_instrumentation(self):
        pass

    def disable_bps(self):
        for b in (True, False):
            bentry = self.bp_by_before_and_evt_name[b]

            for (name, bp) in bentry.items():
                try:
                    breakpoint.disable(bp)
                except:
                    pass

        #self.bp_by_before_and_evt_name = {True:{}, False:{}}

    def restore_checkpoint(self, cid):
        self.disable_bps()
        self.evt_by_before_and_evt_name = {True:{}, False:{}}

        checkpoint = self.checkpoints[cid]

        for event in checkpoint:
            self.watch(event, force=True)

    def drop_checkpoint(self, cid):
        del self.checkpoints[cid]

    def delete(self):
        self.callback = None
