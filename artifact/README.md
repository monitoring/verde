This folder provides a way to play with Verde and run the experiments presented in the paper ''Interactive Runtime Verification''.

We present how to set up the artifact, and then we present how to run it and where to find the results.

Do not hesitate to try the [Bad-Iterator tutorial](#running-bad-iterator) if you are not yet familiar with Interactive Runtime Verification.

# Setting up the artifact

There are two ways to run use this artifact:
 - With Docker (recommended method). Everything is run from a fully controlled environment. The downside is that Docker must be set up, but this is the most robust way to do this.
 - Manually. Dependencies must be installed and Verde will be run directly.

The following instructions assume a Unix-like system. It is best to run the artifact in a Linux system, as Docker in other system might add a layer of virtualization that is not desirable for benchmarking.

In the following instructions, when a line begins by `sudo`, it is assumed that what follows is run as root.

## Using the Docker Container

Install Docker. It is best to follow the [https://docs.docker.com/engine/installation/](official installation instructions from the Docker website) and not use the package shipped by the distribution if not explicitly stated in these instructions, as packages might not be up to date and might behave differently than what is expected here.

NOTE: CRIU (used for checkpointing) does not seem to work inside the Docker container.

If not done already, clone (or download) the Verde repository:

    git clone https://gitlab.inria.fr/monitoring/verde.git
    git checkout [...]
    cd verde

In the directory `artifact/docker-folder`, build the Docker image (make sure Docker is running):

    cd artifact/docker-folder
    sudo make build # (or simply make build, if using Mac or a distro that does not require root to run Docker)

Building the Docker image the first time takes time because it downloads Ubuntu and compiles CRIU and wget. Simple instructions are in the `Dockerfile` to avoid compiling wget, reducing the Docker container building time.
Be aware that not compiling wget will make the wget experiment fail.

It is also possible to patch and compile gdb so native gdb checkpoints can be used with Verde. Verde uses CRIU for checkpoints by default so this is not mandatory. See the Dockerfile to enable gdb patching when building the container.

Then, run the Docker image:

 - On Mac (see the note for Mac users bellow):

        make run-mac

 - On GNU/Linux:

        sudo make run # (or make run-su on some systems. Requires that a root password is set)

To ensure there are no problems with permission or with previous runs with the other methods, run:

    sudo ./exp.sh clean

The folder `files`, which contains Verde and the experiments, is mounted on `/root/eval`.

See Section [Running the artifact](#running-the-artifact) for more information on how to run the experiments.

### **Note for Mac Users**

On Mac, it is possible to get the graphical view using XQuartz.

 1. Install XQuartz ([https://www.xquartz.org/](https://www.xquartz.org/)).
 1. Install socat, for example using Homebrew ([https://brew.sh/](https://brew.sh/)):

        brew install socat

 1. Then run the Docker container.

        make run-mac

   Errors about libGL can be safely ignored:

        libGL error: No matching fbConfigs or visuals found
        libGL error: failed to load driver: swrast

Caveats:
 - The machine must be connected to a network and use an IPv4 address. Alternatively, you can try to set the MACHINE_IP environment variable when running `make run-mac`:

        MACHINE_IP="xxx.xxx.xxx.xxx" make run-mac

    Setting MACHINE_IP to an IPv6 address may work (this is untested).
 - Windows may appear with a delay. A possible workaround is to re-run experiments that show a window a second time.

## Direct Install

Instruction are provided for Debian-like distributions. Alternatively, the commands given in the Dockerfile can also be used. Be aware that Debian and several other distributions, in contrast with the latest versions of Ubuntu, still use Python 2 inside GDB. This is not well tested but efforts have been made to support this in the latest versions of Verde.

    # Make sure your repositories are up to date
    sudo apt update

    # The base
    sudo apt install --no-install-recommends make gcc gdb git wget libc6-dev bc libbsd-dev

    # If you want to be able to show a graph of properties in Verde:
    sudo apt install --no-install-recommends graphviz python3-pip python3-pyqt5 python3-pyqt5.qtwebkit
    sudo pip3 install graphviz

    # If you want to be able to generate benchmark graphs:
    sudo apt install --no-install-recommends r-base r-cran-ggplot2 r-cran-plyr

    # If you want to be able to generate a PDF from them:
    sudo apt install --no-install-recommends inkscape

    # Fix GDB output
    echo "set height 0" >> ~/.gdbinit
    echo "set width 0" >> ~/.gdbinit

    # If you have not downloaded Verde yet:
    git clone https://gitlab.inria.fr/monitoring/verde.git
    cd verde

If you want to try the producer consumer example directly:

    cd examples/prod_cons/
    make
    cd ../../
    gdb examples/prod_cons/prod_cons

    #In GDB:
    source gdbcommands.py
    verde load-property examples/prod_cons/queue-size.prop examples/prod_cons/functions-monitor.py
    verde show-graph
    verde run-with-program

In order to use checkpointing, [install CRIU](https://criu.org/Main_Page) (see the Dockerfile for instructions on how to install criu on Ubuntu).

### Run

    cd eval/src/latest
    ./build-script.sh
    cd ../..
    cp exp-latest.sh exp.sh
    ./exp.sh

See Section [Running the artifact](#running-the-artifact) for more information on how to run the experiments.

# Running the artifact

The folder `eval` contains the script `exp.sh`. It contains all the available experiments, some of which are described more in details in the publications in the dedicated section.

Running it without parameter prints the help message:

    cd eval # if the eval folder is not the current folder yet
    ./exp.sh

A run is parameterized by an experiment name, as described in the following:

## Running the producer-consumer experiment

This experiment runs a producer-consumer program with Verde checking the
property that a queue should not overflow / underflow is violated.
This program fills and empties a queue, but mutexes are misused.
The violation can be observed and the program inspected.

    ./exp.sh prod_cons

## Running the micro-benchmark
This experiment measures the overhead of the instrumentation in function of the temporal gap between two breakpoints.

    ./exp.sh microbench_light # A version that computes a shortened version of the graph. (the run takes a time around two minutes)
    ./exp.sh microbench # To generate the graph as in the paper (the run should take 10 to 20 minutes)

Results (including a PDF of the graph) are generated in `eval/exp/results/microbench(_light)/**<host>**/**<date>**` and the graph is generated in the folder `graph`.

Be aware that any kind of virtualization may affect performance and that different computers will lead to different results.

## Running the wget (Downloading a file) experiment
This experiment measures the impact of monitoring calls to function `read` with Verde when downloading a file with wget.

    ./exp.sh wget

Results are printed and recorded in `eval/exp/results/wget/**<host>**/**<date>**`.

This experiment takes around 30 seconds to run.

## Running the bpcost (Overhead of a breakpoint) experiment

This experiment measures the time of handling a breakpoint in GDB. Verde is not involved.

    ./exp.sh bpcost

The result (the time of a breakpoint in microseconds) is printed and recorded in `eval/exp/results/bpcost/**<host>**/**<date>**/result.txt`.

This experiment takes around 10 seconds to run.

## Running the dyn (Dynamic instrumentation) experiment

This experiment measures the potential improvement brought by dynamic instrumentation in Verde, in contrast with static instrumentation.

    ./exp.sh dyn

The results are printed. First, the execution time in seconds when the instrumentation is dynamic is given, then the execution time when the instrumentation is simulated as non dynamic.

This experiment takes less than 5 seconds to run.

## Running the cpfg experiment (demonstrates checkpointing)

    ./exp.sh cpfg

A program calls a function `f` and then calls a function `g`.

The execution of the program reaches the call to function `f`, then a checkpoint is taken, then the  execution reaches the call to function `g`, and then goes back to before the call to function `f`. The state of the monitor is consistent during the whole execution. If the execution is not modified, the program will execute in the same way as before restarting the checkpoint.

The user can play with the debugger, for example by issuing `continue`.

The code of each experiment can be explored in the folder `eval/verde-exp/**<experiment_set>**/tmp/**<experiment_name>**/` after a run (or after calling `./<experiment_set>.sh prepare`). The entry point is `run.sh`.
The code of the experiment can be changed and run again with `./<experiment_set>.sh <experiment_name>`.

The code of Verde, as soon as an experiment that needs it is run or prepared, can be found in `eval/verde-exp/**<experiment_set>**/tmp/verde/`. This folder is a symbolic link to one of the version of Verde located in the folder `verde`.

## Running Bad-Iterator

Bad-Iterator is a short tutorial to try Verde and i-RV. See [../README.md: Try i-RV on a Simple Example](../README.md#try-i-rv-on-a-simple-example) for more information.

    ./exp.sh baditer

## Exploring Verde

The latest version of Verde is in the `verde/verde-HEAD folder`.

# Provided Results

We ran the `microbench`, `bpcost` and `dyn` experiments on the machine presented in the Micro-Benchmark experiment of the paper, on Ubuntu Xenial with Docker and manually and on Ubuntu Trusty in a chroot.

On Ubuntu Xenial, the time to handle a breakpoint is slower. This is probably due to a regression in GDB, as the kernel that is used is the same. Results on Ubuntu Xenial correspond to the results that are presented in the paper.

These results can be found in the folder `results`. See "Running the artifact" for more information on where the results are located for each experiment.
