#!/usr/bin/env sh

anonymize_this() {
    if [ ! -z "${ANON_NAME}" ]; then
        cp "$WD/../../anonymize.sh" "$WD/../../anonymize.config.sh" .
        chmod +x ./anonymize.sh ./anonymize.config.sh
        ./anonymize.sh
    fi
}

set -e

cd "$(dirname "$0")/docker-folder"
WD=$(pwd)

DEST="${WD}/files"
if [ "$1" != "" ]; then
	DEST="$1"
	echo $DEST
fi

## UNCOMMENT THE FOLLOWING LINE TO ANONYMIZE
# . "$WD/../../anonymize.config.sh"

if [ -z "${ANON_NAME}" ]; then
	TOOL_NAME="verde"
else
	TOOL_NAME="${ANON_NAME}"
fi

get_commit_from_eval() {
    fgrep '_COMMIT="' intro.sh | cut -d'"' -f 2
}

clonein() {
    if [ ! -d "$1" ]; then
        git clone "$WD/../.." "$1"
        cd "$1"
        git reset --hard "$2"
        anonymize_this
        rm -rf eval docker-artifact
    fi
}

rm -rf "${DEST}/"*
mkdir -p "${DEST}"

cp -r "../../eval" "${DEST}"

cd "${DEST}/eval/src"

# for i in *; do
for i in latest; do
    cd "$i"
    COMMIT=$(get_commit_from_eval)
    anonymize_this
    ./build-script.sh
    clonein "${DEST}/${TOOL_NAME}/${TOOL_NAME}-${COMMIT}" "${COMMIT}"
#     rm -rf "${DEST}/${TOOL_NAME}/${TOOL_NAME}-${COMMIT}/doc"
    mv "${DEST}/eval/exp-latest.sh" "${DEST}/eval/exp.sh"
    cd "${DEST}/eval/src"
done

# clonein "${DEST}/${TOOL_NAME}/${TOOL_NAME}-latest" "HEAD"

cd "${DEST}/eval"
find . -type f -exec perl -i -e 's/^(.+)_GIT_REPO=.+$/\1_GIT_REPO=""/g' {} \;

rm -rf "${DEST}/eval/src"

anonymize_this
