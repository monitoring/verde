#ifndef __HEAP_H__
#define __HEAP_H__

#include <stdlib.h>

void push_synchronized(int value, size_t producer_id);
int pop_synchronized(size_t consumer_id);
void init_queue_heap();

#endif
