#!/usr/bin/env sh

cd "$(dirname "$0")"

. ./anonymize.config.sh

rm -rf __pycache__ .git .gdb_history

# Anonymization breaks docker image generation for the moment
rm -rf docker-artifact

# There is no point in keeping evaluations as is neither
rm -rf eval

if [ -f verde ]; then
    mv verde i-RVTool
fi

find . -type f -exec sed -i -e 's/verde/'${ANON_NAME}'/g' -e 's/VERDE/'${ANON_NAME_CAPS}'/g' -e 's/Verde/'${ANON_NAME_CAP}'/g' {} \;
find . -type f -name "*.py" -exec sed -i 's/^__.*//g' {} \;

for i in "Raphaël" "Yliès" "Falcone" "Jakse" "Kevin" "Kévin" "Pouget" "Raphael" "Ylies" "Jean-François" "Jean-Francois" "Mehaut" "Méhaut" "Université" "Joseph" "Fourier" "Grenoble" "Alpes" "UGA" "UJF" "CORSE" "INRIA" "France" ; do # "LIG" "INP" breaks CSS
	find . -type f -exec sed -i "s/$i/###/gI" {} \;
done

find . -type f -exec sed -r -i -e 's/(A)uthors/\1uthors(s)/gI' -e 's/developped by [^.]+/developped by a non-empty set of people/gI' {} \;
find . -type f -exec perl -pi -e 's|is written in Python as part of .+?</p>|is written in Python as part of an anonymized project in undisclosed conditions.</p>|g' {} \;

rm "$0" "$(dirname "$0")/anonymize.config.sh"
