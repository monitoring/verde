#!/usr/bin/env python3
import context
from context import verde_ctx
import gdb
import breakpoint as bpmodule
from functools import partial
from monitorcommon import StopExecution

WATCHPOINT_READ   = gdb.WP_READ
WATCHPOINT_WRITE  = gdb.WP_WRITE
WATCHPOINT_ACCESS = gdb.WP_ACCESS

class Breakpoint(bpmodule.BaseBreakpoint):
        def __init__ (self, spec, is_watchpoint, callback, wp_class=None):
            bptype = gdb.BP_WATCHPOINT if is_watchpoint else gdb.BP_BREAKPOINT
            self.original_spec = spec
            gdb.Breakpoint.__init__(self, spec, bptype, wp_class, internal=True)
            self.silent = True
            self.callback = callback

        def trigger(self):
            self.callback()
            return False

class PendingBreakpoint:
    def __init__(self, spec, is_watchpoint, callback, wp_class=None):
        self.bp = None
        bpmodule.after_breakpoint(partial(self.__create, spec, is_watchpoint, callback, wp_class))

    def __create(self, spec, is_watchpoint, callback, wp_class=None):
        self.bp = Breakpoint(spec, is_watchpoint, callback, wp_class)

    def delete(self):
        bpmodule.after_breakpoint(self.__delete)

    def __delete(self):
        if self.bp is not None:
            self.bp.delete()
            self.bp = None


class PendingCheckpoint:
    def __init__(self):
        self.cid = -1
        self.deleted = False
        bpmodule.after_breakpoint(self.__create)

    def __create(self):
        if not self.deleted:
            self.cid = context.checkpoint()

    def drop(self):
        self.deleted = True
        if self.cid != -1:
            context.drop_checkpoint(self.cid)

    def restore(self, stop=False):
        if self.cid == -1:
            raise Exception("Cannot restore this checkpoint: not ready!")

        p = partial(context.restore_checkpoint, self.cid, stop=stop)
        bpmodule.after_breakpoint(p)
        return p


def watchpoint(spec, access_type, callback):
    return PendingBreakpoint(
        spec=spec,
        is_watchpoint=True,
        wp_class=access_type,
        callback=callback
    )


def breakpoint(spec, callback):
    return PendingBreakpoint(
        spec=spec,
        is_watchpoint=False,
        callback=callback
    )


def remove_breakpoint(breakpoint):
    breakpoint.delete()


def remove_watchpoint(watchpoint):
    watchpoint.delete()

def checkpoint_get_id(c):
    return c.cid

def checkpoint():
    return PendingCheckpoint()


def restore_checkpoint(checkpoint, stop=False):
    return checkpoint.restore(stop=stop)


def drop_checkpoint(checkpoint):
    checkpoint.drop()

def after_breakpoint(f):
    bpmodule.after_breakpoint(f)

def suspend_execution(msg="The scenario suspended the execution"):
    raise StopExecution(msg)
