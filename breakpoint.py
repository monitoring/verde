#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A module which handles gdb's breakpoints.
"""

from gdbvalue2native import gdb_value_to_native_python, gdb_get_args
from property import WatchEvent
from functools import partial
from property import WatchEvent, CallEvent
import gdb
import os
import signal
from monitorcommon import StopExecution

from execution import (
    CONTINUE,
    STOP,
    DEBUGGER_SHELL
)

current_workaround_pc = -1
current_workaround_id = 0

actions_after_breakpoint = []

in_breakpoint = False
behavior_after_actions = CONTINUE

in_cont = False

class ContinueException(BaseException):
    pass

must_continue = False
stop_for_after_breakpoint = False

def combine_stops(stop1, stop2):
    if stop1 is None:
        return stop2
    if stop2 is None:
        return stop1

    return max(stop1, stop2)

def cont():
    global in_cont
    global must_continue

    must_continue = True

    if not in_cont:
        in_cont = True
        while must_continue:
            must_continue = False
            ret = gdb.execute("continue")


def handle_execution_stop(arg=None):
    global behavior_after_actions

    if isinstance(arg, gdb.BreakpointEvent) and stop_for_after_breakpoint:
        stop = handle_actions_after_breakpoints()
        if not stop and behavior_after_actions != DEBUGGER_SHELL:
            cont()
        behavior_after_actions = CONTINUE

gdb.events.stop.connect(handle_execution_stop)


def workaround_avoid_second_bp_hit_after_reattach_at(pc):
    global current_workaround_pc
    global current_workaround_id

    current_workaround_pc = pc
    current_workaround_id += 1

def after_breakpoint(callback):
    actions_after_breakpoint.append(callback)

    if not in_breakpoint:
        handle_actions_after_breakpoints()

def handle_actions_after_breakpoints():
    global actions_after_breakpoint
    stop = False

    while actions_after_breakpoint:
        try:
            (actions_after_breakpoint.pop(0))()
        except StopExecution as exc:
            stop = True
        except:
            print("An exception has been raised while handling defered actions. This is a bug.")
            import traceback
            traceback.print_exc()

    return stop


def _to_gdb_watchpoint_type(watchpoint_type):
    if watchpoint_type == WatchEvent.READ:
        return gdb.WP_READ

    if watchpoint_type == WatchEvent.WRITE:
        return gdb.WP_WRITE

    if watchpoint_type == WatchEvent.WP_ACCESS:
        return gdb.WP_ACCESS

    return 0

class BaseBreakpoint(gdb.Breakpoint):
    def stop(self):
        global in_breakpoint
        global behavior_after_actions

        try:
            in_breakpoint = True
            behavior_after_actions = self.trigger()
            in_breakpoint = False
        except:
            behavior_after_actions = DEBUGGER_SHELL
            import traceback
            traceback.print_exc()
            in_breakpoint = False

        if behavior_after_actions == DEBUGGER_SHELL or actions_after_breakpoint:
            global stop_for_after_breakpoint
            stop_for_after_breakpoint = True
            return True

        return False

    def trigger(self):
        Exception("You must implement the trigger method")


class PrepareAfterBreakpoint(gdb.Breakpoint):
    def __init__(self, spec, after_breakpoint_to_initialize):
        self.ab = after_breakpoint_to_initialize

        gdb.Breakpoint.__init__(
            self,
            spec=spec,
            type=gdb.BP_BREAKPOINT,
            internal=True,
            temporary=True
        )

        self.silent = True

    def stop(self):
        self.ab.set_finish_breakpoint()
        return False

    def enable(self):
        try:
            self.enabled = True
        except:
            pass

    def disable(self):
        try:
            self.enabled = False
        except:
            pass

class Breakpoint(BaseBreakpoint):
    def __init__(self, event, callback, spec):
        self.last_workaround_id = 0

        is_breakpoint = isinstance(event, CallEvent)

        self.event_before = not is_breakpoint or event.before
        self.event_name   = event.name
        self.callback = callback
        self.ready = False

        gdb.Breakpoint.__init__(
            self,
            spec=spec,
            type=gdb.BP_BREAKPOINT if is_breakpoint else gdb.BP_WATCHPOINT,
            wp_class=0 if is_breakpoint else _to_gdb_watchpoint_type(event.access_type),
            internal=True
        )

        self.ready = True
        self.silent = True

    def enable(self):
        try:
            self.enabled = True
        except:
            pass

    def disable(self):
        try:
            self.enabled = False
        except:
            pass

    def trigger(self):
        stop = None
        if current_workaround_id > self.last_workaround_id:
            addr = gdb.newest_frame().pc()
            # WORKAROUND: don't hit the breakpoint once too often after reattach
            self.last_workaround_id = current_workaround_id
            if current_workaround_pc == addr:
                return False

        stop = self.callback(self)
        if stop == STOP:
            gdb.execute("quit")
            return DEBUGGER_SHELL

        return stop


class FinishBreakpoint(gdb.FinishBreakpoint):
    def __init__(self, underlayingbp, event, callback, frame=None):
        self.last_workaround_id = 0

        if frame:
            super().__init__(frame, internal=True)
        else:
            super().__init__(internal=True)

        self.underlayingbp = underlayingbp
        self.event_before = False
        self.event_name   = event.name
        self.callback = callback
        self.ready = False
        self.ready = True
        self.silent = True
        underlayingbp.finish_breakpoints.append(self)

    def enable(self):
        try:
            self.enabled = True
        except:
            pass

    def disable(self):
        try:
            self.enabled = False
        except:
            pass

    def get_return_value(self):
        return self.return_value

    def trigger(self):
        stop = None
        if current_workaround_id > self.last_workaround_id:
            addr = gdb.newest_frame().pc()
            # WORKAROUND: don't hit the breakpoint once too often after reattach
            self.last_workaround_id = current_workaround_id
            if current_workaround_pc == addr:
                return False

        stop = self.callback(self)
        if stop == STOP:
            gdb.execute("quit")
            return DEBUGGER_SHELL

        return stop

    def stop(self):
        self.underlayingbp.finish_breakpoints.remove(self)
        if gdb.newest_frame().name == self.event_name:
            FinishBreakpoint(self.underlayingbp, self.event, self.callback)

        global in_breakpoint
        global behavior_after_actions

        try:
            in_breakpoint = True
            behavior_after_actions = self.trigger()
            in_breakpoint = False
        except:
            behavior_after_actions = DEBUGGER_SHELL
            import traceback
            traceback.print_exc()
            in_breakpoint = False

        if behavior_after_actions == DEBUGGER_SHELL or actions_after_breakpoint:
            global stop_for_after_breakpoint
            stop_for_after_breakpoint = True
            return True

        return False

    def out_of_scope(self):
        return self.stop()

class PendingBreakpoint:
    def __init__(self, event, callback):
        self.event    = event
        self.callback = callback
        self.bp = None
        self.disabled = False
        self.q = None
        self.prepare_after = None

        spec = event.name

        is_breakpoint = isinstance(event, CallEvent)

        if is_breakpoint and spec[0] != "*":
            try:
                # this lookup is done in order to be relatively sure that the
                # breakpoint is set in only one location. This is useful, for
                # example, when you need to preload a function and break into
                # the preloaded function.

                if gdb.lookup_symbol(spec)[0].symtab.filename != "dl-minimal.c":
                    # avoid minimal dynamic linker functions
                    spec = gdb.lookup_symbol(spec)[0].symtab.filename + ":" + spec

            except Exception:
                pass

        self.spec = spec

        if not is_breakpoint or self.event.before:
            self.bp = Breakpoint(self.event, self.callback, spec)
        else:
            self.set_prepare_after()

    def set_prepare_after(self):
        self.finish_breakpoints = []
        f = gdb.newest_frame()

        while f:
            if f.name() == self.event.name:
                self.set_finish_breakpoint(f)
                break
            f = f.older()

        self.prepare_after = PrepareAfterBreakpoint(self.event.name, self)

    def set_finish_breakpoint(self, frame=None):
        f = FinishBreakpoint(self, self.event, self.callback, frame)
        if self.disabled:
            f.disable()

    def restore_after_cont(self, arg=None):
        gdb.events.stop.disconnect(self.restore_after_cont)
        cont()

    def enable(self):
        if self.disabled:
            self.disabled = False
            if self.bp:
                    self.bp.enable()
            elif self.prepare_after:
                self.set_prepare_after()

    def disable(self):
        if not self.disabled:
            self.disabled = True
            if self.bp:
                self.bp.disable()
                gdb.post_event(self.bp.delete)
            elif self.prepare_after:
                self.prepare_after.disable()
                gdb.post_event(self.prepare_after.delete)
                if self.finish_breakpoints:
                    for fb in self.finish_breakpoints:
                        fb.disable()
                        gdb.post_event(fb.delete)


def on(event, callback):
    return PendingBreakpoint(event, callback)

def enable(bp):
    bp.enable()

def disable(bp):
    bp.disable()
