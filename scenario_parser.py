#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A parser for the Scenario Language.

    The main function of this module is parse(string). It Returns an AST of the
    scenario.
"""

import re

from lexer import Lexer, ParseError
from scenario import Scenario, ScenarioEvent


def is_valid_number_char(char, first_char):
    return char.isnumeric()

def is_valid_identifier_char(char, first_char):
    """ Checks if the character is a valid identifier character.

        char       -- the character to check
        first_char -- whether if char is the first character of an identifier
    """

    return (
        char == "_" or (
            char.isalpha() if first_char else char.isalnum()
        )
    )

def is_valid_function_name(char, first_char):
    return char == ":" or is_valid_identifier_char(char, first_char)

def parse_thing(lexer, expected_msg, is_valid_char):
    thing = ""
    lexer.skip_white()
    cur_char = lexer.getchar()
    if not is_valid_char(cur_char, True):
        lexer.ungetchar()
        raise ParseError(expected_msg + lexer.pos_str())
    thing += cur_char

    try:
        cur_char = lexer.getchar()
        while is_valid_char(cur_char, False):
            thing += cur_char
            cur_char = lexer.getchar()

        lexer.ungetchar()
    except lexer.EOF:
        pass

    return thing


def parse_string(lexer):
    if lexer.try_eat('"'):
        identifier = ""
        while lexer.getchar(advance=False) != '"':
            c = lexer.getchar()
            if c == "\\":
                identifier += lexer.getchar()
            else:
                identifier += c
        lexer.eat('"')
        return identifier

    raise ParseError("Expected a string" + lexer.pos_str())


def parse_identifier(lexer):
    """ Parses an identifier from the lexer """
    return parse_thing(lexer, "Expected a valid identifier", is_valid_identifier_char)

def parse_function_name(lexer):
    """ Parses an identifier from the lexer """

    if lexer.try_eat('"'):
        return parse_string(lexer)

    return parse_thing(lexer, "Expected a valid function name", is_valid_function_name)


def action_from_identifier(identifier):
    return identifier


def optimize_indentation(code):
    code = "\n" + code
    beginings = re.finditer(r"^[\s]+", code)

    min_begining = None

    for begining in beginings:
        begining_line = begining.group(0)
        if not min_begining or len(begining_line) < len(min_begining):
            min_begining = begining_line

    if min_begining:
        return code.replace(min_begining, "\n").strip()

    return code.strip()

def parse_python_code(lexer):
    """ Returns a python code from the lexer """

    res = ""

    while True:
        cur_char = lexer.getchar()
        while cur_char != "\n":
            res += cur_char
            cur_char = lexer.getchar()

        while cur_char.strip() == "":
            res += cur_char
            cur_char = lexer.getchar()

        if cur_char == "}":
            while lexer.ungetchar() != "\n":
                pass

            return optimize_indentation(res.rstrip()) + "\n"
        else:
            res += cur_char



def parse_braces_python_code(lexer, brace_already_eaten=False):
    """ Parses a python code surrounded by braces from the lexer """

    if not brace_already_eaten:
        lexer.eat("{")

    cur_char = lexer.getchar()

    while cur_char != "\n" and cur_char.strip() == "":
        cur_char = lexer.getchar()

    if cur_char == "}":
        python_code = ""
    else:
        lexer.ungetchar()

        lexer.eat("\n")

        python_code = parse_python_code(lexer)

        lexer.eat("\n")
        lexer.eat("}")

    return python_code


def parse_maybe_braces_python_code(lexer):
    """ Tries to Parses a python code surrounded by braces from the lexer """

    if lexer.try_eat("{"):
        return parse_braces_python_code(lexer, True)

    return ""


def maybe_parse_identifier(lexer):
    try:
        return parse_identifier(lexer)
    except:
        return None

def parse_remaining_event(lexer):
    """ Parses a state from the lexer (without the keyword "state") """

    if lexer.try_eat("entering"):
        entering = True
    elif lexer.try_eat("leaving"):
        entering = False
    else:
        raise ParseError("Expected 'entering' or 'leaving'" + lexer.pos_str())

    state_name = None
    regex = False

    if lexer.try_eat("non-accepting"):
        accepting = False
        lexer.eat("state")
    elif lexer.try_eat("accepting"):
        accepting = True
        lexer.eat("state")
    else:
        accepting = None
        lexer.eat("state")
        state_name = maybe_parse_identifier(lexer)

        if state_name is None:
            state_name = parse_string(lexer)
            regex = True
        else:
            regex = False

    slice_policy = ScenarioEvent.ANY_SLICE
    if lexer.try_eat("for"):
        if lexer.try_eat("one slice"):
            slice_policy = ScenarioEvent.ONE_SLICE
        elif lexer.try_eat("any slice"):
            slice_policy = ScenarioEvent.ANY_SLICE
        else:
            lexer.eat("all slices")
            slice_policy = ScenarioEvent.ALL_SLICES

    code = parse_braces_python_code(lexer)

    return ScenarioEvent(
        state_name=state_name,
        regex=regex,
        entering=entering,
        accepting=accepting,
        code=code,
        slice_policy=slice_policy
    )

def parse_event_list(lexer):
    """ Parses an event list from the lexer """

    if not lexer.try_eat("on"):
        return []

    evt = parse_remaining_event(lexer)
    return [evt] + parse_event_list(lexer)


def parse_initialization(lexer):
    """ Parses the initialization block from the lexer """

    if lexer.try_eat("initialization"):
        return parse_braces_python_code(lexer)
    else:
        return None

def parse_body(lexer):
    """ Parses the entire property automaton from the lexer """

    initialization = {}

    init = parse_initialization(lexer)

    import scenarioenv
    for (k, v) in scenarioenv.__dict__.items():
        initialization[k] = v

    if init:
        # this make this parser unsafe
        exec(init, initialization)

    ret = Scenario(
        initialization=initialization,
        events=parse_event_list(lexer),
    )

    lexer.expect_end()
    return ret


def parse(string):
    """ Parses the entire property automaton from a string """
    return parse_body(Lexer(string))
