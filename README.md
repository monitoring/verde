# About Verde and i-RV (Interactive Runtime Verification)

Interactive Debugging with a traditional debugger can be tedious.
One has to manually run a program step by step and set breakpoints to track a bug.

i-RV is an approach to bug fixing that aims to help developpers during their Interactive Debbugging sessions using Runtime Verification.

Verde is the reference implementation of i-RV.
It is a GDB plugin written in Python.
Verde can be used to track a bug in a program. A bug is seen as a violation of
a property that is part of the specification of the program.
When a property stops being verified, Verde can run actions like letting the developer access the debugger prompt so (s)he can find the reason.

To get started, see Section [Try i-RV on a Simple Example](#try-i-rv-on-a-simple-example).

Verde is developed by Raphaël Jakse (Ph.D. student of Univ. Grenoble Alpes), supervised by Yliès Falcone, Jean-François Méhaut (CORSE team, INRIA, LIG).

A paper presenting Interactive Runtime Verification is available at [https://arxiv.org/abs/1705.05315](https://arxiv.org/abs/1705.05315). See Section Publications.

# Download, Install and Run Verde

To download Verde, just git clone this repository:


    git clone git@gitlab.inria.fr:monitoring/verde.git


Instructions to install and run Verde, as well as experiments described in our publications, are given in [artifact/README.md](artifact/README.md)

# Try i-RV on a Simple Example

We wrote a short tutorial to experiment with Verde and i-RV.

In this tutorial, the user is invited to find and fix bugs in a faulty program.
The program uses an iterator to browse a container (an array).

When using an iterator, some rules should be observed (i.e. some properties should hold). For instance:
 - When getting the next element of a container using an iterator, one should check that there actually is an element to get.
 - When destroying a container, there should not be any active iterator left on this container.

The user is also invited to write properties to look for other (un)wanted behaviors.

To run this tutorial:

 1. Set-up your environment by following instructions given in [artifact/README.md](artifact/README.md).
 1. Run:

        ./exp.sh baditer

A video of this tutorial is available:

![Bad-Iter Tutorial](https://lig-membres.imag.fr/jakse/irv/verde/baditer.webm)

 - in mp4 format: https://lig-membres.imag.fr/jakse/irv/verde/baditer.mp4
 - in webm format: https://lig-membres.imag.fr/jakse/irv/verde/baditer.webm

# Usage

Usage of the tool is described in [USAGE.md](USAGE.md).

# Writing Properties and Scenarios

Writing properties is described in [WRITING_PROPERTIES.md](WRITING_PROPERTIES.md).
Our model of properties is described in our publications.

# Publications
 - *Interactive Runtime Verification*, Technical Report on ArXiv: [hxttps://arxiv.org/abs/1705.05315](https://arxiv.org/abs/1705.05315).
 - *Interactive Runtime Verification*, conference paper at ISSRE'17: pending publication.
