#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A parser for the Extended Automaton-Based Description Language.

    The main function of this module is parse(string). It Returns an AST of the
    property.
"""

import re

from lexer import Lexer, ParseError, EOF

from property import (
    TransitionEndBlock,
    EventParameter,
    CallEvent,
    WatchEvent,
    Transition,
    State,
    Property
)


def is_valid_number_char(char, first_char):
    return char.isnumeric()

def is_valid_identifier_char(char, first_char):
    """ Checks if the character is a valid identifier character.

        char       -- the character to check
        first_char -- whether if char is the first character of an identifier
    """

    return (
        char == "_" or (
            char.isalpha() if first_char else char.isalnum()
        )
    )

def is_valid_function_name(char, first_char):
    return char == ":" or is_valid_identifier_char(char, first_char)

def parse_thing(lexer, expected_msg, is_valid_char):
    thing = ""
    lexer.skip_white()
    cur_char = lexer.getchar()
    if not is_valid_char(cur_char, True):
        lexer.ungetchar()
        raise ParseError(expected_msg + lexer.pos_str())
    thing += cur_char

    try:
        cur_char = lexer.getchar()
        while is_valid_char(cur_char, False):
            thing += cur_char
            cur_char = lexer.getchar()

        lexer.ungetchar()
    except EOF:
        pass

    return thing

def parse_comments(lexer):
    lexer.skip_white()
    if lexer.try_eat("#"):
        try:
            while lexer.getchar() != '\n':
                pass
            parse_comments(lexer)
        except EOF:
            pass

def parse_string_remaining(lexer):
    identifier = ""
    while lexer.getchar(advance=False) != '"':
        c = lexer.getchar()
        if c == "\\":
            identifier += lexer.getchar()
        else:
            identifier += c
    lexer.eat('"')
    return identifier

def parse_identifier(lexer):
    """ Parses an identifier from the lexer """
    return parse_thing(lexer, "Expected a valid identifier", is_valid_identifier_char)

def parse_function_name(lexer):
    """ Parses an identifier from the lexer """

    if lexer.try_eat('"'):
        return parse_string_remaining(lexer)

    return parse_thing(lexer, "Expected a valid function name", is_valid_function_name)


def action_from_identifier(identifier):
    return identifier


def optimize_indentation(code):
    code = "\n" + code
    beginings = re.finditer(r"^[\s]+", code)

    min_begining = None

    for begining in beginings:
        begining_line = begining.group(0)
        if not min_begining or len(begining_line) < len(min_begining):
            min_begining = begining_line

    if min_begining:
        return code.replace(min_begining, "\n").strip()

    return code.strip()

def parse_python_code(lexer):
    """ Returns a python code from the lexer """

    res = ""

    while True:
        cur_char = lexer.getchar()
        while cur_char != "\n":
            res += cur_char
            cur_char = lexer.getchar()

        while cur_char.strip() == "":
            res += cur_char
            cur_char = lexer.getchar()

        if cur_char == "}":
            while lexer.ungetchar() != "\n":
                pass

            return optimize_indentation(res.rstrip()) + "\n"
        else:
            res += cur_char



def parse_braces_python_code(lexer, brace_already_eaten=False):
    """ Parses a python code surrounded by braces from the lexer """

    if not brace_already_eaten:
        lexer.eat("{")

    cur_char = lexer.getchar()

    while cur_char != "\n" and cur_char.strip() == "":
        cur_char = lexer.getchar()

    if cur_char == "}":
        python_code = ""
    else:
        lexer.ungetchar()

        lexer.eat("\n")

        python_code = parse_python_code(lexer)

        lexer.eat("\n")
        lexer.eat("}")

    return python_code


def parse_maybe_braces_python_code(lexer):
    """ Tries to Parses a python code surrounded by braces from the lexer """

    if lexer.try_eat("{"):
        return parse_braces_python_code(lexer, True)

    return ""


def maybe_parse_identifier(lexer):
    try:
        return parse_identifier(lexer)
    except:
        return None

def parse_maybe_success(lexer):
    """ Parses a success block from the lexer """

    if lexer.try_eat("success"):
        inline_action = parse_maybe_braces_python_code(lexer)
        state = maybe_parse_identifier(lexer)

        if lexer.try_eat("()"):
            action_name = action_from_identifier(state)
            state = maybe_parse_identifier(lexer)
        else:
            action_name = None

        return TransitionEndBlock(
            inline_action=inline_action,
            action_name=action_name,
            state=state
        )

    return None

def parse_success(lexer):
    success = parse_maybe_success(lexer)

    if not success:
        lexer.eat("success", "Expected a success block")

    return success

def parse_remaining_failure(lexer):
    """ Parses a failure block from the lexer
        (without the keyword "failure") """

    inline_action = parse_maybe_braces_python_code(lexer)
    state         = maybe_parse_identifier(lexer)

    if lexer.try_eat("()"):
        action_name = action_from_identifier(state)
        state = None
    else:
        action_name = None

    return TransitionEndBlock(
        inline_action=inline_action,
        action_name=action_name,
        state=state
    )


def parse_failure(lexer):
    """ Parses a failure block from the lexer """

    lexer.eat("failure")
    return parse_remaining_failure(lexer)


def parse_maybe_failure(lexer):
    """ Tries to parse a failure block from the lexer """

    if lexer.try_eat("failure"):
        return parse_remaining_failure(lexer)
    return None

def parse_guard(lexer):
    """ Parses a guard block from the lexer """

    if lexer.try_eat("{"):
        return parse_braces_python_code(lexer, True)

    return None

def try_parse_int(lexer):
    try:
        return int(parse_thing(lexer, "Expected a valid integer", is_valid_number_char))
    except:
        return None

def parse_parameter(lexer, allow_arg_number):
    pointer = False
    ref = False
    pyeval = False
    py_value = None
    c_value = None
    expression = None
    identifier = None

    if lexer.try_eat("*"):
        pointer = True
    elif lexer.try_eat("&"):
        ref = True
    elif lexer.try_eat("!"):
        lexer.eat('"')
        py_value = parse_string_remaining(lexer)
        pyeval = True

    if not pyeval:
        if lexer.try_eat("#"):
            lexer.eat("ecid")
            identifier = "#ecid"
        elif lexer.try_eat('expr("'):
            expression = parse_string_remaining(lexer)
            lexer.eat(")")
        else:
            identifier = parse_identifier(lexer)

        if allow_arg_number and identifier == "arg":
            c_value = try_parse_int(lexer)
            if c_value is not None:
                lexer.eat("as")
                identifier = parse_identifier(lexer)

        if c_value is None:
            c_value = identifier

    if lexer.try_eat("as"):
        identifier = parse_identifier(lexer)

    explicit_type = None
    if lexer.try_eat(":"):
        explicit_type = parse_identifier(lexer)

    return EventParameter(
        name=identifier,
        c_value=c_value,
        py_value=py_value,
        pointer=pointer,
        ref=ref,
        explicit_type=explicit_type,
        expression=expression
    )


def parse_parameter_list_content(lexer):
    """ Parses a parameter list (without the parentheses) from the lexer """

    params = []
    comma_expected = False
    while True:
        if lexer.try_eat(")"):
            lexer.ungetchar()
            return params

        if comma_expected:
            lexer.eat(",")

        params += [parse_parameter(lexer, allow_arg_number=True)]

        comma_expected = True


def parse_parameter_list(lexer):
    """ Parses a parameter list from the lexer """

    if lexer.try_eat("("):
        parameter_list = parse_parameter_list_content(lexer)
        lexer.eat(")")
        return parameter_list

    return []

def parse_maybe_before_after(lexer):
    if lexer.try_eat("after"):
        return False

    lexer.try_eat("before")
    return True

def parse_return(lexer):
    """ parses the name of the variable which will contain the return value of
        the function """

    lexer.skip_white()
    if lexer.try_eat("->") or lexer.try_eat(":"):
        lexer.skip_white()
        return parse_parameter(lexer, allow_arg_number=False)

    return None


def parse_call_event(lexer, name, before):
    params = parse_parameter_list(lexer)
    ret = parse_return(lexer)

    return CallEvent(
        name=name,
        parameters=tuple(params),
        return_var=ret,
        before=before and (ret is None)
    )

def watchpoint_address_char(c, first=False):
    return not c.isspace() and c != "("

def parse_watchpoint_address(lexer):
    return parse_thing(lexer, "", watchpoint_address_char)

def to_watch_event_type(access_type):
    if access_type == "read":
        return WatchEvent.READ

    if access_type == "write":
        return WatchEvent.WRITE

    return WatchEvent.ACCESS

def parse_watch_event(lexer, access_type):
    lexer.skip_white()
    name = parse_watchpoint_address(lexer)
    parameters = parse_parameter_list(lexer)
    ret = parse_return(lexer)

    return WatchEvent(
        name=name,
        parameters=tuple(parameters),
        access_type=to_watch_event_type(access_type),
        return_var=ret
    )

def parse_event(lexer):
    """ Parses an event block from the lexer """

    before = parse_maybe_before_after(lexer)
    lexer.eat("event")

    name = parse_function_name(lexer)
    if name == "read" or name == "write" or name == "access":
        if lexer.try_eat("("):
            lexer.ungetchar()
            return parse_call_event(lexer, name, before)
        else:
            return parse_watch_event(lexer, name)

    return parse_call_event(lexer, name, before)

    if lexer.try_eat("#"):
        name = parse_function_name(lexer)
        if name == "join" or name == "leave":
            return parse_join_leave_event(lexer, name, prgm_target)
        raise ParseError("Expected join or leave " + lexer.pos_str())
    else:
        name = parse_function_name(lexer)

    if name == "read" or name == "write" or name == "access" or name == "timeout" or name == "exception":
        if lexer.try_eat("("):
            lexer.ungetchar()
            return parse_call_event(lexer, name, before, prgm_target)

        if name == "timeout":
            return parse_timeout_event(lexer)

        if name == "exception":
            return parse_exception_event(lexer, prgm_target)

        return parse_watch_event(lexer, name, prgm_target)

    return parse_call_event(lexer, name, before, prgm_target)

def parse_accepting(lexer, undefinedIsAccepting=True):
    """ Parses the state's "(non-)accepting" keyword from the lexer """

    if lexer.try_eat("non-accepting"):
        return False

    if lexer.try_eat("accepting"):
        return True

    return True if undefinedIsAccepting else None

def parse_action(lexer):
    """ Parses the state's "(non-)accepting" keyword from the lexer """

    m = lexer.try_eat_regex(r"([a-zA-Z0-9_]+)\(\)")

    if m:
        return m.group(1)
    return None

def parse_slice(lexer):
    slice_unit = []

    paren = lexer.try_eat("(")
    stop = False

    while not stop:
        slice_unit.append(parse_identifier(lexer))
        if not lexer.try_eat(","):
            stop = True

    if paren:
        lexer.eat(")")

    return slice_unit


def parse_remaining_transition(lexer):
    """ Parses a transition from the lexer
    (without the keyword "transition") """

    name = maybe_parse_identifier(lexer)

    lexer.eat("{")

    parse_comments(lexer)

    event   = parse_event(lexer)
    parse_comments(lexer)
    guard   = parse_guard(lexer)
    parse_comments(lexer)
    success = parse_maybe_success(lexer)
    parse_comments(lexer)
    failure = parse_maybe_failure(lexer)
    parse_comments(lexer)

    if not success:
        success = parse_success(lexer)

    parse_comments(lexer)

    res = Transition(
        name=name,
        event=event,
        guard=guard,
        success=success,
        failure=failure
    )

    lexer.eat("}")

    return res

def parse_transition_list(lexer, slice_bindings):
    """ Parses a transition list from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("transition"):
        transition = parse_remaining_transition(lexer)
        remaining = parse_transition_list(lexer, slice_bindings)

        return [transition] + remaining

    return []

def parse_import_transitions(lexer):

    parse_comments(lexer)

    if lexer.try_eat("import transitions from state"):
        return parse_identifier(lexer)
    return None

def parse_remaining_state(lexer, slice_bindings):
    """ Parses a state from the lexer (without the keyword "state") """

    name       = "*" if lexer.try_eat("*") else parse_identifier(lexer)
    initial    = lexer.try_eat("initial") or name == "init"
    accepting  = parse_accepting(lexer, undefinedIsAccepting=(name != "*"))
    final      = lexer.try_eat("final")
    action_name = parse_action(lexer)
    import_transitions = parse_import_transitions(lexer)

    if lexer.try_eat("{"):
        transitions = parse_transition_list(lexer, slice_bindings)
        lexer.eat("}")
    else:
        transitions = []

    return State(
        name=name,
        accepting=accepting,
        action_name=action_name,
        transitions=transitions,
        import_transitions=import_transitions,
        final=final,
        initial=bool(initial)
    )

def parse_state_list(lexer, slice_bindings):
    """ Parses a state list from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("state"):
        state = parse_remaining_state(lexer, slice_bindings)
        return [state] + parse_state_list(lexer, slice_bindings)

    return []

def parse_initialization(lexer):
    """ Parses the initialization block from the lexer """

    parse_comments(lexer)

    if lexer.try_eat("initialization"):
        return parse_braces_python_code(lexer)
    else:
        return None

def parse_slice_bindings(lexer):
    parse_comments(lexer)

    if lexer.try_eat("slice"):
        lexer.eat("on")

        return parse_slice(lexer)
    return ()

def parse_default_class(lexer):
    parse_comments(lexer)

    if lexer.try_eat("default class"):
        return parse_identifier(lexer)

    return None

def parse_body(lexer):
    """ Parses the entire property automaton from the lexer """

    parse_comments(lexer)

    initialization = {}

    slice_bindings = parse_slice_bindings(lexer)
    default_class  = parse_default_class(lexer)
    init = parse_initialization(lexer)

    if init:
        # this make this parser unsafe
        exec(init, initialization)

    ret = Property(
        slice_bindings=slice_bindings,
        initialization=initialization,
        states=parse_state_list(lexer, slice_bindings)
    )

    lexer.expect_end()
    return ret


def parse(string):
    """ Parses the entire property automaton from a string """
    return parse_body(Lexer(string))
