initialization {
    import gdb;

    allocs        = {}
    not_freed     = {}
    bad_frees     = {}
    good_frees    = {}
    allocs_failed = []
    double_free   = {}

    N = 0

    def to_int(gdb_value_pointer):
        return int(gdb_value_pointer.cast(gdb.lookup_type("long")))

    def check_free(p):
        p_int = to_int(p)
        print(hex(p_int) + ": free'd though was not malloc'd")

        if p_int in double_free and double_free[p_int]:
            print(hex(p_int) + ": double free!")

        if p_int not in bad_frees:
            bad_frees[p_int] = []

        bad_frees[p_int].append(gdb.execute('where', to_string=True))

    def on_malloc(ret):
        global N
        p_int = to_int(ret)

        if p_int == 0:
            print("Warning: allocation failure")
            allocs_failed.append(gdb.execute('where', to_string=True))
            return


        if p_int not in allocs:
            allocs[p_int]    = []

        allocs[p_int].append(gdb.execute('where', to_string=True))
        not_freed[p_int] = True
        double_free[p_int] = False
        N += 1

    def guard_free(p):
        p_int = to_int(p)

        if p_int == 0:
            return None

        if p_int in not_freed and not_freed[p_int]:
            return True

        return False

    def on_realloc(p, size, ret):
        global N

        if size == 0:
            return on_free(p)

        on_malloc(ret)

        if p == ret:
            N -= 1
        elif p != 0:
            return on_free(p)

    def on_free(p):
        global N
        N -= 1
        p_int = to_int(p)

        if p_int in not_freed:
            del not_freed[p_int]

        double_free[p_int] = True
}

state init {
    transition {
        after event malloc() -> ret

        success {
            on_malloc(ret)
        } odd
    }

    transition {
        after event calloc() -> ret

        success {
            on_malloc(ret)
        } odd
    }


    transition {
        event free(p) {
            return guard_free(p)
        }

        success  {
            check_free(p)
        } odd
    }

    transition {
        after event realloc(arg 1 as p, arg 2 as size) -> ret

        success {
            print("Realloc before any malloc")
            return on_realloc(p, size, ret)
        } odd
    }
}

state odd {
    transition {
        event free(arg 1 as p) {
            return guard_free(p)
        }

        success {
            return on_free(p)
        } odd

        failure {
            check_free(p)
        } odd
    }

    transition {
        after event malloc() -> ret

        success {
            on_malloc(ret)
        } odd
    }

    transition {
        after event calloc() -> ret

        success {
            on_malloc(ret)
        } odd
    }

    transition {
        after event realloc(arg 1 as p, arg 2 as size) -> ret

        success {
            return on_realloc(p, size, ret)
        } odd
    }
}
