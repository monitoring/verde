#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    Scenario-related classes
"""

class ScenarioEvent(object):
    # BEWARE: these three variables have to be numbers and not strings
    # see calls to scenario_bind_evt_to_state in monitor.py
    # strings would potentially conflict with state names

    ANY_SLICE  = 1
    ONE_SLICE  = 2
    ALL_SLICES = 3

    def __init__(self, state_name, regex, entering, accepting, code, slice_policy):
        self.state_name = state_name
        self.regex = regex
        self.entering = entering
        self.accepting = accepting
        self.code = code
        self.compiled_code = None
        self.slice_policy = slice_policy

    def __str__(self):
        return "<" + ("entering" if self.entering else "leaving" ) + " " + (
            ("state " + self.state_name) if self.state_name else (
                self.regex if self.regex else (
                    (
                        "accepting" if self.accepting else "non-accepting"
                    ) + " state"
                )
            )
        ) + " for " + (
            "all slices"
                if self.slice_policy == ScenarioEvent.ALL_SLICES
                else (
                    "one slice"
                        if self.slice_policy == ScenarioEvent.ONE_SLICE
                        else "any slice"
                )
        ) + ">"

class Scenario(object):
    def __init__(self, initialization, events):
        self.initialization = initialization
        self.events = events
