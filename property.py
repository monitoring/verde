#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A module which contains the automaton model.
"""

from collections import Counter, namedtuple
from monitorcommon import events

#NOTE: field 'type' is needed for event named tuples because otherwise,
#      different event type can be equal.
#      e.g. namedtuple("A", "a b")(1,2) == namedtuple("B", "a b")(1,2)
#
#      See https://bugs.python.org/issue31239

class EventParameter(namedtuple("EventParameter", "name, c_value, pointer, ref, py_value, explicit_type, expression")):
    def __str__(self):
        if self.ref:
            s = "&"
        else:
            s = ""

        if self.pointer:
            s += "*"

        if self.c_value:
            s += "val " + self.c_value
        elif self.py_value:
            s += 'py ' + self.py_value
        elif self.expression:
            s += 'expr ' + self.py_value

        if self.explicit_type:
            s += ":" + self.explicit_type

        return "Param " + self.name + ("<" + s + ">" if s else "")

EventParameter.__new__.__defaults__ = (None, False, False, None, None, None)

CallEvent = namedtuple("CallEvent", "name, before, methodArgs, parameters, return_var, type")
CallEvent.__new__.__defaults__ = (True, None, (), None, None, "call")

JoinEvent = namedtuple("JoinEvent", "parameters, type")
JoinEvent .__new__.__defaults__ = ("join",)

LeaveEvent = namedtuple("LeaveEvent", "parameters, type")
LeaveEvent .__new__.__defaults__ = ("leave",)

class WatchEvent(namedtuple("WatchEvent", "name, access_type, parameters, return_var, type")):
    READ   = 1
    WRITE  = 2
    ACCESS = 4

WatchEvent.__new__.__defaults__ = ((), None, None, "watch")

class ExceptionEvent(namedtuple("ExceptionEvent", "name, caught, uncaught, parameters")):
    def get_parameter_names(self):
        return [parameter.name for parameter in self.parameters]

class TransitionEndBlock(object):
    def __init__(self, inline_action=None, state=None,action_name=None):
        self.inline_action = inline_action
        self.state         = state
        self.action_name   = action_name
        self.compiled_inline_action = None

class Transition(object):
    def __init__(self, name, event, guard=None, success=None, failure=None, slices=[], slices_new=None): # BUG ?
        self.name       = name
        self.event      = event

        if isinstance(guard, str):
            self.guard   = guard
            self.compiled_guard = None
        else:
            self.guard   = None
            self.compiled_guard = guard

        self.success = success
        self.failure = failure

class State(object):
    REMOVE_TRANSITION = 0
    ADD_TRANSITION    = 1

    def __init__(self, name, accepting, action_name=None, import_transitions=None, transitions=set(), initial=False, final=False):
        self.name         = name
        self.initial      = initial

        # If the state is reached, the slice will be forgotten
        self.final       = final

        self.accepting    = accepting
            # True:  the state is accepting
            # False: the state is non-accepting
            # None:  this is undefined (possible for 'state *')

        self.action_name  = action_name
        self.transitions  = set(transitions)
        self.import_transitions = import_transitions # this is the name of a state
        self.events       = {
            self.ADD_TRANSITION:    set(),
            self.REMOVE_TRANSITION: set()
        }

    def add_transition(self, transition):
        for e in events[self.ADD_TRANSITION]:
            e(self, transition)

        self.transitions.add(transition)

    def remove_transition(self, transition):
        for e in events[self.REMOVE_TRANSITION]:
            e(self, transition)

        self.transitions.remove(transition)

    def listen(self, event, callback):
        self.events[event].add(callback)

    def release(self, event, callback):
        self.events[event].remove(callback)

    def __str__(self):
        return "State <" + self.name + ">"

class Property(object):
    REMOVE_TRANSITION = 0
    ADD_TRANSITION    = 1
    ADD_STATE         = 2
    REMOVE_STATE      = 4

    def __init__(self, name=None, initialization={}, states=set(), slice_bindings=(), default_class=None):
        self.name = name
        self.initialization = initialization
        self.states = states
        self.slice_bindings = slice_bindings
        self.default_class = default_class

        self.events = {
            self.ADD_TRANSITION:    set(),
            self.REMOVE_TRANSITION: set(),
            self.ADD_STATE:         set(),
            self.REMOVE_STATE:      set()
        }

        for state in states:
            self.add_state(state)

    def add_state(self, state):
        if state in self.states:
            return

        for e in self.events[self.ADD_STATE]:
            e(self, state)

        for transition in state.transitions:
            for e in self.events[self.ADD_TRANSITION]:
                e(self, state, transition)

        state.listen(state.ADD_TRANSITION, self.added_transition)
        state.listen(state.REMOVE_TRANSITION, self.removed_transition)
        self.states.add(state)

    def remove_state(self, state):
        if state not in self.states:
            return

        for e in self.events[self.REMOVE_STATE]:
            e(self, state)

        for transition in state.transitions:
            for e in self.events[self.REMOVE_TRANSITION]:
                e(self, state, transition)

        state.release(state.ADD_TRANSITION, self.added_transition)
        state.release(state.REMOVE_TRANSITION, self.removed_transition)
        self.states.remove(state)

    def removed_transition(self, state, transition):
        for e in self.events[self.REMOVE_TRANSITION]:
            e(self, state, transition)

    def added_transition(self, state, transition):
        for e in self.events[self.ADD_TRANSITION]:
            e(self, state, transition)

    def listen(self, event, callback):
        self.events[event].add(callback)

    def release(self, event, callback):
        self.events[event].remove(callback)

class RuntimeEvent:
    def __init__(self, event):
        self.formalEvent = event
        self.parameters = {}

    def __str__(self):
        if isinstance(self.formalEvent, CallEvent):
            return self.formalEvent.name + str(self.parameters)

        return str(self.formalEvent) + ", parameters: " + str(self.parameters)
