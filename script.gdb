python import sys, os
python sys.path.append(".")
python import breakpoint as checkexecbpmodule
define hook-run
    pi checkexecbpmodule.interactive = False
end
define hook-step
    pi checkexecbpmodule.interactive = True
end
define hook-start
    pi checkexecbpmodule.interactive = True
end
define hook-continue
    pi checkexecbpmodule.interactive = False
end
set can-use-hw-watchpoints 0

set python print-stack full
