#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys
import os

try:
    from PyQt5.QtWebKitWidgets import *
    from PyQt5.QtWebKit import *
    from PyQt5.QtWidgets import *
    from PyQt5.QtGui import *
    from PyQt5.QtCore import *
except ImportError:
    try:
        from PyQt4.QtWebKit import *
        from PyQt4.QtGui import *
        from PyQt4.QtCore import *
    except ImportError:
        from PySide.QtWebKit import *
        from PySide.QtGui import *
        from PySide.QtCore import *
        from PySide.QtCore import Signal as pyqtSignal
        #from PySide.QtCore import Slot as pyqtSlot


class Object(QObject):
    """ See http://blog.mathieu-leplatre.info/le-piege-des-qthread-fr.html for
        explanations of this class. """

    startWorker = pyqtSignal(name="startWorker")

    def emitStartWorker(self):
        self.startWorker.emit()


class QueueWorker(QObject):
    evaluateJavaScript = pyqtSignal('QString', name="evaluateJavaScript")

    def __init__(self):
        super(self.__class__, self).__init__()
        self.stopped = False

    def run(self):
        item = "\n"
        while not self.stopped and item:
            item = sys.stdin.readline()
            if item:
                self.evaluateJavaScript.emit(item)
        app.quit()

    def stop(self):
        self.stopped = True


sys.stdout.close()
sys.stderr.close()

os.close(1)
os.close(2)

monitor_id = sys.stdin.readline()
non_accepting = sys.stdin.readline()

svg = ""
line = sys.stdin.readline()

while line != "END\n":
    svg = svg + line
    line = sys.stdin.readline()

app = QApplication(sys.argv)
window = QWebView()
window.setHtml("""<!DOCTYPE html>
    <html xmlns='http://www.w3.org/1999/xhtml'>
        <head>
            <meta charset="utf-8" />
            <title>Graph</title>
            <style>
                html {
                    background: white
                }

                svg {
                    margin: auto;
                    position: relative;
                }

                .node ellipse {
                    fill: white
                }

                .node.non-accepting ellipse {
                    fill: #FDD
                }

                .node.non-accepting text {
                    fill: #500
                }

                .edge text, .edge polygon {
                    fill: blackA50;
                }

                .edge path, .edge polygon {
                    stroke: black;
                }

                .node ellipse, .edge path, .edge polygon, .edge text {
                    -webkit-transition: 500ms;
                    transition: 500ms;
                }

                .node.active ellipse {
                    fill: #5C5
                }

                .node.non-accepting.active ellipse {
                    fill: #C55
                }

                .node.old ellipse {
                    fill: #CCC
                }

                .edge.active text, .edge.active polygon {
                    fill: #A50;
                }

                .edge.active path, .edge.active polygon {
                    stroke: #A50;
                }

            </style>
            <script>
                var svg = null, nodes = null, edges = null;
                var mustClearTrans = true;

                if (!window.Set) {
                    window.Set = function (l) {
                        this._s = {};
                        l.forEach(
                            (function (e) {
                                this._s["%" + e] = e;
                            }).bind(this)
                        );
                    };

                    window.Set.prototype.has = function (e) {
                        return this._s.hasOwnProperty("%" + e);
                    };

                    window.Set.prototype.forEach = function (f) {
                        for (var i in this._s) {
                            if (this._s.hasOwnProperty(i)) {
                                f(this._s[i]);
                            }
                        }
                    };
                }

                function setCurrent(states, forgetOldStates) {
                    nodes.forEach(
                        function (n) {
                            if (states.has(n.getAttribute("id"))) {
                                n.classList.add("active");
                                n.classList.remove("old");
                                n.classList.remove("inactive");
                            } else if (n.classList.contains("active")) {
                                n.classList.remove("active");
                                if (!forgetOldStates) {
                                    n.classList.add("old");
                                }
                                n.classList.add("inactive");
                            } else {
                                n.classList.remove("old");
                            }
                        }
                    );

                    mustClearTrans = true;
                }

                function clearTransitions() {
                    edges.forEach(
                        function (e) {
                            e.classList.remove("active");
                        }
                    );
                }

                function takeTransition(trans) {
                    if (mustClearTrans) {
                        mustClearTrans = false;

                        clearTransitions();

                        document.getElementById(trans).classList.add("active");
                    }
                }

                window.onload = function () {
                    svg = document.getElementsByTagName("svg")[0];
                    edges = Array.prototype.slice.call(svg.getElementsByClassName("edge"));
                    nodes = Array.prototype.slice.call(svg.getElementsByClassName("node"));

                    """ + non_accepting + """.forEach(
                        function (n) {
                            document.getElementById(n).classList.add("non-accepting");
                        }
                    );
                };
            </script>
        </head>
        <body>
            """ + svg + """
        </body>
    </html>"""
)

window.setWindowTitle(u"Graph of the property #" + monitor_id)
window.page().settings().setAttribute(QWebSettings.DeveloperExtrasEnabled, True)

window.show()
worker = QueueWorker()
worker.evaluateJavaScript.connect(window.page().mainFrame().evaluateJavaScript)
thread = QThread()
worker.moveToThread(thread)
obj = Object()
obj.startWorker.connect(worker.run)
thread.start()
obj.emitStartWorker()
app.exec_()
