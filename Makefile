PREFIX ?= '/usr/local'

all:
	@echo "make install to install, make uninstall to uninstall."

install: uninstall
	mkdir -p ${PREFIX}/lib/verde ${PREFIX}/doc ${PREFIX}/bin
	find -maxdepth 1 -not -path './.git*' -not -path . -not -path ./__pycache__ -not -path ./doc -exec cp -r '{}' ${PREFIX}'/lib/verde/{}' ';'
	cp -r doc ${PREFIX}/doc/verde
	ln -s ${PREFIX}/lib/verde/verde ${PREFIX}/bin/verde

uninstall:
	rm -rf ${PREFIX}/lib/verde
	rm -rf ${PREFIX}/doc/verde
	rm -f  ${PREFIX}/bin/verde
