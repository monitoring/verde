#!/usr/bin/env python3
from dict import Dict
import breakpoint
import criucheckpoint
import gdb

def criu_checkpoint(block_workaround=False):
    #if not is_running():
        #raise Exception("Cannot checkpoint: the process is not running.")

    #print("BLOCK_WORKAROUND {}".format(block_workaround))
    if True:
        breakpoint.workaround_avoid_second_bp_hit_after_reattach_at(gdb.newest_frame().pc())
    cid = criucheckpoint.criu_checkpoint(block_workaround)

    for name, m in verde_ctx.monitors.items():
        m.checkpoint(cid)
        #m.restore_instrumentation()

    return cid

def criu_restore_checkpoint(checkpoint_id, stop=False):
    (pc, block_workaround) = criucheckpoint.criu_restore(checkpoint_id, stop=stop)

    if True:
        breakpoint.workaround_avoid_second_bp_hit_after_reattach_at(pc)

    for name, m in verde_ctx.monitors.items():
        m.restore_checkpoint(checkpoint_id)

def criu_drop_checkpoint(checkpoint_id):
    #criucheckpoint.criu_delete(checkpoint_id)

    for name, m in verde_ctx.monitors.items():
        m.remove_checkpoint(checkpoint_id)

def is_running():
    return gdb.selected_inferior().is_valid()

checkpoint = criu_checkpoint
restore_checkpoint = criu_restore_checkpoint
drop_checkpoint = criu_drop_checkpoint

verde_ctx = Dict({
    "monitors":        {},
    "current_monitor": "",
    "last_failed":     False,
    "inside_group":    False,
    "globals":         globals(),
    "checkpoints":     [],
    "timecmds":        False,
    "timeid":          1
})
