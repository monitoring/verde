#define _GNU_SOURCE
#include <dlfcn.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <sys/socket.h>

int open(char *path, int flags) {
    static int (*f)(char*, int) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "open");
    }

    return f(path, flags);
}

int open64(char *path, int flags) {
    static int (*f)(char*, int) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "open64");
    }

    return f(path, flags);
}

int close(int descriptor) {
    static int (*f)(int) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "close");
    }

    return f(descriptor);
}

ssize_t read(int fildes, void *buf, size_t nbyte) {
    static ssize_t (*f)(int, void*, size_t) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "read");
    }

    return f(fildes, buf, nbyte);
}

ssize_t write(int fildes, const void *buf, size_t nbyte) {
    static ssize_t (*f)(int, const void*, size_t) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "write");
    }

    return f(fildes, buf, nbyte);
}

int socket(int domain, int type, int protocol) {
    static int (*f)(int, int, int) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "socket");
    }

    return f(domain, type, protocol);
}

int bind(int socket, const struct sockaddr *address, socklen_t address_len) {
    static int (*f)(int, const struct sockaddr *, socklen_t) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "bind");
    }

    return f(socket, address, address_len);
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
    static int (*f)(int, const struct sockaddr *, socklen_t) = NULL;

    if (!f) {
        f = dlsym(RTLD_NEXT, "connect");
    }

    return f(sockfd, addr, addrlen);

}
