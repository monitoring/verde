OLD_PWD=$(pwd)
EXP_RES_FOLDER=$(new_experiment_dir)


make libwrapsyscall.so

cd "${EXP_RES_FOLDER}"

fallocate -l 875000K "${EXP_RES_FOLDER}/bigfile" || truncate --size 875000K "${EXP_RES_FOLDER}/bigfile"

python3 -m http.server 1337 &
HTTP_PID=$!

sleep 2

LANG=C LD_PRELOAD="${OLD_PWD}/libwrapsyscall.so" wget -O /dev/null http://127.0.0.1:1337/bigfile 2>&1 | grep /dev/null | grep -v "Saving to" > wget-without-verde.txt

LANG=C gdb \
    -ex "set environment LD_PRELOAD=${OLD_PWD}/libwrapsyscall.so" \
    -ex "source ${OLD_PWD}/../verde/gdbcommands.py" \
    -ex "verde load-property ${OLD_PWD}/fdaccess.prop" \
    -ex "verde exec set_quiet" \
    -ex "verde run-with-program" \
    -ex "source ${OLD_PWD}/print_read.py" \
    -ex "quit" \
    --args wget -O /dev/null http://127.0.0.1:1337/bigfile 2>&1 | grep '/dev/null\|Number' > wget-with-verde.txt

rm bigfile

kill ${HTTP_PID}

echo "Wget without verde: "
cat wget-without-verde.txt

echo "Wget with verde: "
cat wget-with-verde.txt

cd "${OLD_PWD}"
