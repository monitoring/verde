#!/usr/bin/env sh
set -eu

# From where to clone verde
if [ -z ${VERDE_GIT_REPO+x} ]; then
    VERDE_GIT_REPO="https://gitlab.inria.fr/monitoring/verde.git"
fi
export VERDE_GIT_REPO

if [ -z ${VERDE_COMMIT+x} ]; then
    VERDE_COMMIT="HEAD"
fi
export VERDE_COMMIT

# Where to conduct experiments
if [ -z ${EXPERIMENTS_DIR+x} ]; then
    EXPERIMENTS_DIR="./exp"
fi
export EXPERIMENTS_DIR

VERDE_DIR=$(readlink -m "${EXPERIMENTS_DIR}/tmp/verde")

get_verde () {
    if [ -z "$VERDE_GIT_REPO" ]; then
        rm -f ${VERDE_DIR}
        ln -s "$(realpath "$INITIAL_WD/../verde/verde-${VERDE_COMMIT}")" "${VERDE_DIR}"
    else
        OLDDIR=$(pwd)
        git clone "${VERDE_GIT_REPO}" "${VERDE_DIR}"
        cd "${VERDE_DIR}"
        git reset --hard "${VERDE_COMMIT}"
        cd "${OLDDIR}"
    fi
}

ensure_we_have_verde () {
    if [ ! -d "${VERDE_DIR}" ]; then
        get_verde;
    fi
}

