if [ -z ${GLIBITER_EXEC+x} ] && [ -z ${FILE_EXEC+x} ]; then
    cat << 'HERE'

Then, when calling the experiment, set either GLIBITER_EXEC, FILE_EXEC, VLC_EXEC
or MPLAYER_EXEC.

Set GLIBITER_EXEC to the executable of the program to test with verde
checking for bad usage of Glib iterators. You can for instance try with Gimp,
Evolution, Gnome-calculator.

Set FILE_EXEC to the executable of the program to test with verde
checking for files closes. You can for instance try with Dolphin, Nautilus,
Gimp.

Set VLC_EXEC or MPLAYER_EXEC to the executable of vlc or mplayer (respectively)
to test them while verde is intrumenting the function which renders a
frame.

ATTENTION: you must have the debug symbols for the applications and the set
of libraries they use.

Please see https://wiki.ubuntu.com/Debug%20Symbol%20Packages for more
information.

You might get performance improvements by adding the following line:
        -ex "verde exec set-quiet" \

before the line:
        -ex "verde run-with-program" \

In the run.sh of this experiment. This disables verde's output.
HERE
fi

if [ ! -z ${FILE_EXEC+x} ]; then
    gdb \
        -ex "source ../verde/gdbcommands.py" \
        -ex "verde load-property fopen-close.prop" \
        -ex "verde run-with-program" \
        -ex "quit" \
        "${FILE_EXEC}"
fi

if [ ! -z ${GLIBITER_EXEC+x} ]; then
    gdb \
        -ex "source ../verde/gdbcommands.py" \
        -ex "verde load-property glib_hash_iter.prop" \
        -ex "verde run-with-program" \
        -ex "quit" \
        "${GLIBITER_EXEC}"
fi

if [ ! -z ${VLC_EXEC+x} ]; then
    gdb \
        -ex "source ../verde/gdbcommands.py" \
        -ex "verde load-property vlc-render.prop" \
        -ex "verde run-with-program" \
        -ex "quit" \
        "${GLIBITER_EXEC}"
fi

if [ ! -z ${MPLAYER_EXEC+x} ]; then
    gdb \
        -ex "source ../verde/gdbcommands.py" \
        -ex "verde load-property mplayer-render.prop" \
        -ex "verde run-with-program" \
        -ex "quit" \
        "${GLIBITER_EXEC}"
fi
