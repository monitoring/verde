#define _POSIX_C_SOURCE 199309L
#define _DEFAULT_SOURCE

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#define NB_REPEAT_INSIDE_EACH_EXP 100


// attente active pendant le nombre de microsecondes donné
void activesleep(long microseconds) {
    struct timespec tbegin, tcurrent;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tbegin);
    time_t sec = 0;
    long nsec = 0;
    while (microseconds > sec * 1e6 + nsec / 1000) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &tcurrent);
        sec = tcurrent.tv_sec - tbegin.tv_sec;
        if (tcurrent.tv_nsec > tbegin.tv_nsec) {
            nsec = tcurrent.tv_nsec - tbegin.tv_nsec;
        } else {
            sec--;
            nsec = (1000000000L + tcurrent.tv_nsec) - tbegin.tv_nsec;
        }
    }
}

void f(int t) {
    __asm__("");
}

int main(int argc, char** argv) {
    if (argc < 2) {
        puts("Please give a time in microseconds in argument.");
    }

    struct timespec tbefore, tafter;

    int t = atoi(argv[1]);

    if (t == -1) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &tbefore);
        for (size_t i = 0; i < NB_REPEAT_INSIDE_EACH_EXP; i++) {
            __asm__("");
        }
        clock_gettime(CLOCK_MONOTONIC_RAW, &tafter);
    } else if (t == 0) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &tbefore);
        for (size_t i = 0; i < NB_REPEAT_INSIDE_EACH_EXP; i++) {
            f(t);
        }
        clock_gettime(CLOCK_MONOTONIC_RAW, &tafter);
    } else {
        clock_gettime(CLOCK_MONOTONIC_RAW, &tbefore);
        for (size_t i = 0; i < NB_REPEAT_INSIDE_EACH_EXP; i++) {
            f(t);
            activesleep(t);
        }
        clock_gettime(CLOCK_MONOTONIC_RAW, &tafter);
    }

    long nsec;

    time_t sec = tafter.tv_sec - tbefore.tv_sec;

    if (tafter.tv_nsec > tbefore.tv_nsec) {
        nsec = tafter.tv_nsec - tbefore.tv_nsec;
    } else {
        sec--;
        nsec = (1000000000L + tafter.tv_nsec) - tbefore.tv_nsec;
    }

    printf(" %7d, %ld.%09ld\n", t, sec, nsec);

    return EXIT_SUCCESS;
}
