import random
import time
import os
import sys
from subprocess import call

#NB_REPEAT_FOR_EACH_EXP = 40
NB_REPEAT_FOR_EACH_EXP = 10

BENCH_DIR = sys.argv[1] + "/"

with open(BENCH_DIR + 'raw.txt', 'w') as fraw:
    with open(BENCH_DIR + 'verde.txt', 'w') as fverde:
        with open(BENCH_DIR + 'verde-arg.txt', 'w') as fverdearg:
            #with open(BENCH_DIR + 'valgrind.txt', 'w') as fvalgrind:
                times = [
                    # in µs
                    0,
                    0.0001,
                    0.0005,
                    0.001,
                    0.005,
                    0.01,
                    0.05,
                    5,
                    10,
                    25,
                    50,
                    75,
                    100,
                    250,
                    500,
                    750,
                    1000,    # 1ms
                    2000,
                    3000,
                    4000,
                    5000,
#                    10000
                ]

                methods = [
                    (fraw, ""),
                    (fverde, "gdb --batch --command verde.gdb --args "),
                    (fverdearg, "gdb --batch --command verde-arg.gdb --args ") #,
                    #(fvalgrind, "valgrind ")
                ]

                for i in range(NB_REPEAT_FOR_EACH_EXP):
                    print("series: " + str(i))
                    copytimes = times[:]
                    random.shuffle(copytimes)
                    for time in copytimes:
                        print(" time:" + str(time))
                        copymethods=methods[:]
                        random.shuffle(copymethods)
                        for method in copymethods:
                            call(method[1] + "./cetime " + str(time), shell=True, stdout=method[0])

