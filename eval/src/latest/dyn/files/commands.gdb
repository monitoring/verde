# Needed so gdb doesn't crash, and is less annoying to use
set height 0
set width 0

source ../verde/gdbcommands.py

verde load-property queue-vip.prop
verde exec set_quiet
verde run-with-program

# Comment this if you want to play with the debugger at this point
quit
