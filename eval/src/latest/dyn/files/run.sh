#!/usr/bin/env sh
make
echo "For a maximum number of pop in the queue = 1000:"
echo
echo "*** Measuring execution time when the instrumentation is dynamic… ***"
echo
gdb --silent --command commands.gdb --arg main 1000
echo
echo "*** Measuring execution time when the instrumentation is (simulated as) non dynamic… ***"
echo
gdb --silent --command commands-unop.gdb --arg main 1000

# If you want gdb not to quit immediately, edit the commands.gdb file.
