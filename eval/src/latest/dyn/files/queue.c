#include "queue.h"
#include "stdlib.h"

void queue_init(queue_t* queue) {
    queue->last = queue->first = NULL;
}

void queue_push(queue_t* queue, int val) {
    queue_elem_t* new_last = malloc(sizeof(queue_elem_t));
    new_last->prev = queue->last;
    new_last->val = val;

    if (queue->last) {
        queue->last->next = new_last;
    } else {
        queue->first = new_last;
    }

    queue->last = new_last;
}

int queue_pop(queue_t* queue) {
    int val = queue->first->val;
    queue_elem_t* old_first = queue->first;
    queue->first = queue->first->next;

    if (queue->first) {
        queue->first->prev = NULL;
    } else {
        queue->last = NULL;
    }

    free(old_first);
    return val;
}
