#include "queue.h"
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <bsd/stdlib.h>

#undef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309L
#include <time.h>

const int vip = 42;

queue_t queue;
const int NB_ITER = 100;

void push_vip() {
    queue_push(&queue, vip);
}

void push_normal(int val) {
    queue_push(&queue, val);
}

int main(int argc, char** argv) {
    struct timespec tbefore, tafter;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tbefore);


    if (argc < 2) {
        fprintf(stderr, "Please give the max number of pops as the first parameter.\n");
        return EXIT_FAILURE;
    }

    long long max_pop_counts = atoll(argv[1]);

    queue_init(&queue);

    int j = 0;
    while (max_pop_counts > 0) {
        if (j) {
            int i;

            for (i = 0; (queue.first != NULL) && (i < NB_ITER); i++) {
                queue_pop(&queue);
            }

            max_pop_counts -= i;
        } else {
            for (int i = 0; i < NB_ITER; i++) {
//                 int r = arc4random_uniform(100);
                if (i == vip) {
                    push_vip();
                } else {
                    push_normal(i);
                }
            }
        }

        j = !j;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &tafter);
    long nsec;

    time_t sec = tafter.tv_sec - tbefore.tv_sec;

    if (tafter.tv_nsec > tbefore.tv_nsec) {
        nsec = tafter.tv_nsec - tbefore.tv_nsec;
    } else {
        sec--;
        nsec = (1000000000L + tafter.tv_nsec) - tbefore.tv_nsec;
    }

    printf("Execution time (seconds): %ld.%09ld\n", sec, nsec);

    return EXIT_SUCCESS;
}
