#ifndef __QUEUE_H__
#define __QUEUE_H__

typedef struct queue_elem_t {
    struct queue_elem_t *prev;
    struct queue_elem_t *next;
    int val;
} queue_elem_t;


typedef struct queue_t {
    queue_elem_t* first;
    queue_elem_t* last;
} queue_t;

void queue_init(queue_t* queue);
int queue_pop(queue_t* queue);
void queue_push(queue_t* queue, int val);

#endif

