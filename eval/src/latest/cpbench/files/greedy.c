#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void f(useconds_t s)
{
	static size_t c = 0;
	++c;
	printf("Call #%lu\n", c);

    if (s) {
        usleep(s);
    }
}

int main(int argc, char** argv)
{
	if (argc != 3) {
		puts(
			"This program takes exactly two parameters:" "\n"
			"  1. The amount of memory to eat, in bytes" "\n"
			"  2. The sleep time between two calls to f, in microseconds"
		);
		return EXIT_FAILURE;
	}

	size_t size = strtol(argv[1], NULL, 10);
	size_t usec = strtol(argv[2], NULL, 10);
	char *s = malloc(size);

    for (size_t i = 0; i < size; i++) {
        s[i] = '1';
    }

	while (1) {
		f(usec);
	}

	return EXIT_SUCCESS;
}
