#!/usr/bin/env sh

make

if [ "$(id -u)" != "0" ]; then
	echo 'You are not root. This will probably fail.'
fi

if [ "$(which criu)" = "" ]; then
	echo "Could not find CRIU. Note that on some systems (like Fedora), using sudo might pose problems (criu might not be in the PATH if compiled manually)."
	exit
fi

EXP_RES_FOLDER=$(new_experiment_dir)
echo "size_megabytes,cmd,time_sec" > "${EXP_RES_FOLDER}/results.csv"

max=10
for i in $(seq ${max}); do
    echo "Run $i/$max:"
    for size_in_megabytes in 1 5 10 25 50 75 100 250 500 750 1000; do
        echo "  Size ${size_in_megabytes} MB…"
        gdb --silent --command commands.gdb --args ./greedy $((size_in_megabytes*1024*1024)) 0 |
            fgrep '[verde] Time of' |
            perl -pe 's/^[\s\S]+command ([\S]+) [^:]*: ([\S]+) sec/'${size_in_megabytes}',\1,\2/g' >> "${EXP_RES_FOLDER}/results.csv"
    done
done
echo "You can get the results in the directory ${EXP_RES_FOLDER}"
