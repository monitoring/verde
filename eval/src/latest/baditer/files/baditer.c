#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct IntArray {
    int* a;
    size_t length;
    size_t capacity;
} IntArray;

typedef struct IntArrayIterator {
    IntArray* ia;
    size_t position;
} IntArrayIterator;

IntArray* int_array_new(size_t capacity) {
    IntArray* ia = malloc(sizeof(IntArray));
    ia->a = malloc((sizeof(int)) * capacity);
    ia->length = 0;
    ia->capacity = capacity;

    return ia;
}

void int_array_push(IntArray* ia, int value) {
// assert(ia->length + 1 < ia->capacity);
// not iterating!
    ia->a[ia->length++] = value;
}

int int_array_pop(IntArray* ia) {
// assert(ia->length > 0);
// not iterating!
   return ia->a[--ia->length];
}

size_t int_array_length(IntArray* ia) {
   return ia->length;
}

size_t int_array_capacity(IntArray* ia) {
   return ia->capacity;
}

void int_array_set(IntArray* ia, size_t index, int value) {
// index > 0 anyway
// assert(index < ia->length);
// not iterating!

    ia->a[index] = value;
}

int int_array_get(IntArray* ia, size_t index) {
    // index > 0 anyway
    // assert(index < ia->length);

    return ia->a[index];
}

void int_array_free(IntArray* ia) {
    free(ia->a);
    free(ia);
}

IntArrayIterator* int_array_iterator_new(IntArray* ia) {
    IntArrayIterator* iter = malloc(sizeof(IntArrayIterator));
    iter->ia = ia;
    iter->position = 0;

    return iter;
}

bool int_array_iterator_has_next(IntArrayIterator* iter) {
    return iter->position < iter->ia->length;
}

int int_array_iterator_next(IntArrayIterator* iter) {
    return int_array_get(iter->ia, iter->position++);
}


void int_array_iterator_free(IntArrayIterator* iter) {
    free(iter);
}

int initialize_array_at(size_t index) {
    return index * 3;
}

int main() {
    size_t size = 100;
    IntArray* ia = int_array_new(100);

    size_t i = 0;
    while (i < size) {
        int value = initialize_array_at(i);
        int_array_push(ia, value);
        i++;

        if (i < size && value % 6 == 0) {
            // Every value divisible by 6 is put twice in the array
            int_array_push(ia, value);
            i++;
        }
    }

    IntArrayIterator* iter = int_array_iterator_new(ia);

    while (int_array_iterator_has_next(iter)) {
        int value = int_array_iterator_next(iter);
        printf("%s: Value in array: %d\n", __FILE__, value);
        if (!(value % 18)) {
            printf("%s: Value is divisible by 18!\n", __FILE__);
            int value2 = int_array_iterator_next(iter);
            if (value2 == value) {
                printf("%s: Next value is the same, ok!\n", __FILE__);
            } else {
                printf("%s: Next value is different, something is wrong!\n", __FILE__);
            }
        }
    }

    int_array_free(ia);
    return EXIT_SUCCESS;
}
