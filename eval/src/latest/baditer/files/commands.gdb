# Needed so gdb doesn't crash, and is less annoying to use
set height 0
set width 0

source ../verde/gdbcommands.py

verde load-property has-next.prop
verde load-scenario stop.scn
verde show-graph
verde load-property free-iter-before-array.prop
verde load-scenario stop.scn
verde show-graph
#verde run-with-program
