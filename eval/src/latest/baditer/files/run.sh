#!/usr/bin/env sh

if [ -z ${var+x} ] || [ ! -x "$SHELL" ]; then
	SHELL=/bin/bash
fi

shell=$SHELL

if [ -z "${BADITER_FAST+x}" ]; then

cat <<'EOF'
 * Welcome to the Bad-Iterator experiment.
 *
 * Make sure your terminal's width is at least 80 characters, otherwise our
 * beautiful presentation will be completely messed up. Also, you will need to
 * be able to scroll up and down. I bet your terminal emulator is able to do
 * that out of the box though.
 *
 * I'll wait for you. When you are ready and your seat belt is fastened,
 * type CTRL+D.
EOF
$shell

cat <<'EOF'
 * First, let's look around by typing ls and show file contents using less or
 * a code editor of your choice.
 *
 * You might want to look at baditer.c. This is a buggy program that we
 * will try to fix together.
 *
 * This program creates and fills a container, and then iterates over it.
 * At the begining of the file, we define structures to represent containers
 * and iterators that should be manipulated using dedicated methods defined
 * right after. There is no need to look at the methods and the structs for now.
 *
 * NOTE FOR USERS OF THE DOCKER CONTAINER:
 *  | To install an editor in the docker container, type:
 *  |
 *  |    apt install vim # (or nano, or any other editor you like)
 *  |
 *  | You can also access this folder from outside of Docker in the folder
 *  | verde/artifact/docker-folder/files/eval/exp/tmp/baditer.
 *
 * When you are done, type CTRL+D.
EOF
$shell
cat <<'EOF'
 * Have you seen? There are two .prop files. These are descriptions of two
 * properties that the program breaks.
 *
 * The first one, has-next.prop, says that before using the next method of an
 * iterator, one should call hasNext to check if there is an element to get.
 *
 * The second one, free-iter-before-array.prop, says that one should not destroy
 * a container if there are still active iterators over this container.
 *
 * You can look at these properties if you wish.
 *
 * When you are done, type CTRL+D.
EOF

$shell
cat <<'EOF'
 * We are going to execute the program inside the debugger and see what happens.
 * The execution of the program is guided by the scenario stop.scn.
 *
 * This scenario basically says that if whenever we enter a non accepting
 * state in a property, the execution should be suspended so the developer can
 * use the debugger.
 *
 * This scenario can be modified so checkpoints are taken instead of suspending
 * the execution, so the developer can choose which bug (s)he wants to study
 * first.
 *
 * NOTE: Any modification to the experiment will persist until its folder (i.e.
 * the folder is which you currently are) is deleted. In which case it will be
 * rebuilt on next run.
 *
 * You can look at the scenario stop.scn if you wish.
 *
 * When you are done, type CTRL+D.
EOF

$shell
cat <<'EOF'
 * All right, let's build the program! Type:
 *
       make
 *
 * When you are done, type CTRL+D.
EOF

$shell
cat <<'EOF'
 *  We are ready to start debugging and use Verde! Type this:
 *
       gdb baditer
 *
 * Then, inside gdb, load verde:
 *
       source ../verde/gdbcommands.py
 *
 * Then, we don't want gdb to mess with our terminal (and crash), so it is
 * important to type:
 *
       set height 0
       set width 0
 *
 * Then, let's load our first property, the scenario and show its graph:
 *
       verde load-property has-next.prop
       verde load-scenario stop.scn
       verde show-graph
 *
 * We definitely want to check both properties at the same time while debugging,
 * right? Let's do the same with the second property:
 *
       verde load-property free-iter-before-array.prop
       verde load-scenario stop.scn
       verde show-graph
 *
 * Now, let's run property verification and the program at the same time:
 *
       verde run-with-program
 *
 *
 * +~~~~~~~~~~~~~~~~<HEY, ALL THIS IS A BIT PAINFUL, ISN'T IT?>~~~~~~~~~~~~~~~~+
 * | You will probably agree that all this is a lot of work we don't want to   |
 * | have to do every time we need to debug a program.                         |
 * |                                                                           |
 * | Fortunately, everyting is automatable! Next time, save yourself some      |
 * | troubles and use a GDB script you will run like that instead of running   |
 * | all these commands manually:                                              |
 * |                                                                           |
          make && gdb --silent --command commands.gdb baditer
 * |                                                                           |
 * | Don't cheat by using this command the very first time though! :-)         |
 * +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
 *
 *
 * +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<GOING FURTHER>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
 * | Surely, you do not want a container to be modified while                  |
 * | iterating over it. Try to write a property expressing this, and modify    |
 * | the program so you can test your property.                                |
 * +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
EOF

else

clear

echo " * Welcome to the fast version of Bad-Iter, the “hello world” of Verde."
echo " * Magic command:"
echo " * "
echo " * 	make && gdb --silent --command commands.gdb baditer"
echo " * "
echo " * Don't forget to issue run-with-program"
echo " * "
echo " * Good luck debugging!"
echo " * "

fi

exec $shell
