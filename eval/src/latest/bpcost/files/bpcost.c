#define _POSIX_C_SOURCE 199309L
#define _DEFAULT_SOURCE

#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void break_this() { asm(""); }

int main(int argc, char** argv) {
    struct timespec tbefore, tafter;

    if (argc < 2) {
        puts("We are expecting a number as the first parameter");
        return EXIT_FAILURE;
    }

    const uint64_t m = atoll(argv[1]);

    clock_gettime(CLOCK_MONOTONIC_RAW, &tbefore);
        for (uint64_t i = 0; i < m; i++) {
                break_this();
        }
    clock_gettime(CLOCK_MONOTONIC_RAW, &tafter);

    long nsec;

    time_t sec = tafter.tv_sec - tbefore.tv_sec;

    if (tafter.tv_nsec > tbefore.tv_nsec) {
        nsec = tafter.tv_nsec - tbefore.tv_nsec;
    } else {
        sec--;
        nsec = (1000000000L + tafter.tv_nsec) - tbefore.tv_nsec;
    }

    printf("%ld.%09ld\n", sec, nsec);

    return EXIT_SUCCESS;
}
