#!/usr/bin/env sh

EXP_RES_FOLDER=$(new_experiment_dir)

cc -std=gnu99 bpcost.c -g -o bpcost

NB_BP=100000

echo "Measuring the (fixed) overhead induced by encountering one breakpoint during the execution of a program:"
./bpcost ${NB_BP} > ${EXP_RES_FOLDER}/rawtime.txt
NB_BP=${NB_BP} result_file="${EXP_RES_FOLDER}/bptime.txt" gdb --silent --command=commands.gdb ./bpcost

echo "($(cat "${EXP_RES_FOLDER}/bptime.txt") - $(cat "${EXP_RES_FOLDER}/rawtime.txt")) * 1000000 / ${NB_BP}" | bc -l > "${EXP_RES_FOLDER}/result.txt"

echo "Time of one breakpoint (usec): $(cat "${EXP_RES_FOLDER}/result.txt")"
