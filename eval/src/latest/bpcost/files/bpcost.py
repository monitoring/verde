import gdb
class Breakpoint(gdb.Breakpoint):
    def __init__ (self):
        gdb.Breakpoint.__init__(self, "break_this", internal=True)
        self.silent = True

    def stop(self):
        return False

Breakpoint()

