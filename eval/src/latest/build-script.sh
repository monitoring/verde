#!/usr/bin/env sh
set -eu

if [ -z ${SCRIPT_PREFIX+x} ]; then
    SCRIPT_PREFIX="exp"
fi

if [ -z ${SCRIPT_FOLDER+x} ]; then
    SCRIPT_FOLDER="../.."
fi

if [ -z ${SCRIPT_NAME+x} ]; then
    SCRIPT_NAME="${SCRIPT_FOLDER}/${SCRIPT_PREFIX}-$(basename $(pwd)).sh"
fi
SCRIPT_NAME_PART=${SCRIPT_NAME}.part

INITIAL_WD=$(pwd)

foreach_exp () {
    # Execute a function for each experiment

    cd "${INITIAL_WD}"

    for i in *; do
        if [ -d "$i" ]; then
            $1 "$i"
        fi
    done

}

get_heredoc_for () {
    HERE="HERE";
    while grep -q "$HERE" "$1"; do
        HERE="${HERE}E"
    done
    printf "%s" "$HERE"
}

echo_prepare_exp_files () {
    # Write the functions which pepare the experiment

    echo "prepare_exp_files_$1 () {"

    cd "$1/files"

    for i in *; do
        HERE=$(get_heredoc_for "$i")

        printf "cat > '%s' << '%s'"'\n' "$i" "$HERE"
        sed -e '$a\' "$i"
        printf '%s\n\n' "$HERE"
    done

    cd ..

    if [ -f prepare.sh ]; then
        sed -e '$a\' prepare.sh
    fi

    cd ..
    echo "cd \${INITIAL_WD}"
    echo "}"
    echo
}

if [ ! -f "intro.sh" ]; then
    cat << 'HERE'
An "intro.sh" file is missing in the current folder. It should at least contain
something in the lines of:

---
#!/usr/bin/env sh
set -eu

# Where to conduct experiments
if [ -z ${EXPERIMENTS_DIR+x} ]; then
    EXPERIMENTS_DIR="./my-experiment-set"
fi
export EXPERIMENTS_DIR
---

Then, put each experiment in a folder. This folder contains:
 - a folders "files" containing at least a "run.sh" file, which runs the experiment
 - an optional prepare.sh, which will be run one time in the folder where the
   files will be copied. You might want to run commands to compile things, or
   chmod +x to make some files executable

The prepare.sh scripts will have access to variables and functions defined in
intro.sh.
HERE
    exit 1
fi

cat intro.sh > "${SCRIPT_NAME_PART}"

cat >> "${SCRIPT_NAME_PART}" << 'HERE'
INITIAL_WD=$(pwd)

new_experiment_dir() {
    # Should only be called from an experiment's folder

    if [ -z ${OVERRIDE_RESULT_FOLDER_PATH+x} ]; then
        RES="$(readlink -m "../../results/$(uname -n)/$(date +%Y-%m-%d--%H-%M-%S)")"
    else
        RES=${OVERRIDE_RESULT_FOLDER_PATH}
    fi

    mkdir -p "${RES}/host"

    cp /proc/cpuinfo    "${RES}/host/cpuinfo"
    cp /proc/meminfo    "${RES}/host/meminfo"
    uname -a >          "${RES}/host/uname-a"
    cp /proc/version    "${RES}/host/version"
    cp /etc/lsb-release "${RES}/host/lsb-release" | true
    cp /etc/os-release  "${RES}/host/os-release"  | true
    gdb --version >     "${RES}/host/gdb-version"

    chmod u+x "${RES}/host/"
    chmod -R u+rw "${RES}/host/"

    printf "%s" "${RES}"
}

HERE

foreach_exp echo_prepare_exp_files >> "${SCRIPT_NAME_PART}"

cat >> "${SCRIPT_NAME_PART}" << 'HERE'
prepare_exp_files () {
    mkdir -p "${EXPERIMENTS_DIR}/tmp/$1"
    cd "${EXPERIMENTS_DIR}/tmp/$1"
    prepare_exp_files_$1
    cd "${INITIAL_WD}"
    chmod +x "${EXPERIMENTS_DIR}/tmp/$1/run.sh"
}

run_exp () {
    if [ ! -f "${EXPERIMENTS_DIR}/tmp/$1/run.sh" ]; then
        prepare_exp_files $1
    fi

    cd "${EXPERIMENTS_DIR}/tmp/$1"
    . ./run.sh
    cd "${INITIAL_WD}"
}

HERE

echo_prepare_exp_files_call () {
    echo "            prepare_exp_files" "$1"
}

echo_run_exp_call () {
    echo "            run_exp" "$1"
}

echo_case_exp () {
    echo "        $1)"
    printf "    "
    echo_run_exp_call "$1"
    echo "        ;;"
}


echo_case_prepare_exp () {
    echo "        prepare:$1)"
    printf "    "
    echo_prepare_exp_files_call $1
    echo "        ;;"
}

if [ ! -f help ]; then
    cat << 'HERE'
A "help" file is missing. It should at least explain what environment
variables can be used to configure your experiment set and the default value
used (you can use the ${VARIABLE} notation), and each experiment.
HERE
    exit 1
fi

HERE=$(get_heredoc_for "help")
echo "help () {" >> "${SCRIPT_NAME_PART}"
printf "    cat << %s"'\n' $HERE >> "${SCRIPT_NAME_PART}"
sed -e '$a\' "help" >> "${SCRIPT_NAME_PART}"

cat >> "${SCRIPT_NAME_PART}" << 'HERE'
Additionally, you can use those options:
    - all      - run all those experiments in this order
    - prepare  - don't run anything, just prepare the files of each experiments.
    - clean    - remove the files that were created, excluding the results.
    - mrproper - remove everything in the EXPERIMENTS_DIR folder.
    - prepare:\${expname} - prepare the given experiment

If you want to study one experiment, go to
"${EXPERIMENTS_DIR}/tmp/\${experiment_name}/" and check the run.sh script after
having prepared or run the experiment.
HERE
echo "$HERE" >> "${SCRIPT_NAME_PART}"

echo "}" >> "${SCRIPT_NAME_PART}"

cat >> "${SCRIPT_NAME_PART}" << 'HERE'

if [ -z ${EXPERIMENTS_DIR+x} ]; then
    echo "The \$EXPERIMENTS_DIR variable is not set. It should be set to a"
    echo "folder, and also propably exported, in your intro.sh file."
    exit 1
fi

if [ -z ${1+x} ]; then
    help
else
    case "$1" in
HERE

echo '        prepare)' >> "${SCRIPT_NAME_PART}"
    foreach_exp echo_prepare_exp_files_call >> "${SCRIPT_NAME_PART}"
echo '        ;;' >> "${SCRIPT_NAME_PART}"

echo '        all)' >> "${SCRIPT_NAME_PART}"
    foreach_exp echo_run_exp_call >> "${SCRIPT_NAME_PART}"
echo '        ;;' >> "${SCRIPT_NAME_PART}"

cat >> "${SCRIPT_NAME_PART}" << 'HERE'
        clean)
            rm -rf "${EXPERIMENTS_DIR}/tmp"
            echo "Temporary directory cleaned. If you want me to clean the results, use the mrproper parameter."
        ;;
        mrproper)
            rm -rf "${EXPERIMENTS_DIR}"
            rmdir $(dirname "${EXPERIMENTS_DIR}")
        ;;
HERE

foreach_exp echo_case_exp >> "${SCRIPT_NAME_PART}"
foreach_exp echo_case_prepare_exp >> "${SCRIPT_NAME_PART}"
echo '    *) echo "Sorry, but I didn’t understand what you mean by ”$1“"; help ;;' >> "${SCRIPT_NAME_PART}"

echo "    esac" >> "${SCRIPT_NAME_PART}"
echo "fi" >> "${SCRIPT_NAME_PART}"

mv "${SCRIPT_NAME_PART}" "${SCRIPT_NAME}"
chmod +x "${SCRIPT_NAME}"
echo "Script has been written to "$(realpath "${SCRIPT_NAME}")
