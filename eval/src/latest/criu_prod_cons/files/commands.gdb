source ../verde/gdbcommands.py
verde load-property queue-size.prop queue-size.py
b queue_push
print "We run the program and the property verification until queue_push is reached."
verde run-with-program
print "We reached queue_push. Lets execute a step until next queue_push call…"
continue
print "And again…"
continue
print "The property is in an accepting state. We set a checkpoint using CRIU."
verde checkpoint-criu
print "We delete our breakpoint."
delete breakpoints
print "See how the execution continues correctly."
continue
print "We stopped because the property reached a non-accepting state. Let's restart from the last checkpoint."
verde checkpoint-criu-restart 1
print "The property is in an accepting state again. If you type 'continue', you will be able to see that the program restart from the same number of pushes than when we took the checkpoint. When you are done, don't forget to run “verde checkpoint-criu-cleanup” to remove all checkpoints."
