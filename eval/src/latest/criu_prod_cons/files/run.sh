#!/usr/bin/env sh

make

if [ "$(id -u)" != "0" ]; then
    echo 'You need to be root in order to experiment with CRIU checkpoints.'
    exit 1
fi

if [ "$(which criu)" = "" ]; then
        echo "Could not find CRIU. Note that on some systems (like Fedora), using sudo might pose problems (criu might not be in the PATH if compiled manually)."
	exit
fi

gdb --silent --command commands.gdb prod_cons ; killall prod_cons

# If you want gdb not to quit immediately, edit the commands.gdb file.
