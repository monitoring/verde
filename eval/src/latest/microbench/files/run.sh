#!/usr/bin/env sh
make

OLD_PWD=$(pwd)

EXP_RES_FOLDER=$(new_experiment_dir)

echo "Running the benchmark… (it might take a while)"
echo "Result will be stored in the following folder: ${EXP_RES_FOLDER}"

if [ ! -f "${EXP_RES_FOLDER}/verde.txt" ]; then
    python3 ./bench.py "${EXP_RES_FOLDER}" > "${EXP_RES_FOLDER}/progress" 2> "${EXP_RES_FOLDER}/errors"
fi

echo "Benchmark done. Formatting data…"

cd "${EXP_RES_FOLDER}"

if [ ! -f "verde.tmp.txt" ]; then
    mv verde.txt verde.tmp.txt
    mv verde-arg.txt verde-arg.tmp.txt
    mv raw.txt raw.tmp.txt
#     mv valgrind.txt valgrind.tmp.txt
    echo "   sleep, time" > verde.txt
    echo "   sleep, time" > verde-arg.txt
    echo "   sleep, time" > raw.txt
#     echo "   sleep, time" > valgrind.txt
    cat verde.tmp.txt | fgrep -i -v  "if (" | fgrep -i -v  "int main(" | fgrep -i -v Breakpoint | fgrep -i -v "Inferior 1" | awk 'NF' >> verde.txt
    cat verde-arg.tmp.txt | fgrep -i -v  "if (" | fgrep -i -v  "int main(" | fgrep -i -v Breakpoint | fgrep -i -v "Inferior 1" | awk 'NF' >> verde-arg.txt
#     cat valgrind.tmp.txt >> valgrind.txt
    cat raw.tmp.txt >> raw.txt

    cat raw.txt | fgrep -i -v -- "-1" > raw-1.txt
    cat verde.txt | fgrep -i -v -- "-1" > verde-1.txt
    cat verde-arg.txt | fgrep -i -v -- "-1" > verde-arg-1.txt
#     cat valgrind.txt | fgrep -i -v -- "-1" > valgrind-1.txt
fi

echo "Generating graphs…"

R CMD BATCH "${OLD_PWD}/plot.R"

mkdir -p graph
mv plot-log.svg plot-linear.svg graph
cd graph

if [ "$(which inkscape)" = "" ]; then
    echo "Not producing PDF files from SVG as I cannot find Inkscape (probably not installed)."
else
    inkscape -f plot-log.svg -A plot-log.pdf
    inkscape -f plot-linear.svg -A plot-linear.pdf
fi

