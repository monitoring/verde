#include "queue.h"
#include "common.h"

#include <stdatomic.h>

void queue_new(queue_t* queue, size_t size) {
    queue->container          = malloc(sizeof(int) * size);
    queue->size_max           = size;
    queue->begin_queue_index  = 0;
    queue->end_queue_index    = 0;
    pthread_mutex_init(&(queue->mutex_queue_access), NULL);

    sem_init(&(queue->sem_pop), 0, 0);
    sem_init(&(queue->sem_push), 0, queue->size_max);
}

void queue_delete(queue_t* queue) {
    sem_destroy(&(queue->sem_pop));
    sem_destroy(&(queue->sem_push));
    pthread_mutex_destroy(&(queue->mutex_queue_access));
    free(queue->container);
}


void queue_push(queue_t* queue, int value) {
	static __thread size_t thread_push_count = 0;

	++thread_push_count;

	queue->container[queue->end_queue_index] = value;
	queue->end_queue_index = (queue->end_queue_index + 1) % queue->size_max;
}

void queue_push_synchronized(queue_t* queue, int value) {
	sem_wait(&queue->sem_push);

	#if BREAK_THINGS
		bool break_now = rand01() < BREAKING_PROBABILITY;
	#endif

	if (!break_now) {
		pthread_mutex_lock(&queue->mutex_queue_access);
	}

	queue_push(queue, value);

	if (!break_now) {
		pthread_mutex_unlock(&queue->mutex_queue_access);
	}

	sem_post(&queue->sem_pop);
}

int queue_pop(queue_t* queue) {
	int current_begin_queue_index = queue->begin_queue_index;
	queue->begin_queue_index = (queue->begin_queue_index + 1) % queue->size_max;
	return queue->container[current_begin_queue_index];
}

int queue_pop_synchronized(queue_t *queue) {
	sem_wait(&queue->sem_pop);

	#if BREAK_THINGS
		bool break_now = rand01() < BREAKING_PROBABILITY;
	#endif

	if (!break_now) {
		pthread_mutex_lock(&queue->mutex_queue_access);
	}

	int value = queue_pop(queue);

	if (!break_now) {
		pthread_mutex_unlock(&(queue->mutex_queue_access));
	}

	sem_post(&(queue->sem_push));
	return value;
}

