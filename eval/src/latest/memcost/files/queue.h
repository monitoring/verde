#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

typedef struct queue_t {
    int begin_queue_index;
    int end_queue_index;
    sem_t sem_pop, sem_push;
    size_t size_max;
    int* container;
    pthread_mutex_t mutex_queue_access;
} queue_t;
int queue_pop(queue_t* queue);
void queue_push(queue_t* queue, int value);
void queue_push_synchronized(queue_t* queue, int value);
int queue_pop_synchronized(queue_t* queue);
void queue_new(queue_t* queue, size_t size);
void queue_delete(queue_t* queue);

#endif

