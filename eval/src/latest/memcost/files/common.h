#include <stdio.h>
#include <stdbool.h>

#define UNUSED(x) (x=x)

#define rand01() ((float)rand()/(float)(RAND_MAX))

#ifndef BREAK_THINGS
	#define BREAK_THINGS 0
#endif

#if !BREAK_THINGS
	#define break_now false
#endif

#ifndef BREAKING_PROBABILITY
	#define BREAKING_PROBABILITY 0.5
#endif


#ifndef NBPROD
	#define NBPROD 5
#endif

#ifndef NBCONS
	#define NBCONS 20
#endif

#ifndef MAX_PROD_WAIT_MICROTIME
	#define MAX_PROD_WAIT_MICROTIME 3000000
#endif

#ifndef MIN_PROD_WAIT_MICROTIME
	#define MIN_PROD_WAIT_MICROTIME 300000
#endif

#ifndef MAX_CONS_WAIT_MICROTIME
	#define MAX_CONS_WAIT_MICROTIME 3000000
#endif

#ifndef MIN_CONS_WAIT_MICROTIME
	#define MIN_CONS_WAIT_MICROTIME 300000
#endif

#ifndef MAX_EXECUTION_TIME
	#define MAX_EXECUTION_TIME 0
#endif

#ifndef DIRECT_DEBUG
	#define DIRECT_DEBUG 0
#endif

#ifndef MAX_VALUE
	#define MAX_VALUE 50
#endif

#ifndef QUEUE_SIZE_MAX
    #define QUEUE_SIZE_MAX 50
#endif

#ifndef NB_QUEUES
    #define NB_QUEUES 10000
#endif

#define debug(str, ...) printf("*** " str " ***\n", __VA_ARGS__)
#define new_value() (rand() % MAX_VALUE)

