#!/usr/bin/env sh
make

OLD_PWD=$(pwd)

EXP_RES_FOLDER=$(new_experiment_dir)

echo "Running the benchmark… (it might take a while)"
echo "Result will be stored in the following folder: ${EXP_RES_FOLDER}"

python3 > "${EXP_RES_FOLDER}/asizeof.txt" <<'END'
from pympler import asizeof
print("dict():", asizeof.asizeof(dict()))
print("():", asizeof.asizeof(()))
print("(None,):", asizeof.asizeof((None,)))
print("(None, None):", asizeof.asizeof((None, None)))
print("(None, None, None):", asizeof.asizeof((None, None, None)))
print("[]:", asizeof.asizeof([]))
END

out="${EXP_RES_FOLDER}/results.csv"

if [ -f "$out" ]; then
    echo "Results already exist, skipping measures."
else
    printf "nb_obj,before,after\n" > "$out"
    for i in 0 1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90 100 200 300 400 500 600 700 800 900 1000 5000 10000 50000 100000; do
        echo "Measuring for $i queues…"
        printf "%d" "$i" >> "$out"
        for line in $(gdb --batch --command=commands.gdb --args prod_cons $i | grep asizeof | cut -d':' -f3 | sed 's/ //g'); do
            printf ",%s" "$line" >> "$out"
        done
        printf "\n" >> "$out"
    done
fi

echo "Generating graphs…"

cd "${EXP_RES_FOLDER}"

R CMD BATCH "${OLD_PWD}/plot.R"

mkdir -p graph
mv plot-linear.svg graph/
cd graph

if [ "$(which inkscape)" = "" ]; then
    echo "Not producing PDF files from SVG as I cannot find Inkscape (probably not installed)."
else
    inkscape -f plot-linear.svg -A plot-linear.pdf
fi

