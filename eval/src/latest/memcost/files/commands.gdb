# Needed so gdb doesn't crash, and is less annoying to use
set height 0
set width 0

source ../verde/gdbcommands.py

verde load-property queue-fast.prop
verde exec set_quiet
verde load-property queue-gdb-mem.prop
verde load-scenario queue-gdb-mem.scn
verde exec set_quiet
verde run-with-program
