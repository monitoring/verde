#define _DEFAULT_SOURCE // for usleep
#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include "queue.h"
#include "common.h"

/* Variables partagées */

void before_queue_new(void)    {}
void after_queue_new(void)     {}
void before_queue_delete(void) {}
void after_queue_delete(void)  {}

int main(int argc, char* argv[]) {
	size_t nb_queues = 0;

	if (argc != 2) {
		fprintf(stderr, "Expecting 1 parameter: the number of queues to create.\n");
		return EXIT_FAILURE;
	}

	if (sscanf(argv[1], "%zu", &nb_queues) != 1) {
 		fprintf(stderr, "error - not an integer");
		return EXIT_FAILURE;
	}

	printf("Creating %zu queues (bytes: %zu)\n", nb_queues, sizeof(queue_t) * nb_queues);

	queue_t* queue = malloc(sizeof(queue_t) * nb_queues);

	if (!queue) {
		fprintf(stderr, "Allocation failed");
		return EXIT_FAILURE;
	}

	srand(time(NULL));

	before_queue_new();

	for (size_t i = 0; i < nb_queues; i++) {
		queue_new(&(queue[i]), QUEUE_SIZE_MAX);
	}

	after_queue_new();

	for (size_t i = 0; i < nb_queues; i++) {
		queue_push(&(queue[i]), new_value());
	}

	for (size_t i = 0; i < nb_queues; i++) {
		queue_pop(&(queue[i]));
	}

	before_queue_delete();

	for (size_t i = 0; i < nb_queues; i++) {
		queue_delete(&(queue[i]));
	}

	after_queue_delete();

	free(queue);

	return EXIT_SUCCESS;
}
