#!/usr/bin/env sh
make
gdb --silent --command commands.gdb prod_cons

# If you want gdb not to quit immediately, edit the commands.gdb file.
