#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <inttypes.h>

#define NBTREADS 5

#define MAX_ITER 10000

void* thread(void* data) {
    intptr_t i = 0;
    for (i = 0; i < MAX_ITER; i++) {
        usleep(250000 + (rand() % 500000));
        printf("data=%" PRIxPTR ", i=%" PRIxPTR  "\n", (intptr_t) data, i);
    }

    return (void*) i;
}

int main() {
    pthread_t threads[NBTREADS];

    for (intptr_t i = 0; i < NBTREADS; i++) {
        pthread_create(&threads[i], NULL, thread, (void*) i);
    }

    for (int i = 0; i < NBTREADS; i++) {
        int res = (int) pthread_join(threads[i], NULL);

        assert(res == MAX_ITER);
    }

    return EXIT_SUCCESS;
}
