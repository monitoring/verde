#!/usr/bin/env sh
# Remotely run experiments
# Usage example:
# ./run.sh no-sync-verde exp microbench result-folder microbench host example.org rebuild-exp run-exp


SSH_CONTROL_PATH="$HOME/.ssh/expe-%r-%h-%p"

suit=latest
localWorkingFolder=.
remoteWorkingFolder=experiments
resultFolder=""
exp=""
host=""
port="22"
syncVerde=1
cleanRes=0

if [ "$1" = "" ]; then
    echo "Please commands to run."
    exit
fi

while [ "$1" != "" ]; do
    case "$1" in
        "suit")
            shift
            suit="$1"
        ;;

        "rebuild-everything")
            rm -rf "$localWorkingFolder"
        ;;

        "host")
            shift
            host="$1"
        ;;

        "exp")
            shift
            expName="$1"
        ;;

        "local-folder")
            shift
            localWorkingFolder="$1"
        ;;

        "remote-folder")
            shift
            remoteWorkingFolder=$1
        ;;

        "remote-port")
            shift
            port="$1"
        ;;

        "clean-verde")
            rm -rf "${localWorkingFolder}/exp/tmp/verde"
            syncVerde=1
        ;;

        "clean-results")
            cleanRes=1
        ;;

        "no-sync-verde")
            syncVerde=0
        ;;

        "rebuild-exp")
            if [ "${expName}" = "" ]; then
                echo "Please set exp before clean-exp."
                exit
            fi

            rm -rf "${localWorkingFolder}/exp-${suit}.sh"
            rm -rf "${localWorkingFolder}/exp/tmp/${expName}"
        ;;

        "result-folder")
            shift
            resultFolder="$1"

        ;;

        "run-exp")
            if [ "${expName}" = "" ]; then
                echo "Please set exp before run-exp."
                exit
            fi

            mkdir -p "${localWorkingFolder}"

            if [ ! -f "${localWorkingFolder}/exp-${suit}.sh" ]; then
                cd ./src/$suit
                ./build-script.sh
                cd -

                cp "exp-${suit}.sh" "${localWorkingFolder}/"
            fi

            cd "${localWorkingFolder}"
            OVERRIDE_RESULT_FOLDER_PATH="$resultFolder" ./exp-"$suit".sh prepare:"${expName}"

            if [ "$host" = "" ]; then
                if [ "${cleanRes}" = 1 ]; then
                    if [ "$resultFolder" = "" ]; then
                        echo "Cannot clean non specified result folder"
                    else
                        echo "Cleaning results folder $resultFolder"
                        rm -rf "$resultFolder"
                    fi
                fi
                OVERRIDE_RESULT_FOLDER_PATH="$resultFolder" ./exp-"$suit".sh "${expName}"
            else
                echo "Running on $host port $port in $remoteWorkingFolder…"

                ssh -fN -p "${port}" -oControlMaster=yes -oControlPath="${SSH_CONTROL_PATH}" "${host}"

                ssh -oControlMaster=no -oControlPath="${SSH_CONTROL_PATH}" -p "${port}" "${host}" "mkdir -p ${remoteWorkingFolder}/exp/tmp/${expName}/; mkdir -p ${remoteWorkingFolder}/exp/tmp/verde/"

                rsync -e "ssh -oControlMaster=no -oControlPath='${SSH_CONTROL_PATH}' -p '${port}'" -avzp --delete "${localWorkingFolder}/exp/tmp/${expName}/" "${host}:${remoteWorkingFolder}/exp/tmp/${expName}/"

                if [ "${syncVerde}" = 1 ]; then
                    rsync -e "ssh -oControlMaster=no -oControlPath='${SSH_CONTROL_PATH}' -p '${port}'" -avzp --delete "${localWorkingFolder}/exp/tmp/verde/"      "${host}:${remoteWorkingFolder}/exp/tmp/verde/"
                fi

                if [ "${cleanRes}" = 1 ]; then
                    if [ "$resultFolder" = "" ]; then
                        echo "Cannot clean non specified result folder"
                    else
                        echo "Cleaning results folder ${remoteWorkingFolder}/results/$resultFolder"
                        ssh -oControlMaster=no -oControlPath="${SSH_CONTROL_PATH}" -p "${port}" "${host}" "rm -rf '${remoteWorkingFolder}/results/$resultFolder'"
                    fi
                fi

                rsync -e "ssh -oControlMaster=no -oControlPath='${SSH_CONTROL_PATH}' -p '${port}'" -avzp --delete "${localWorkingFolder}/exp-${suit}.sh"      "${host}:${remoteWorkingFolder}/exp-${suit}.sh"

                ssh -oControlMaster=no -oControlPath="${SSH_CONTROL_PATH}" -p "${port}" "${host}" "bash -c 'cd ${remoteWorkingFolder}/; OVERRIDE_RESULT_FOLDER_PATH=\"\$PWD/results/${resultFolder}\" ./exp-"$suit".sh ${expName}'"

                ssh -O exit -oControlMaster=no -oControlPath="${SSH_CONTROL_PATH}" -p "${port}" "${host}"
            fi
            cleanRes=0
        ;;

        *)
            echo "Unknown command $1" ;;
    esac
    shift
done
