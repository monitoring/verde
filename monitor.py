#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A module which drives gdb on a program from a property.
"""

import sys

STEP_COLOR    = "\033[1;94m"
ERROR_COLOR   = "\033[1;91m"
STATES_COLOR  = "\033[0;1m"
ENV_KEY_COLOR = "\033[0;92m"
SLICES_COLOR  = "\033[0;1m"
NON_ACCEPTING_COLOR = ERROR_COLOR

from inspect import getmembers

from time import gmtime, strftime

from execution import (
    CONTINUE,
    STOP,
    DEBUGGER_SHELL
)

from dict import Dict

from monitorinterface import MonitorInterface

from monitorcommon import (
    MonitorException,
    StopExecution,
    SetCurrentState,
    events
)

from scenario import ScenarioEvent

from collections import Counter

import copy

import traceback

def colored(color, msg):
    """ Returns a colored version of the msg string with the given color. """
    return color + msg + "\033[0;0m"

def error(msg, monitor=None, exc=None):
    """ Prints an error and throws the exception given, if given. If a monitor
        is given, its state is also printed. """
    global ERROR_COLOR

    print(colored(ERROR_COLOR, "Error: ") + msg)
    if exc:
        raise exc

class AutomatonSlice(object):
    def __init__(self, slice_length, old=None, parent=None):
        self.current_state = None
        self.children = []
        self.parent = parent
        self.root_slice = parent.root_slice if parent else self
        self.checkpointsByCid = {}
        self.watched_events = set()

        if parent is None:
            self.tree_active_states = Counter()

        if old:
            self.set_slice_instance(old.get_slice_instance())
            self.set_current_state(old.get_current_state())
            self.set_env(old.get_env_copy())
        else:
            self.set_slice_instance((None,) * slice_length)
            # WARNING: set it when instanciating
            self.set_env(None)

    def is_state_active(self, state):
        return self.root_slice.tree_active_states[state] != 0

    def get_children_recursive(self):
        for s in self.children:
            yield s
            for c in s.get_children_recursive():
                yield c

    def has_children(self):
        return len(self.children) != 0

    def fork(self):
        new_s = AutomatonSlice(old=self, slice_length=-1, parent=self)
        self.add_child(new_s)
        return new_s

    def add_child(self, new_s):
        self.children.append(new_s)

    def remove(self):
        if self.parent:
            self.parent.children.remove(self)

    def set_current_state(self, state):
        if self.current_state is not None:
            self.child_remove_current_state(self.current_state)

        self.current_state = state

        if state is not None:
            self.child_add_current_state(state)

    def child_add_current_state(self, state):
        if self.is_root():
            self.tree_active_states[state] += 1
        else:
            self.root_slice.child_add_current_state(state)

    def child_remove_current_state(self, state):
        if self.is_root():
            self.tree_active_states[state] -= 1
        else:
            self.root_slice.child_remove_current_state(state)

    def is_root(self):
        return self.root_slice is self

    def get_current_state(self):
        return self.current_state

    def set_env_key(self, key, val):
        self.env[key] = val

    def get_env_copy(self):
        # WARNING:
        # this is a shallow copy. If dictionaries or other complex objects are
        # in there, they will be shared accross slices.
        # This is intentional.
        env = self.get_env()

        if env is None:
            return None

        return env.copy()

    def get_env(self):
        return self.env

    def set_env(self, env):
        self.env = env

    def get_slice_instance(self):
        return self.slice_instance

    def get_parameter_value(self, p, param_names):
        for (param_instance, param_name) in zip(self.get_slice_instance(), param_names):
            if param_name == p:
                return param_instance
        raise KeyError("Unknown parameter name.")

    def set_slice_instance(self, slice_instance):
        self.slice_instance = tuple(slice_instance)

    def get_tree_active_states(self):
        return [key for (key, val) in self.tree_active_states.items() if val != 0]

    def deepcopy(self, parent, root_slice):
        new_s = AutomatonSlice(old=self, slice_length = -1, parent=parent)

        if not root_slice:
            root_slice = new_s
            new_s.root_slice = root_slice

        new_s.children = list(map(lambda child: child.deepcopy(parent=new_s, root_slice=root_slice), self.children))
        return new_s

    def checkpoint(self, cid):
        assert(self == self.root_slice)

        cp = self.deepcopy(None, None)

        cp.tree_active_states = self.tree_active_states.copy()

        self.checkpointsByCid[cid] = cp

    def restore_checkpoint(self, cid):
        cp = self.checkpointsByCid[cid].deepcopy(None, None)
        self.current_state = cp.current_state
        self.children = cp.children
        self.parent = cp.parent
        self.env = cp.env
        self.set_slice_instance(cp.get_slice_instance())
        self.root_slice = self

        self.tree_active_states = cp.tree_active_states.copy()

    def drop_checkpoint(self, cid):
        del self.checkpointsByCid[cid]

def print_step(step):
    """ Prints a step, with, prepended with the current time. """
    global STEP_COLOR
    print(colored(STEP_COLOR, strftime("[%H:%M:%S] ", gmtime()) + step))


def list_states(s):
    """ Makes a string from an iterable, separating its elements by a comma. """
    return ", ".join([(colored(NON_ACCEPTING_COLOR, state.name) if state.accepting is False else state.name) for state in s])


class SlicesByInstance(object):
    def __init__(self, root_slice):
        self.depth = len(root_slice.slice_instance)
        self.init_container()
        self.root_slice = root_slice
        self.add_recursive(root_slice) # FIXME probably costly as hell

    def init_container(self):
        self.container = set() if self.depth == 0 else {}

    def add(self, s, instance_tuple=None, container=None, depth=0):
        if depth == 0:
            container = self.container

        if depth == self.depth:
            container.add(s)
            return

        if instance_tuple is None:
            instance_tuple = s.get_slice_instance()

        if instance_tuple[0] not in container:
            container[instance_tuple[0]] = {} if depth + 1 < self.depth else set()

        self.add(s, instance_tuple[1:], container[instance_tuple[0]], depth + 1)

    def remove(self, s, instance_tuple=None, container=None, depth=0):
        if depth == 0:
            container = self.container

        if depth == self.depth:
            container.remove(s)
            return (not container)

        if instance_tuple is None:
            instance_tuple = s.get_slice_instance()

        try:
            child_container = container[instance_tuple[0]]
        except KeyError:
            #FIXME
            return

        if self.remove(s, instance_tuple[1:], child_container, depth + 1):
            container.pop(instance_tuple[0], None)
            return (not child_container)

    def get(self, instance_tuple, container=None, depth=0):
        if container is None:
            assert depth == 0, "container is None, depth should be 0"
            container = self.container

        if depth == self.depth:
            return set(container)

        param = instance_tuple[0]

        res = set()

        if param is None:
            for p in container:
                res.update(self.get(instance_tuple[1:], container[p], depth + 1))
        else:
            if param in container:
                res.update(self.get(instance_tuple[1:], container[param], depth + 1))

            if None in container:
                res.update(self.get(instance_tuple[1:], container[None], depth + 1))

        return res

    def add_recursive(self, s):
        self.add(s)
        for c in s.get_children_recursive():
            self.add(c)

class MonitorCheckpoint(object):
    def __init__(self, definitive_states, watched_events):
        self.definitive_states = definitive_states
        self.watched_events = watched_events

def globals_line(p):
    l = ",".join(i for i in p if i != "__builtins__")
    return "global " + l  + "\n" if l else ""

def instance_is_less_specific(i1, i2):
    for i in range(len(i1)):
        if i1[i] is not None and i2[i] != i1[i]:
            return False
    return True

def instance_compatible(new_instance, slices):
    for other_s in slices:
        if instance_is_less_specific(new_instance, other_s.get_slice_instance()):
            return False

    return True

class Monitor(object):
    """ The Monitor class is used to monitor a program against a given
        property. """
    cur_id = 0

    def __init__(self, eventManager, prop=None):
        """ This constructor expects a property to check against the monitored
            program. """

        Monitor.cur_id += 1

        ## ***WARNING*** ##
        # If you add, remove or modify variables here, also check whether they
        # needs to be handled in checkpoint(). Also add them in delete().

        self.scenario           = None
        self.checkpoints        = {} # stores known checkpoints for this property
        self.prop               = None
        self.bp_by_evt_name     = Dict()
        self.interface          = MonitorInterface(self)
        self.states             = set()
        self.states_by_name     = {}
        self.initial_state      = None
        self.quiet              = False
        self.tmpname            = "_r"
        self.registered_events  = {}
        self.step_by_step       = False
        self.definitive_states  = set()
        self.root_slice         = None
        self.slices_by_instance = None
        self.trigger_event_hit  = 0
        self.externToRealCheckpointId = {}
        self.lastCheckpointId   = 0
        self.event_manager      = eventManager
        self.watched_events     = Counter()
        self.updated_slices     = {}
        self.initialCheckpoint  = None

        eventManager.set_callback(self.trigger_event)

        self.monitor_id         = Monitor.cur_id

        self.stop_is_debugger_shell = True # if a StopExecution is caught, drop
                                            # to the debugger's shell.
        if prop:
            self.set_property(prop)

    def checkpoint(self, checkpoint_id=None):
        # WARNING: checkpoint_id should be a nonzero and strictly increasing number

        realCid = self.lastCheckpointId + 1

        if checkpoint_id:
            self.externToRealCheckpointId[checkpoint_id] = realCid


        self.checkpoints[realCid] = MonitorCheckpoint(
            definitive_states=set(self.definitive_states),
            watched_events=Counter(self.watched_events)
        )

        self.lastCheckpointId = realCid

        self.root_slice.checkpoint(realCid)
        self.event_manager.checkpoint(realCid)

        return self.lastCheckpointId

    def restore_instrumentation(self):
        self.event_manager.restore_instrumentation()

    def restore_checkpoint(self, checkpoint_id, isRealCid=False):
        # WARNING don't forget to register slices again when restoring a checkpoint

        realCid = (checkpoint_id
                            if isRealCid
                            else self.externToRealCheckpointId[checkpoint_id])

        c = self.checkpoints[realCid]
        self.definitive_states = set(c.definitive_states)
        self.watched_events = Counter(c.watched_events)

        self.root_slice.restore_checkpoint(realCid)
        self.slices_by_instance = SlicesByInstance(self.root_slice) # FIXME: costly as hell
        self.set_root_slice(self.root_slice)
        self.event_manager.restore_checkpoint(realCid)

        if not self.quiet:
            print("-- monitor restored --")
            self.print_state()

        self.emit(events.checkpoint_restart, ())

    def drop_checkpoint(self, checkpoint_id, isRealCid=False):
        realCid = (checkpoint_id
                            if isRealCid
                            else self.externToRealCheckpointId[checkpoint_id])

        del self.checkpoints[realCid]
        self.root_slice.drop_checkpoint(realCid)
        #self.slices_by_instance.drop_checkpoint(realCid)
        self.event_manager.drop_checkpoint(realCid)

    def remove_checkpoint(self, checkpoint_id):
        del self.checkpoints[checkpoint_id] # FIXME: untested

    def __exit__(self, ty, value, traceback):
        self.delete()

    def __del__(self):
        self.delete()

    def set_quiet(self, b=True):
        """ Sets the monitor quiet or not """
        self.quiet = b

    def delete(self):
        self.event_manager.delete()
        self.checkpoints        = None
        self.prop               = None
        self.scenario           = None
        self.bp_by_evt_name     = None
        self.interface          = None
        self.states_by_name     = None
        self.states             = None
        self.initial_state      = None
        self.tmpname            = None
        self.quiet              = None
        self.registered_events  = None
        self.step_by_step       = None
        self.root_slice         = None
        self.definitive_states  = None
        self.slices_by_instance = None
        self.lastCheckpointId   = None
        self.event_manager      = None
        self.externToRealCheckpointId = None
        self.watched_events     = None
        self.updated_slices     = None
        self.initialCheckpoint  = None
        self.name               = ""

        self.stop_is_debugger_shell = False

    def set_property(self, prop):
        """ Sets the property to check the program against. """
        self.prop = prop
        self.prepare_states()

    def scenario_bind_evt_to_state(self, moment, scenario_evt, state_name):
        try:
            scenario_event_set = self.scenario[moment][state_name]
        except KeyError:
            scenario_event_set = set()
            self.scenario[moment][state_name] = scenario_event_set

        scenario_event_set.add(scenario_evt)

    def set_scenario(self, scenario):
        """ Sets the property to check the program against. """
        self.scenario = {
            "entering": {},
            "leaving": {}
        }

        for scenario_evt in scenario.events:
            scenario_evt.state = self.states_by_name[scenario_evt.state_name] if scenario_evt.state_name else None

            moment = "entering" if scenario_evt.entering else "leaving"

            if scenario_evt.slice_policy == ScenarioEvent.ALL_SLICES:
                self.scenario_bind_evt_to_state(moment, scenario_evt, ScenarioEvent.ALL_SLICES)
            elif scenario_evt.accepting is None:
                self.scenario_bind_evt_to_state(moment, scenario_evt, scenario_evt.state_name)
            else:
                for state in self.states:
                    if state.accepting == scenario_evt.accepting:
                        self.scenario_bind_evt_to_state(moment, scenario_evt, state.name)

        self.scenario_env = dict()
        self.scenario_env["monitor"] = self.interface

        for (k, v) in scenario.initialization.items():
            self.scenario_env[k] = v



    def init_env(self, s):
        """ Called internally. Prepares the environment by calling the code of
            the property's initialization block. """

        env = dict(self.prop.initialization)
        assert env is not None, "env is None in init_env"
        s.set_env(env)

        while self.tmpname in env:
            self.tmpname += "_"

    def prepare_states(self):
        """ Called internally. Maps state name -> state data for all the states
            of the property. """

        state_star_non_accepting = None
        state_star_accepting     = None

        for state in self.prop.states:
            if state.name == "*":
                if state.accepting is True or state.accepting is None:
                    state_star_accepting = state.transitions

                if state.accepting is False or state.accepting is None:
                    state_star_non_accepting = state.transitions
            else:
                self.states_by_name[state.name] = state
                self.states.add(state)
                if state.initial:
                    self.initial_state = state

        if not self.initial_state:
            if "init" not in self.states_by_name:
                error(
                    "Missing initial state. " +
                    "Your property must have an initial state named 'init'."
                )

                sys.exit(2)

            self.initial_state = self.states_by_name["init"]

        for state in self.prop.states:
            if state.import_transitions:
                try:
                    transitions = self.states_by_name[state.import_transitions].transitions
                    for trans in transitions:
                        state.transitions.add(copy.copy(trans))
                except KeyError:
                    error(
                        "Missing state " + state.import_transitions +
                        " when importing transition in state " + state.name
                    )

            if state_star_accepting and state.accepting and state_star_accepting != state.transitions:
                for trans in state_star_accepting:
                    state.transitions.add(copy.copy(trans))

            elif not state.accepting and state_star_non_accepting:
                for trans in state_star_non_accepting:
                    state.transitions.add(copy.copy(trans))

            for trans in state.transitions:
                trans.begin_state = state

    def run(self):
        """ Launches the monitor. """

        if not self.prop:
            raise MonitorException(
                "A property must be loaded into the monitor before running it."
            )

        self.prop.initialization["debugger_shell"] = lambda sel: sel.debugger_shell()
        self.prop.initialization["stop_execution"] = lambda sel: sel.stop_execution()

        s = AutomatonSlice(len(self.prop.slice_bindings))
        self.init_env(s)
        self.set_root_slice(s)

        assert s.env is not None, "s.env is None in run"

        if not self.quiet:
            self.show_env("Initialization:")

        self.trigger_event(None)
        self.initialCheckpoint = self.checkpoint()

    def reset(self):
        self.restore_checkpoint(self.initialCheckpoint, isRealCid=True)
        if not self.quiet:
            self.print_current_state()


    def active_states(self):
        return self.root_slice.get_tree_active_states() if self.root_slice else []


    def unwatch_events(self, s, oldevents):
        for evt in oldevents:
            self.watched_events[evt] -= 1
            if not self.watched_events[evt]:
                self.event_manager.unwatch(evt)

    def safe_execute_action(self, state, s):
        try:
            self.execute_action(state, s)
        except StopExecution:
            return True

        return False

 
    def watch_state(self, state, s):
        for trans in state.transitions:
            if not self.watched_events[trans.event]:
                self.event_manager.watch(trans.event)

            self.watched_events[trans.event] += 1
            s.watched_events.add(trans.event)

        return self.safe_execute_action(state, s)

    def unsafe_set_current_state(self, s, new_state):
        """ Called internally. Replaces current states by given new states.
            Can throw a StopExecution exception.
        """

        #print("unsafe_set_current_state", s.get_current_state().name if s.get_current_state() else None, new_state.name)

        old_state = s.get_current_state()

        if not self.quiet:
            self.updated_slices[s.get_slice_instance()] = old_state

        oldevents = s.watched_events
        s.watched_events = set()

        s.set_current_state(new_state)

        if old_state == new_state:
            stop = self.safe_execute_action(new_state, s)
        else:
            stop = self.watch_state(new_state, s)


        if old_state:
            self.states_involved_during_event["leaving"].add(old_state)

            if old_state.accepting:
                self.accepting_states_have_been_involved_during_event["leaving"] = True
            else:
                self.non_accepting_states_have_been_involved_during_event["leaving"] = True

            stop = self.trigger_scenario_events("leaving", old_state, old_state, new_state, s) or stop

        if new_state:
            if new_state.accepting:
                self.accepting_states_have_been_involved_during_event["entering"] = True
            else:
                self.non_accepting_states_have_been_involved_during_event["entering"] = True

            self.states_involved_during_event["entering"].add(new_state)
            stop = self.trigger_scenario_events("entering", new_state, old_state, new_state, s) or stop

        self.emit(events.state_changed, (s, new_state))

        if old_state and old_state != new_state and not self.root_slice.is_state_active(old_state):
            self.unwatch_events(s, oldevents)

        return stop

    def apply_scenario_evt(self, scenario_evt, s, old_state, new_state):
        if not scenario_evt.compiled_code:
            scenario_evt.compiled_code = self.compile_code(scenario_evt.code)

        self.scenario_env["old_state"] = old_state.name if old_state else ""
        self.scenario_env["new_state"] = new_state.name if new_state else ""
        self.scenario_env["current_slice"] = s

        try:
            exec(scenario_evt.compiled_code, self.scenario_env)

        except StopExecution:
            return True

        except Exception as exc:
            error(
                "Failed to execute the scenario event " +
                str(scenario_evt) + ".",
                exc
            )
            raise exc

        return False

    def trigger_scenario_events(self, moment, state, old_state, new_state, s):
        #print("trigger_scenario_events")
        stop = False
        if self.scenario:
            try:
                scenario_evts = self.scenario[moment][state.name]
            except KeyError:
                return stop

            for scenario_evt in scenario_evts:
                if scenario_evt.slice_policy == ScenarioEvent.ONE_SLICE:
                    if scenario_evt in self.scenario_triggered_by_moment[moment]:
                        continue
                    self.scenario_triggered_by_moment[moment].add(scenario_evt)

                elif scenario_evt.slice_policy == ScenarioEvent.ALL_SLICES:
                    print("FIXME: This should not happen")
                    continue

                stop = self.apply_scenario_evt(scenario_evt, s, old_state, new_state) or stop
        return stop

    def has_several_slices(self):
        return self.root_slice.has_children()

    def get_slices(self):
        yield self.root_slice
        for s in self.root_slice.get_children_recursive():
            yield s

    def updated_slice_string(self, s):
        try:
            begin_state = self.updated_slices[s.get_slice_instance()]
        except KeyError:
            return ""

        if begin_state:
            return " (from " + begin_state.name + ")"

        return ""

    def print_current_state(self):
        global STATES_COLOR, NON_ACCEPTING_COLOR, SLICES_COLOR
        """ Prints current states. """
        i = 1

        if self.has_several_slices():
            print_step("Current state (monitor #" + str(self.monitor_id) + "):")

            for s in self.get_slices():
                print(colored(SLICES_COLOR, "  Slice " + str(i)), "<", end="")
                print(*s.get_slice_instance(), sep=", ", end="")
                state = s.get_current_state()
                print(
                    ">: ",
                    colored(STATES_COLOR, state.name),
                    self.updated_slice_string(s),
                    colored(NON_ACCEPTING_COLOR, " non-accepting") if state.accepting == False else "",
                    sep=""
                )

                self.show_env_slice(s)

                i += 1

            if self.definitive_states:
                print(
                    "    Final slices: " + colored(
                        STATES_COLOR,
                        list_states(self.definitive_states)
                    )
                )
        else:
            print_step(
                "Current state (monitor #" + str(self.monitor_id) + "): " + colored(
                    STATES_COLOR,
                    list_states(self.definitive_states.union({self.root_slice.get_current_state()}))
                ) + self.updated_slice_string(self.root_slice)
            )

        if not self.quiet:
            self.updated_slices = {}

    def show_env_slice(self, s, indent=""):
        env = s.get_env()
        for key in env:
            if key != "__builtins__" and not callable(env[key]):
                print(
                    "    " + indent +
                    colored(ENV_KEY_COLOR, key) + ": " +
                    str(env[key])
                )


    def show_env(self, title=""):
        """ Prints the current environment. """
        global ENV_KEY_COLOR

        if title:
            print_step(title)

        i = 1

        for s in self.get_slices():
            if self.has_several_slices():
                print("  Slice " + str(i))

            self.show_env_slice(s, indent="    ")
            i += 1

        print("")

    def print_state(self):
        """ Prints monitor's current state by printing current states and the
            environment. """
        self.print_current_state()
        self.show_env()

    def execute_action(self, action_container, s):
        """ Called internally. If a transition or a state contains an action,
            execute this action when it is taken / reached (respectively). """
        if not action_container.action_name:
            return

        env = s.get_env()

        if action_container.action_name in env:
            return env[action_container.action_name](self.interface, s)

        error(
            "Action " + action_container.action_name + " is not defined.",
            self
        )

    def to_state(self, nstate, point):
        if not nstate:
            return None

        try:
            return self.states_by_name[nstate]
        except KeyError:
            error(
                "Error while executing the " + point +
                " point of the transition: the returned value, " +
                nstate + ", is not a state of the property."
            )
            return None

    def take_transition_point(self, s, trans, point, params):
        """ Called internally. Handles the end of a transition, with eventual
            code to execute. point is either "success" or "failure", when the
            guard succeeded or failed, respectively. """

        #print("take_transition_point")

        state = None

        trans_point = getattr(trans, point)
        if trans_point:
            if trans_point.inline_action:

                # We copy the environment so that the event parameters are not kept.
                env = {}

                for (p, v) in zip(self.prop.slice_bindings, s.get_slice_instance()):
                    env[p] = v

                env.update(s.get_env())
                env.update(params)

                if not trans_point.compiled_inline_action:
                    code = globals_line(env) + trans_point.inline_action
                    try:
                        trans_point.compiled_inline_action = self.compile_code(code)
                    except Exception as exc:
                        error(
                            "Failing to parse the " + point +
                            " point of the transition",
                            self,
                            exc
                        )

                try:
                    exec(trans_point.compiled_inline_action, env)
                except Exception as exc:
                    traceback.print_exc()
                    error(
                        "An error occured when executing the action of the " + point +
                        " point of the transition",
                        self,
                        exc
                    )


                #FIXME: check usage of tmpname in the compiled code, could
                # conflict.

                nstate = env[self.tmpname]
                del env[self.tmpname]

                for (key, val) in env.items():
                    if key not in params:
                        s.set_env_key(key, val)

                state = self.to_state(nstate, point)

            state2 = self.to_state(self.execute_action(trans_point, s), point)

            state = state2 if state2 else (state if state else self.to_state(trans_point.state, point))

            if state:
                self.emit(events.transition_taken, (trans, point, state))

            return state

        return None

    def compile_code(self, python_code):
        """ Called internally. returns the compiled version of the code, storing
            its return value inside env[self.tmpname]. """

        code = "def " + self.tmpname + "():\n" + (
                    "\n" +
                    python_code
                ).replace("\n", "\n\t") + "\n" + self.tmpname + " = " + self.tmpname + "()\n"

        return compile(code, "<string>", "exec")

    def eval(self, val):
        return eval(val, self.root_slice.get_env())

    def compile_guard(self, trans, evt_name):
        """ Called internally. Ensures the guard of the transition is compiled.
            evt_name is here for easing the debug of a property, and isn't
            necessary from an algorithmic point of view. """

        try:
            trans.compiled_guard = self.compile_code(trans.guard)
        except Exception as exc:
            error(
                "Failed to parse guard while handling event " +
                "\033[0;1m" + evt_name + "\033[0;0m in state " +
                "\033[0;1m" + trans.begin_state.name + "\033[0;0m.\n" +
                "def " + self.tmpname + "():\n" + (
                    "\n" +
                    trans.guard
                ).replace("\n", "\n\t") + "\n" +
                self.tmpname + " = " + self.tmpname + "()\n",
                self,
                exc
            )

        # We compile a code in this form:
        #
        #     def _r():
        #         <user's code with returns>
        #     _r = _r()
        #
        # so we have what the user's code returned in env["_r"]
        #
        # We do this that way to work around the lack of anonymous
        # functions in python

    def apply_transition(self, s, runtime_event, trans, env):
        stop = False
        next_state = None

        assert env is not None

        # if there is a guard, we execute it
        if not trans.compiled_guard and trans.guard:
            self.compile_guard(trans, runtime_event.formalEvent)

        params = runtime_event.parameters

        if trans.compiled_guard:
            for (p, v) in zip(self.prop.slice_bindings, s.get_slice_instance()):
                env[p] = v

            for p in params:
                env[p] = params[p]

            try:
                exec(trans.compiled_guard, env)
            except Exception as exc:
                error(
                    "Failed to execute guard while handling event " +
                    "\033[0;1m" + runtime_event.formalEvent.name + "\033[0;0m in state "  +
                    "\033[0;1m" + str(trans.begin_state) + "\033[0;0m.",
                    self,
                    exc
                )

            guard_res = env[self.tmpname]

            if guard_res is not None:
                try:
                    next_state = self.take_transition_point(
                        s,
                        trans,
                        "success" if guard_res else "failure",
                        params
                    )
                except StopExecution:
                    stop = True
        else:
            next_state = self.take_transition_point(s, trans, "success", params)

        return (stop, next_state)

    def take_transition(self, s, runtime_event, trans, env, begin_state, param_instance, slices):
        if instance_is_less_specific(param_instance, s.get_slice_instance()):
            return self.apply_transition(s, runtime_event, trans, env)

        new_s = s.fork()

        new_slice_instance = list(new_s.get_slice_instance())

        for i in range(len(self.prop.slice_bindings)):
            if param_instance[i] is not None:
                new_slice_instance[i] = param_instance[i]

        new_s.set_slice_instance(new_slice_instance)

        if instance_compatible(new_slice_instance, slices):
            (stop, next_state) = self.apply_transition(
                                    s,
                                    runtime_event,
                                    trans,
                                    env
                                )

            if next_state is not None: # and next_state != begin_state:
                new_s.set_current_state(next_state)
                new_s.set_env(s.get_env_copy())

            self.slices_by_instance.add(new_s)

            if next_state is not None:
                stop = self.watch_state(next_state, new_s) or stop

            next_state = new_s.get_current_state() # FIXME is this line necessary?

            if not self.quiet:
                self.updated_slices[s.get_slice_instance()] = begin_state

            if next_state.final:
                self.definitive_states.add(new_s.get_current_state())
                self.unregister_slice(new_s)

            return (stop, None)
        else:
            new_s.remove()

        return (False, None)

    def param_subset_relevant_to_event(self, params, event):
        return {k: params[k] for k in event.get_parameter_names() if k in params}

    def apply_event_slice(self, s, runtime_event, param_instance, slices):
        dest_state = None

        assert s.env is not None, "s.env is None in apply_event_slice"

        env = s.get_env_copy()

        assert env is not None, "env is None in apply_event_slice"

        took_transition = False

        stop = False

        current_state = s.get_current_state()

        evt = runtime_event.formalEvent

        for trans in current_state.transitions:
            if trans.event != evt:
                # FIXME inefficient?
                continue

            took_transition = True

            (new_stop, next_state) = self.take_transition(
                s,
                runtime_event,
                trans,
                env,
                current_state,
                param_instance,
                slices
            )

            stop = new_stop or stop

            if next_state is not None:
                dest_state = next_state
                break


        if dest_state is None:
            return stop

        return self.set_current_state(s, dest_state) or stop

    def get_param_instance(self, event_params):
        res = []

        for var in self.prop.slice_bindings:
            res.append(event_params[var] if var in event_params else None)

        return res

    def unregister_slice(self, s):
        self.slices_by_instance.remove(s)
        for child_s in s.get_children_recursive():
            self.unregister_slice(child_s)
        s.remove()

    def register_slice(self, s):
        self.slices_by_instance.add(s)
        for child_s in s.get_children_recursive():
            self.slices_by_instance.add(s)

    def set_root_slice(self, s):
        self.root_slice = s
        self.slices_by_instance = SlicesByInstance(s)

    def set_current_state(self, s, state):
        """ Called internally. Calls unsafe_set_current_state, while handling
            the SetCurrentState exception. Yep, this can loop infinitely. """

        stop = False

        try:
            stop = self.unsafe_set_current_state(s, state)
        except SetCurrentState as exc:
            return self.set_current_state(s, exc.state)

        if s.get_current_state().final:
            # forget the slice as requested by the property for memory
            # consumption and / or correctness.

            self.definitive_states.add(s.get_current_state())
            self.unregister_slice(s)

        return stop

    def trigger_event(self, runtime_event):
        """ Called from within a gdb breakpoint, when the event evt_name
            happens. """

        if not self.quiet:
            print(colored(SLICES_COLOR, "\nEvent:"), runtime_event)

        self.emit(events.event_applying, (runtime_event,))

        self.scenario_triggered_by_moment = {
            "leaving": set(),
            "entering": set()
        }

        self.accepting_states_have_been_involved_during_event = {
            "leaving": False,
            "entering": False
        }

        self.non_accepting_states_have_been_involved_during_event = {
            "leaving": False,
            "entering": False
        }

        self.states_involved_during_event = {
            "leaving": set(),
            "entering": set()
        }

        stop = False

        if runtime_event:
            param_instance = self.get_param_instance(runtime_event.parameters)
            slices = self.slices_by_instance.get(param_instance)

            for s in slices:
                try:
                    new_stop = self.apply_event_slice(
                        s,
                        runtime_event,
                        param_instance,
                        slices
                    )

                    stop = stop or new_stop
                except SetCurrentState as exc:
                    (new_state, stop) = (exc.state, exc.stop)
                    stop = self.set_current_state(s, new_state) or stop

            if not self.quiet:
                self.print_current_state()
        else:
            self.set_current_state(self.root_slice, self.initial_state)

        if self.scenario:
            for moment in ("leaving", "entering"):
                sc_all_slices = None
                try:
                    sc_all_slices = self.scenario[moment][ScenarioEvent.ALL_SLICES]
                except KeyError:
                    pass

                if sc_all_slices:
                    for scenario_evt in sc_all_slices:
                        if scenario_evt.accepting is None:
                            if {scenario_evt.state} == self.states_involved_during_event[moment]:
                                stop = self.apply_scenario_evt(scenario_evt, None, None, None) or stop
                        elif scenario_evt.accepting:
                            if not self.non_accepting_states_have_been_involved_during_event[moment]:
                                stop = self.apply_scenario_evt(scenario_evt, None, None, None) or stop
                        else:
                            if not self.accepting_states_have_been_involved_during_event[moment]:
                                stop = self.apply_scenario_evt(scenario_evt, None, None, None) or stop

        self.accepting_states_have_been_involved_during_event = None
        self.non_accepting_states_have_been_involved_during_event = None
        self.states_involved_during_event = None

        self.scenario_triggered_by_moment = None

        self.emit(events.event_applied, (runtime_event,))

        if stop:
            if self.stop_is_debugger_shell:
                return DEBUGGER_SHELL
            else:
                return STOP

        if self.step_by_step:
            return DEBUGGER_SHELL

        return CONTINUE

    def emit(self, event_type, event_params):
        """ When something happens in the monitor like a current states change
            or a transition taking, this method is called so these events can
            be monitored.
        """

        if event_type in self.registered_events:
            for callback in self.registered_events[event_type]:
                callback(*event_params)

    def register_event(self, event_type, callback):
        """ Register a callback for this event type.

            See the help for MonitorInterface.register_event for more
            information.

            Notice: contrary to MonitorInterface.register_event, event_type is
            not a string but an int given by monitorcommon.event[event_name].
        """

        if event_type not in self.registered_events:
            self.registered_events[event_type] = set()

        self.registered_events[event_type].add(callback)

    def unregister_event(self, event_type, callback):
        """ Unregister this callback for this event type.
            See also register_event.
        """

        if event_type in self.registered_events:
            self.registered_events[event_type].discard(callback)

