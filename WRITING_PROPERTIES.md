# The Property Model

Properties are verified against an execution trace composed of events produced by instrumenting the program.

In Verde, properties are described in a model close to Finite State Machines, extended with an environment used to store values (a memory). This model is described in our papers (see Section publication in [README.md](README.md)). Here is a brief overview.
Examples of properties can be found in the experiments.

In this model, a property is an object composed of:
 - a set of states;
 - an initial state;
 - a set of transitions;
 - an initial environment which maps initial values to variables used in the property;
 - an optional set of slicing parameters. The property will be checked against each instance of this set of parameters rather than on the whole execution.

Before the reception of any event, the current state of the property is the initial state.
Transitions are taken as relevant events are received. When a transition is taken, the current state changes.
The exact behavior of the transitions is described later.

## States

A state:
 - has a **unique name**.
 - can be either **accepting** or **non-accepting**. By default, states are accepting.
 - can be associated to an **action**. If a state is associated to an action, this action is executed each time the state is reached.

## Transitions

In Verde, transitions have one start state and two destinations states: a destination for failure and a destination for success (see below). A transition has:
 - The **name of the event** that triggers the transition. For function calls, the event name is the name of the function. For variable accesses, the name is the name of the variable.
 - an indication on when the event is taken: before or after the event happens (e.g. before a function call, or when the function returns).
 - an optional **list of parameters**, which is a list of variables which can be accessed from the debugger at the moment the event happens and are used in the guard. Events are optionally typed.
 - an optional **guard**, which is a code that is executed right after the function is called. This code should not have side effects and should return one of these values:
   - *Success* (`True`): the transition is taken, and succeeded.
   - *Failure* (`False`): the transition is taken, and failed.
   - *Not concerned* (`None`): the transition is not taken.
  This code can access to variables in the environment of the property and to the monitored parameters of the event.
 - an optional **success block**, which is composed of:
   - an optional code block which has read/write access to the environment and read access to the monitored parameters, used to update the environment.
   - an **action** to execute, or a **state to reach**, when leaving the transition.
 - an optional **failure block**, which is formed like a success block.

When the event of a transition happens while the property is in the start start of this transition:
 - the guard is tested;
 - according to the return value of the guard:
   - if *Not concerned*: the transition is ignored;
   - if *Success*: the success block is considered;
   - if *Failure*: the failure block is considered;
 - for the considered block, if defined:
   - the update code of the block is executed, if any;
   - the action associated to the block is executed if any. Otherwise, the state is updated.

## Writing a Property in Verde

The property file begins with an optional `Initialization` block containing a Python code that initializes the memory environment of the property. For instance:

    initialization {
        N = 0
        s = ""
    }

Note. The Python code must be well indented. Any Python code block must be enclosed with braces, **the last one being alone in its own line**. Code blocks are the only place where whitespaces are significant.

Then, follows a list of states containing at least the initial state which must be called `init`:

    state init

    [...]

    state sink non-accepting sink_reached()

To declare a state, write the keyword `state` followed by its name, an optional `accepting` / `non-accepting` flag and an optional name of action (then followed by `()`). If not given, the state is accepting by default and has no action. The action is executed whenever the state is reached.

The action can be:
 - `debugger_shell`, which drops the user to the debugger command line;
 - `stop_execution`, which stops the execution and the debugger;
 - a Python function defined in a separate file (loaded using the command `verde load-functions`).

Inside each state, declare a list of transitions like the following one:

    state init {
        transition {
            event push(producer_id, value, begin_queue, end_queue, size_max) {
                if begin_queue <= end_queue:
                    queue_size = end_queue - begin_queue + 1
                else:
                    queue_size = end_queue + size_max - begin_queue + 1

                print("GUARD: nb push: " + str(N));
                return queue_size < size_max
            }

            success {
                N = N + 1
            } init

            failure sink
        }
    }

This is an example of a transition which does many things, but many parts are not mandatory. Lets step in this example progressively.

First, a transition is declared with the keyword `transition`, alone on its line. Then, everything related to the transition is indented under the keyword `transition`. This is presentational, but highly recommended.

Following the transition declaration keyword, the event to monitor is described, beginning with the `event` keyword. Then, the event name follows. As an event is just a function call, the event name is the name of the function which must be monitored. Follows an optional list of parameter names that are names of variables that can be accessed from the debugger at the moment of the event:

    event push(producer_id, value, begin_queue, end_queue, size_max)

After the declaration of the event, a guard written in Python and enclosed in braces can be specified:

    {
        if begin_queue <= end_queue:
            queue_size = end_queue - begin_queue + 1
        else:
            queue_size = end_queue + size_max - begin_queue + 1

        print("GUARD: nb push: " + str(N));
        return queue_size < size_max
    }

There are three cases:
 - **The guard returns `True`**. In this case, the transition succeeds.
 - **The guard returns `False`**. In this case, the transition fails.
 - **The guard returns `None`**. The transition is then considered as not relevant, and is not taken.

Any other return value from the guard is unspecified. The guard must not modify the environment: the modifications could be kept or not, this is unspecified. In other words, the code of the guard code should be "pure".

If the transition succeeds, the success block is used, if any. If it fails, the failure block is used, if any.

Below, an example of a `success` block:

    success {
        N = N + 1
    } init

The `success` and the `failure` blocks are optional. The `success` (resp. `failure`) keyword declares the block, and is optionally followed by a Python code block enclosed with braces, used to update the environment of the property, followed by either the name of a state to reach or the name of an action to execute. The name of the action is followed by `()` (allowing the distinction between a state name and an action name at the syntactical level).

# The Scenario

Scenarios are a way of making decisions when the current state of a property changes.
They bind Monitor events like "entering state X", "leaving state X", "entering non-accepting state", "leaving accepting state" to actions. These actions can be used to control the debugger or the execution of the program, or to print some information about the execution.

In Verde, these actions are written in Python.

Here are some examples of scenarios:

 - The following scenario drops to the debugger shell when entering in a non-accepting state.

        on entering non-accepting state {
            print("Scenario: a non-accepting state has been hit! (" + old_state + " → " + new_state + ")")
            print("Stopping the execution.")
            print(" Now, you can try to see what happened using the debugger, fix the C code, compile and re-run the experiment when you are done.")
            print(" For example, try to type “list”, “backtrace” or “where”. You can use “quit” to quit GDB.")
            print(" Also, don't forget to later run the experiment with the alternative scenario by editing stop.scn if you want to go a bit further!")
            monitor.debugger_shell()
        }

 - The following scenario takes a checkpoint when entering in a non-accepting state:

        on entering non-accepting state {
            c = checkpoint()

            def called_when_checkpoint_ready():
                cid = checkpoint_get_id(c)
                print(" Checkpoint on this breakage:", cid, "To restore, type verde checkpoint-restart", cid)

            after_breakpoint(called_when_checkpoint_ready)
        }

 - The following scenario is more elaborated and was written for a Sudoku solver. It takes checkpoints when entering accepting states, and when the property used with this scenario (see below) reaches an incorrect state, the scenario guides the developer and lets him / her chose a checkpoint to restore and sets watchpoints on cells to get more information during the second run.

        initialization {
            last_checkpoint = None
            watchpoint_enabled = False
            non_accepting_step = 0
            watchpoints = []
            checkpoints_on_board_ready = dict()
            checkpoints_by_access = []

            def watchpoint_hit(cboard, i, j):
                print("Checkpointing before a modification to the cell ({}, {})".format(i, j))
                checkpoints_by_access.append(
                    (i, j, checkpoint())
                )
        }

        on entering state board_ready {
            board = current_slice.get_parameter_value("board", monitor.get_slice_bindings())

            if board in checkpoints_on_board_ready:
                drop_checkpoint(checkpoints_on_board_ready[board])

            print("new board created, checkpointing.")
            checkpoints_on_board_ready[board] = checkpoint()
        }

        on entering state solved_board_eliminated {
            board = current_slice.get_parameter_value("board", monitor.get_slice_bindings())

            if board in checkpoints_on_board_ready and checkpoints_on_board_ready[board]:
                drop_checkpoint(checkpoints_on_board_ready[board])
                del checkpoints_on_board_ready[board]
        }

        on entering state dead_board_eliminated {
            board = current_slice.get_parameter_value("board", monitor.get_slice_bindings())

            if board in checkpoints_on_board_ready and checkpoints_on_board_ready[board]:
                drop_checkpoint(checkpoints_on_board_ready[board])
                del checkpoints_on_board_ready[board]
        }

        on entering state board_eliminated {
            board = current_slice.get_parameter_value("board", monitor.get_slice_bindings())

            if board in checkpoints_on_board_ready and checkpoints_on_board_ready[board]:
                drop_checkpoint(checkpoints_on_board_ready[board])
                del checkpoints_on_board_ready[board]
        }


        on entering accepting state for all slices {
            global non_accepting_step

            if non_accepting_step:
                return

            print("Entering accepting state, checkpointing")
            global last_checkpoint
            if last_checkpoint is not None:
                drop_checkpoint(last_checkpoint)

            last_checkpoint = checkpoint()
        }

        on entering non-accepting state for one slice {
            def suite1():
                global watchpoint_enabled
                global non_accepting_step
                non_accepting_step += 1

                if non_accepting_step > 2:
                    return

                env = monitor.get_env()
                cboard = current_slice.get_parameter_value("board", monitor.get_slice_bindings())

                try:
                    b = env["boards"][cboard]
                except KeyError:
                    print("Entered in a non-accepting state. The board was not loaded in the monitor.")
                    return

                from functools import partial
                import gdb
                print("Entered in a non-accepting state: {}. Current board ({}), before setting incorrect value, is:".format(new_state, cboard))

                for l in b:
                    print(l)

                failure = env["failure"]
                i = failure["i"]
                j = failure["j"]

                if i != -1:
                    print(
                        "The program Trying to set board[{}][{}] from {} to {}: {}".format(
                            i,
                            j,
                            b[i][j],
                            failure["v"],
                            failure["reason"]
                        )
                    )

                if not watchpoint_enabled: input("")

                print("Current position: ")
                gdb.execute("where")
                gdb.execute("l")

                if not watchpoint_enabled: input("")

                if watchpoint_enabled:
                    for w in watchpoints:
                        remove_watchpoint(w)

                    old_s = old_state
                    new_s = new_state
                    def list_access():
                        print("The property state went from {} to {}. These accesses have been collected:".format(old_s, new_s))

                        k = 1

                        for (i, j, c) in checkpoints_by_access:
                            cid = checkpoint_get_id(c)
                            print(" {}. ({}, {}) : checkpoint {}".format(k, i, j, cid))
                            k += 1

                        while True:
                            checkpoint_to_restore = input("Do you want to restore a checkpoint? (0 for no): ")
                            if checkpoint_to_restore == "0":
                                return

                            checkpoint_to_restore = int(checkpoint_to_restore) - 1
                            if checkpoint_to_restore < len(checkpoints_by_access):
                                restore_checkpoint(checkpoints_by_access[checkpoint_to_restore][2], stop=True)
                                break
                            else:
                                print("Sorry, this checkpoint does not exist.")

                    after_breakpoint(list_access)
                    monitor.debugger_shell()
                else:
                    print("I am going to restart from last checkpoint with watchpoints on the faulty board")
                    print("Restoring checkpoint cid={}".format(last_checkpoint.cid))

                    input("")

                    restore_checkpoint(last_checkpoint)

                    for i in range(env["board_height"]):
                        for j in range(env["board_width"]):
                            watchpoints.append(
                                watchpoint(
                                    "((board_t*){})->fields[{}][{}]".format(cboard, i, j),
                                    WATCHPOINT_WRITE,
                                    partial(watchpoint_hit, cboard, i, j)
                                )
                            )

                    print("Set {} watchpoints".format(len(watchpoints)))
                    watchpoint_enabled = True

            after_breakpoint(suite1)
        }

    This scenario is used with the following property, that is used to verify that the rules of Sudoku are respected and that the solver behaves correctly (for instance, the same number cannot appear in the same row, the same column, or the same square in a board ; a board that has been decided unsolvable by the solver should not receive further modifications):

        slice on board

        initialization {
            boards  = dict()

            failure = {"reason": "", "i": -1, "j": -1, "v": -1}

            data    = dict()
        }

        state init {
            transition {
                after event new_board() -> board : int {
                    return board != 0
                }

                success allocated_board
                failure allocation_failed
            }

            transition {
                before event copy_board(board as old_board : int) {
                    return (old_board in boards)
                }

                success init
                failure copy_from_non_existant_board
            }

            transition {
                after event copy_board(board as old_board : int) -> new_board as board : int {
                    data["_board"] = retrieve_program_board(board)
                    return board_eq(data["_board"], boards[old_board])
                }

                success {
                    boards[board] = data["_board"]
                } board_ready

                failure {
                    boards[board] = data["_board"]
                } bad_board_copy
            }
        }

        state allocated_board {
            transition {
                before event load_board(board)
                success loading_board
            }
        }

        state loading_board {
            transition {
                after event load_board(board : int) -> is_good : bool {
                    data["_board"] = retrieve_program_board(board)
                    if not is_good:
                        return False

                    return check_valid_board(data["_board"])
                }

                success {
                    boards[board] = data["_board"]
                } board_ready

                failure {
                    boards[board] = data["_board"]
                } failure_load
            }
        }

        state board_ready {
            transition {
                before event set_digit(board : int, i_row : int, i_col: int, value: int) {
                    return check_board_at(boards[board], i_row, i_col, value)
                }

                success {
                    boards[board][i_row][i_col] = value
                } board_ready

                failure {
                    boards[board][i_row][i_col] = value
                } dead_board
            }

            transition {
                after event is_dead_end(board : int) -> res : bool {
                    return res and is_dead_board(boards[board])
                }

                success dead_board

            }

            transition {
                after event is_complete(board : int) -> res : bool {
                    return res == check_complete(boards[board])
                }

                success {
                    if res: return "solved"
                } board_ready

                failure {
                    if res: return "board_incorrectly_evaluated_as_solved"
                } board_incorrectly_evaluated_as_not_solved
            }

            transition {
                before event myfree(arg 1 as board : int)
                success board_eliminated
            }

        }

        state dead_board accepting {
            transition {
                before event set_digit(board : int)
                success set_incorrect_after_dead
            }

            transition {
                after event is_dead_end(board : int) -> res : bool {
                    return res == True
                }

                success board_correctly_deamed_dead
            }

            transition {
                before event myfree(arg 1 as board : int)
                success dead_board_eliminated
            }
        }

        state board_correctly_deamed_dead accepting {
            transition {
                before event myfree(arg 1 as board : int)
                success dead_board_eliminated
            }
        }

        state failure_load non-accepting {
            transition {
                before event myfree(arg 1 as board : int)
                success dead_board_eliminated
            }

            transition {
                event set_digit()
                success operation_after_failure_load
            }
        }

        state operation_after_failure_load non-accepting {

        }

        state set_incorrect_after_dead accepting {

        }

        state dead_board_eliminated accepting {

        }

        state copy_from_non_existant_board non-accepting {

        }

        state bad_board_copy non-accepting {

        }

        state board_incorrectly_deamed_dead non-accepting {

        }

        state board_incorrectly_deamed_alive non-accepting {

        }

        state allocation_failed non-accepting {

        }

        state board_incorrectly_evaluated_as_solved non-accepting {

        }

        state board_incorrectly_evaluated_as_not_solved non-accepting {

        }

        state board_incorrectly_deamed_alive non-accepting {

        }

        state solved accepting {
            transition {
                before event myfree(arg 1 as board : int)
                success solved_board_eliminated
            }
        }

        state solved_board_eliminated accepting {

        }

        state board_eliminated accepting {

        }


    This property is used with this file of functions:

        board_width  = 9
        board_height = 9
        block_width  = 3
        block_height = 3
        valid_digits = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        import gdb

        def fail(reason, i, j, v):
            failure["reason"] = reason
            failure["i"] = i
            failure["j"] = j
            failure["v"] = v
            return False

        def check_board_line(board, i, j, v):
            line = []

            for k in range(board_width):
                c = v if k == j else board[i][k]

                if c != 0 and c in line:
                    return fail("Digit already present in the line", i, j, v)

                line.append(c)

            return True

        def check_board_block(board, i, j, v):
            block = []

            start_i = i - (i % block_height)
            start_j = j - (j % block_width)

            for ki in range(start_i, start_i + block_height):
                for kj in range(start_j, start_j + block_width):
                    c = v if kj == j and ki == i else board[ki][kj]
                    if c != 0 and c in block:
                        return fail("Digit already present in the block", i, j, v)

            return True

        def check_board_col(board, i, j, v):
            col = []
            for k in range(board_height):
                c = v if k == i else board[k][j]

                if c != 0 and c in col:
                    failure_reason = "Digit already present in the column"
                    failure_i = i
                    failure_j = j
                    return False

                col.append(c)

            return True


        def check_valid_digit(i, j, v):
            if v == 0 or v in valid_digits:
                return True

            return fail("Incorrect digit", i, j, v)

        def check_board_at(board, i, j, v):
            return (
                check_valid_digit(i, j, v)     and
                check_board_line(board, i, j, v)  and
                check_board_col(board, i, j, v)   and
                check_board_block(board, i, j, v)
            )

        def check_valid_board(board):
            for i in range(board_height):
                for j in range(board_width):
                    if not check_valid_digit(i, j, board[i][j]):
                        return False

                if not check_board_line(board, i, 0, board[i][0]):
                    return False

            for j in range(board_width):
                if not check_board_col(board, 0, j, board[0][j]):
                    return False

            for i in range(3):
                for j in range(3):
                    if not check_board_block(board, i * 3, j * 3, board[i][j]):
                        return False

            return True

        def new_board():
            return [
                [ 0 for i in range(board_width) ] for j in range(board_height)
            ]

        def retrieve_program_board(board_address):
            board = new_board()
            getstr = "((board_t*) ({}))->fields[{{}}][{{{{}}}}]".format(board_address)
            for i in range(board_height):
                getstrl = getstr.format(i)
                for j in range(board_width):
                    board[i][j] = int(gdb.parse_and_eval(getstrl.format(j)))

            return board

        def board_eq(b1, b2):
            for i in range(board_height):
                for j in range(board_width):
                    if b1[i][j] != b1[i][j]:
                        return False
            return True

        def check_complete(board):
            for i in range(board_height):
                for j in range(board_width):
                    if board[i][j] == 0:
                        return False

            return True

        def possibilities(board, i, j):
            p = valid_digits.copy()

            for ki in range(board_height):
                try:
                    p.remove(board[ki][j])
                except ValueError:
                    pass

            for kj in range(board_height):
                try:
                    p.remove(board[i][kj])
                except ValueError:
                    pass

            start_i = i - (i % block_height)
            start_j = j - (j % block_width)

            for ki in range(start_i, start_i + block_height):
                for kj in range(start_j, start_j + block_width):
                    try:
                        p.remove(board[ki][kj])
                    except ValueError:
                        pass

            return p

        def is_dead_board(board):
            for i in range(board_height):
                for j in range(board_width):
                    if board[i][j] == 0:
                        if not possibilities(board, i, j):
                            return True
            return False

# Formal grammar of the syntax of Properties

Whitespace are not significant, excepted the mandatory new line character sequence before the closing brace. "body" is the begining non-terminal.

### Terminals
    NL                   :   a new line character sequence : "\n"

### Non-Terminals
    body                 ::= slicing ? initialization ? stateList
    bracesPythonCode     ::= "{" NL pythonFunctionBody NL "}"
    initialization       ::= "initialization" bracesPythonCode
    slicing              ::= "slice on (" parameterList ")"
    stateList            ::= ( state stateList ) ?
    state                ::= "state" STATE_NAME accepting ("{" transitionList "}") ?
    transition           ::= "transition" "{" event guard success maybeFailure "}"
    transitionList       ::= ( transition transitionList ) ?
    accepting            ::= ( "accepting" | "non-accepting" ) ?
    event                ::= "event" ( callEventTail | watchEventTail )
    callEventTail        ::= EVENT_NAME parameterList return ?
    watchEventTail       ::= (read | write | access) WATCH_ADDR parameterList
    return               ::= ( ":" | "->" ) PARAM_NAME
    parameterList        ::= ( "(" parameterListContent ")" ) ?
    param                ::= PARAM_NAME | "arg" NUMER "as" PARAM_NAME
    parameterListContent ::= ( param "," parameterListContent ) ?
    guard                ::= bracesPythonCode ?
    maybeFailure         ::= failure ?
    action               ::= ( FUNCTION_NAME "()" ) ?
    success              ::= "success" bracesPythonCode ? action ? STATE_NAME ?
    failure              ::= "failure" bracesPythonCode ? action ? STATE_NAME ?
