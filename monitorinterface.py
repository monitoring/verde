#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A module providing an interface which allows to manipulate the monitor.
"""


from monitorcommon import (
    StopExecution,
    SetCurrentState,
    events
)

class MonitorInterface(object):
    """ An instance of the MonitorActionInterface class is created by instance
        of the Monitor class. It is passed to user defined action functions to
        let them control and consult the monitor state.

        WARNING: This class exposes the monitor object and while the interface
        of this class is expected to be more or less backward / forward
        compatible, the monitor is expected to evolve and direct access to it
        might cause backward/forward compatibility issues. Please use methods of
        MonitorActionInterface whenever possible. """

    def __init__(self, monitor):
        """ This is the constructor of the class. A monitor must be given. """
        self.monitor = monitor
        self.globals = dict()

    def set_globals(self, g):
        """ Sets the dictionary in which functions will be found, if needed,
            when e.g. calling (un)register_event. """
        self.globals = g

    def debugger_shell(self):
        """ Raises an exception making the monitor drop to the debugger's
            shell. """
        self.monitor.stop_is_debugger_shell = True
        raise StopExecution(
            "user's function asked to drop to the debugger's shell"
        )

    def stop_execution(self):
        """ Raises an exception making the execution of the monitor stop the
            program's and the monitor's execution. """
        self.monitor.stop_is_debugger_shell = False
        raise StopExecution(
            "user's function asked to stop the execution of the program"
        )

    def get_slices(self):
        return self.monitor.get_slices()

    def get_env(self, s = None):
        """ Returns the environment dictionary of the property.

            WARNING: It is unspecified whether a modification to this dictionary
            will be reflected in the environment, and whether a modification to
            the environment will be reflected in the dictionary.
            Call get_env() each time you need to get the most up-to-date
            environment.

            If needed, use copy() to be sure to keep a static version of the
            environment correponding to the moment when this method is called,
            and the set_env_dict() and set_env_value() methods to modify the
            environment. """

        if s is None:
            for s in self.get_slices():
                return s.get_env()

        return s.env_globals

    def set_env_dict(self, s, new_env):
        """ Sets the environment of the property.

        WARNING: Please don't modify the given dictionary after passing it to
        this method. This causes undefined behavior. Use copy() if needed when
        passing the dictionary to set_env_dict(). new_env is not guaranteed to
        be a reference to the actual new dictionary """

        s.env_globals.clear()
        for p in new_env:
            s.env_globals[p] = new_env[p]


    def get_env_value(self, s, key):
        """ Returns the value of the given variable in the property's
        environment. Raises if the key is not present in the environment. """

        return s.env_globals[key]

    def set_env_value(self, s, key, value):
        """ Sets the value of the given variable in the property's
            environment. """

        s.env_globals[key] = value

    def get_env_keys(self, s, as_iterator=True):
        """ Returns the keys of the property's environment, as an iterator or a
            list, whether as_iterator is True or False, repectively. """

        if as_iterator:
            return s.env_globals.iterkeys()

        return s.env_globals.keys()

    def print_monitor_state(self):
        """ Prints the monitor's current state. """

        self.monitor.print_state()

    def get_current_states(self):
        """ Returns the set of the current states of the
            property.

            WARNING: Follow the same precautions as for
            the get_env() method. """

        res = set()

        res.update(self.monitor.active_states())
        res.update(self.monitor.definitive_states)

        return res

    def set_current_state(self, state):
        """ Sets the current state of (the root slice of) the property."""

        raise SetCurrentState(state, False)

    def get_states(self):
        """ Returns the set of the states of the property.

            WARNING: Follow the same precautions as for
            the get_env() method. """

        return self.monitor.states

    def get_slice_bindings(self):
        return self.monitor.prop.slice_bindings

    def set_transition_debug_function(self, fun_name=None):
        """ Specifies a user's function to call whenever a monitored action not
            taken in account in the current states of the property is called.
            no argument means the default: no user function is called when it
            happens.
        """

        self.monitor.set_transition_debug_function(
            None if fun_name is None else self.monitor.actions[fun_name]
        )

    def set_quiet(self, b="True"):
        """ Sets the monitor quiet or not."""

        if b is True or b == "True" or b == "true" or b == "1":
            b = True
        else:
            b = False

        self.monitor.set_quiet(b)

    def step_by_step(self, b="True"):
        """ Set step by step monitor """

        if b is True or b == "True" or b == "true" or b == "1":
            self.monitor.step_by_step = True
        else:
            self.monitor.step_by_step = False

    def register_event(self, event_type, callback):
        """ Register a callback for this event type.

            Possible events:
             - state_changed(new_states)
                new_states is the set of the new current states
             - transition_taken(transition, point)
                transition is the object representing the transition
                point is either "success" or "failure"
             - event_applied

            The event type is given by its name and the parameters passed to the
            callback is what is given in parenthesis.
        """

        if isinstance(callback, str):
            callback = self.globals[callback]

        if isinstance(event_type, str):
            event_type = events[event_type]

        self.monitor.register_event(event_type, callback)

    def get_internal_id(self):
        return self.monitor.monitor_id

    def unregister_event(self, event_type, callback):
        """ Unregister a callback for this event type.
            See also register_event.
        """

        if isinstance(callback, str):
            callback = self.globals[callback]

        if isinstance(event_type, str):
            event_type = events[event_type]

        self.monitor.unregister_event(event_type, callback)
