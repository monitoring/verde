# Usage

Here is a typical begining of debugging session with Verde:

    $ gdb an_executable
    (gdb) source /path/to/verde/gdbcommands.py
    (gdb) verde load-property file.prop
    (gdb) verde load-scenario file.scn
    (gdb) verde show-graph
    (gdb) verde run-with-program


To load a property stored in `file.prop`:

    (gdb) verde load-property file.prop

See [WRITING_PROPERTIES.md](WRITING_PROPERTIES.md) for more details on how to write properties.

To load a file that contains Python functions inside the current monitor:

    (gdb) verde load-functions file.prop

To load a scenario for the current monitor:

    (gdb) verde load-scenario file.scn

To show the animated graph of the automaton in a window:

    (gdb) verde show-graph

Note: In order to show the graph in a window, graphviz is needed, as well as the graphviz module for Python and either PyQt5, PyQt4 or PySide, and QtWebkit. In Ubuntu, Mint or other Debian-like operating systems, you can run the following to install graphviz and PyQt5:

    # Python 3
    pip3 install graphviz
    sudo apt-get install graphviz python3-pyqt5 python3-pyqt5.qtwebkit

Be sure to install graphviz for the version of Python used in gdb. To know which version of Python is used in gdb, type `pi`, and then `import sys` and `sys.version`.

To run the monitors and the program at the same time:

    (gdb) verde run-with-program

To run the current monitor without running the program at the same time:

    (gdb) verde run

To run another monitor:

    (gdb) verde run my-monitor

Note: the `run` command runs the monitor but not the program. The gdb command `run` must be used for that. For best results, you should set a breakpoint at the function [c main], run the program, run your monitors, then run your program again:

    (gdb) break main
    (gdb) verde run
    (gdb) run

It is recommended to use the command `verde run-with-program` to run the monitor and the program at the same time.

To delete a monitor (if the last parameter is omited, the current monitor will be deleted):

    (gdb) verde delete monitor-name

It is also possible to execute an action with ``verde exec``:
  - To execute a user-defined function:

        (gdb) verde exec userfun fun_name

  - To execute a action:

        (gdb) verde exec action

  - To see a list of possible actions, type `help verde exec`.

To get a list of possible verde related commands:

        (gdb) help verde

To get help on a particular command:

        (gdb) help verde command

## Writing user-defined functions

It is possible to write custom Python functions called from the property. To do this, create a new `.py` file containing the functions. For instance:

    def sink_reached(monitor):
        # Body of the function

The parameter `monitor` references the instance of the MonitorInterface class corresponding to the current monitor. From this interface, you can inspect and control the monitor.

