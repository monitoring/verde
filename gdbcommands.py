#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A module which declares monitor related gdb custom commands.
"""

CMD_PREFIX = "verde"

import os
import sys
import inspect
import re
import traceback
import gdb
import time

if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from dict import Dict

from lexer import ParseError, out_pretty_parse_error
import criucheckpoint
from monitor import Monitor
from monitorcommon import MonitorException,  StopExecution, SetCurrentState
from monitorinterface import MonitorInterface
from gdbeventmanager import EventManager

import property_parser
import scenario_parser
from context import *

class CMDError(Exception):
    """ The exception raised when an error occurs in a gdb command defined in
        this file. """

    def __init__(self, msg):
        """ This constructor takes a msg which will be passed to the constructor
            of Exception and stored in the msg field of the instance. """

        self.msg = msg
        super(CMDError, self).__init__(msg)

class CheckExecCommand(gdb.Command):
    """ Base class for the gdb commands defined in this file. """

    def __init__(self, ctx):
        """ This constructor takes a ctx dict file which is shared by all the
            gdb commands defined here and then the parameters of the default
            constructor of gdb.Command. """

        super(CheckExecCommand, self).__init__ (
            CMD_PREFIX + " " + self.__class__.cmd_name,
            gdb.COMMAND_SUPPORT,
        )

    def action(self, argv):
        try:
            tcmd = verde_ctx.timecmds
            if tcmd:
                t = time.time()

            self.act(argv)

            if tcmd:
                print(
                    "[%s] Time of command %s (#%i): %f sec" % (
                        CMD_PREFIX,
                        self.__class__.cmd_name,
                        verde_ctx.timeid,
                        time.time() - t
                    )
                )
                verde_ctx.timeid += 1

        except CMDError as e:
            print(
                "{} command {} failed.\n".format(
                    CMD_PREFIX,
                    self.__class__.cmd_name
                )
            )

        except Exception as e:
            traceback.print_exc()

    def check_used_monitor(self, name=""):
        """
            Checks whether the monitor which is used in the command calling this
            method is correct. If name is not provided, the current monitor will
            be checked. If it is correct, returns the name of the monitor.
            Otherwise, raises an error.
        """

        if name:
            i = name
            if i not in verde_ctx.monitors:
                return self.error("This monitor doesn't exist")
        else:
            i = verde_ctx.current_monitor
            if i not in verde_ctx.monitors:
                return self.error("There is no current monitor")

        return i


    def get_unused_monitor_name(self):
        idx = 1
        i = "m" + str(idx)
        while i in verde_ctx.monitors:
            idx = idx + 1
            i = "m" + str(idx)

        return i

    def new_monitor(self, i):
        if i in verde_ctx.monitors:
            raise Exception("This monitor name is already used.")

        verde_ctx.current_monitor = i

        evtMgr = EventManager()
        m = Monitor(evtMgr)
        evtMgr.set_monitor(m)

        verde_ctx.monitors[i] = m
        m.interface.set_globals(verde_ctx.globals)

    def load_property(self, prop_file):
        """ Loads the property stored in the file prop_file in the
            monitor mon """

        f = open(prop_file, "r", encoding='utf-8')
        try:
            prop = property_parser.parse(f.read())
        except ParseError as exc:
            _, _, trace = sys.exc_info()
            f.close()
            out_pretty_parse_error(trace, exc.message, sys.stderr)
            self.error("The property could not be parsed")

        f.close()
        return prop

    def load_scenario(self, mon, scenario_file):
        """ Loads the property stored in the file prop_file in the
            monitor mon """

        f = open(scenario_file, "r", encoding='utf-8')
        try:
            scenario = scenario_parser.parse(f.read())
        except ParseError as exc:
            _, _, trace = sys.exc_info()
            f.close()
            out_pretty_parse_error(trace, exc.message, sys.stderr)
            return self.error("The scenario could not be parsed")

        f.close()
        print("Monitor:", mon)
        verde_ctx.monitors[mon].set_scenario(scenario)

    def load_functions(self, mon, fun_file):
        """ Loads the user-defined functions stored in the file fun_file in the
            monitor mon
        """

        with open(fun_file, encoding='utf-8') as f:
            code = compile(f.read(), fun_file, 'exec')
            exec(code, verde_ctx.monitors[mon].prop.initialization)

    def invoke(self, arg, from_tty):
        """ Calls the action associated to the command if conditions are met and
            handles errors of the command. """

        if verde_ctx.last_failed and verde_ctx.inside_group and not (
                isinstance(self, GroupBeginCommand) or
                isinstance(self, GroupEndCommand)):
            return

        try:
            self.action(gdb.string_to_argv(arg))
        except (CMDError, MonitorException) as exc:
            sys.stderr.write(exc.msg + "\n")
            return

    def error(self, msg):
        """ handle and raises an error. """
        if verde_ctx.inside_group:
            verde_ctx.last_failed = True
        raise CMDError(msg)

    def run(self, argv):
        if len(argv) == 1:
            i = self.check_used_monitor(argv[0])
        elif len(argv) != 0:
            return self.error("This command takes one argument, or none.")
        else:
            i = self.check_used_monitor()

        verde_ctx.monitors[i].run()

class ActivateCommand(CheckExecCommand):
    """ Activates all the commands monitor related commands """

    cmd_name = "activate"

    def act(self, argv):
        if len(argv) != 0:
            return self.error("This command takes no arguments")

        ctx = verde_ctx

        MonitorNewCommand(ctx)
        LoadPropertyCommand(ctx)
        LoadUserDefinedFunctionsCommand(ctx)
        RunCheckExecCommand(ctx)
        RunMonitorAndProgramCommand(ctx)
        MonitorExecuteCommand(ctx)
        MonitorDeleteCommand(ctx)
        SetCurrentCheckExecCommand(ctx)
        GetCurrentCheckExecCommand(ctx)
        MonitorShowGraphCommand(ctx)
        GroupBeginCommand(ctx)
        GroupEndCommand(ctx)
        CheckpointCommand(ctx)
        CheckpointCriuCommand(ctx)
        CheckpointCriuRestartCommand(ctx)
        CheckpointCriuDeleteCommand(ctx)
        CheckpointCriuCleanUpCommand(ctx)
        CheckpointNativeCommand(ctx)
        CheckpointNativeRestartCommand(ctx)
        CheckpointNativeDeleteCommand(ctx)
        CheckpointRestartCommand(ctx)
        CheckpointDeleteCommand(ctx)
        CheckpointCleanUpCommand(ctx)
        LoadScenarioCommand(ctx)
        SetTimeCommand(ctx)
        NOPCommand(ctx)

class MonitorNewCommand(CheckExecCommand):
    """ Creates a monitor which will also become the current monitor.

            Usage: verde new [monitor-name]

        monitor-name is the name of the new monitor. If not given, a default
        name will be picked. The chosen name is not specified and can change
        between version.
    """

    cmd_name = "new"

    def act(self, argv):
        if len(argv) > 1:
            return self.error("This command takes one argument or none.")

        if len(argv) > 0:
            i = argv[0]
            if not i:
                return self.error("The name of a monitor cannot be empty.")
        else:
            i = self.get_unused_monitor_name()

        self.new_monitor(i)

class MonitorDeleteCommand(CheckExecCommand):
    """ Deletes a monitor.

            Usage: verde delete [monitor-name]

        monitor-name is the name of the monitor to be deleted. If not given,
        the current monitor will be deleted.
    """
    cmd_name = "delete"

    def act(self, argv):
        if len(argv) > 1:
            return self.error("This command takes one argument or none.")

        i = self.check_used_monitor(argv[0] if len(argv) > 0 else "")
        verde_ctx.monitors[i].delete()
        verde_ctx.monitors.pop(i, None)

        if verde_ctx.current_monitor not in verde_ctx.monitors:
            verde_ctx.current_monitor = ""

    def complete(self, word, text):
        res = []
        for key in verde_ctx.monitors:
            if key.startswith(text):
                res.append(key)
        return res

class SetCurrentCheckExecCommand(CheckExecCommand):
    """ Sets the current monitor.

            Usage: verde set-current monitor-name

        monitor-name is the name of the monitor which will become the current
        monitor.
    """

    cmd_name = "set-current"

    def act(self, argv):
        if len(argv) != 1:
            return self.error("This command takes exactly one argument.")

        if argv[0] not in verde_ctx.monitors:
            return self.error("This monitor doesn't exist.")

        verde_ctx.current_monitor = argv[0]

    def complete(self, word, text):
        res = []
        for key in verde_ctx.monitors:
            if key.startswith(text):
                res.append(key)
        return res

class SetTimeCommand(CheckExecCommand):
    """ Sets the current monitor.

            Usage: verde set-time-commands (on | off)

        Show the execution time of commands
    """

    cmd_name = "set-time-commands"

    def act(self, argv):
        if len(argv) != 1:
            return self.error("This command takes exactly one argument.")

        verde_ctx.timecmds = (argv[0] == "on")


class MonitorExecuteCommand(CheckExecCommand):
    """ Executes an action in the current monitor.

            Usage: verde exec ($predef_function | userfun $user_fun | help [$predef-function]) [$params]

        - $predef_function is a method of the MonitorInterface class.
        - $user_fun is a function that was defined by the user, usable in the
          property.
        - $params are the parameters the designed function takes."""

    cmd_name = "exec"

    blacklisted_actions = {
        "get_env_keys",
        "get_env_dict",
        "set_env_dict",
        "set_current_states",
        "stop_execution",
        "debugger_shell",
        "set_globals"
    }

    def act(self, argv):
        if len(argv) == 0:
            return self.error("This command takes at least one argument.")

        cmd = argv[0]

        monitor = verde_ctx.monitors[self.check_used_monitor()]

        try:
            if cmd == "userfun":
                if len(argv) != 2:
                    return self.error(
                        "The userfun parameter of this command takes exactly " +
                        "one argument: the name of the user function"
                    )

                monitor.actions[argv[1]](monitor.interface)
            elif cmd == "help":
                if len(argv) == 1:
                    print(
                        "Here is the help of the class containing the " +
                        "commands you can call:\n"
                    )

                    help(MonitorInterface)

                    print(
                        "Some commands are not accessible from monitor exec: " +
                        (", ".join(map(str, self.blacklisted_actions))) + "."
                    )

                elif len(argv) == 2:
                    predefs = inspect.getmembers(
                        MonitorInterface,
                        predicate=inspect.ismethod
                    )

                    helped = False
                    for key in predefs:
                        if key[0] == argv[1]:
                            helped = True
                            help(key[1])
                            break

                    if not helped:
                        print("No such predefined function")
                else:
                    return self.error(
                        "The help parameter of this command takes only zero " +
                        "or one argument (the name of the predefined function)"
                    )
            elif cmd == "get_states":
                print(", ".join([s.name for s in monitor.interface.get_states()]))
            elif cmd == "get_current_states":
                print(", ".join([s.name for s in monitor.interface.get_current_states()]))
            else:
                res = getattr(monitor.interface, argv[0])(*argv[1:])

                if res != None:
                    print(res)
        except StopExecution:
            pass
        except SetCurrentState as exc:
            monitor.set_current_state(exc.state)

    def complete(self, word, text):
        res = []

        if "userfun".startswith(text):
            res.append("userfun")

        if "help".startswith(text):
            res.append("help")

        if text.startswith("userfun"):
            monitor = verde_ctx.monitors[self.check_used_monitor()]
            for f in monitor.actions.keys():
                if (f.startswith(word)
                    and f != "stop_execution"
                    and f != "debugger_shell"
                    and f != ""
                    and f[0] != "_"):
                    res.append(f)

        else:
            predefs = inspect.getmembers(
                MonitorInterface,
                predicate=lambda x: inspect.isfunction(x) or inspect.ismethod(x)
            )

            for key in predefs:
                if (key[0].startswith(word)
                    and key[0][0] != "_"
                    and key[0] not in self.blacklisted_actions):
                    res.append(key[0])

        return res

class GetCurrentCheckExecCommand(CheckExecCommand):
    """ Prints the name of the current monitor.

        Usage: verde get-current
    """

    cmd_name = "get-current"

    def act(self, argv):
        if len(argv) > 0:
            return self.error("This command takes no arguments.")

        i = verde_ctx.current_monitor
        print(i if i else "(no current monitor)")


class CheckpointNativeCommand(CheckExecCommand):
    """ set a checkpoint for the program and each managed monitor using GDB's native checkpoints.

        Usage: verde checkpoint-native
    """

    cmd_name = "checkpoint-native"

    checkpoint_re = re.compile(r"^checkpoint ([0-9]+)")

    def act(self, argv):
        checkpoint_str = gdb.execute("checkpoint", to_string=True)

        if checkpoint_str:
            # Patched gdb, faster
            checkpoint_match = self.checkpoint_re.match(checkpoint_str)
            if checkpoint_match:
                gdb_checkpoint_id = checkpoint_match.group(1)
            else:
                raise Exception("Could not get the checkpoint id")
        else:
            # Unpatched gdb, slower but just works.
            raise Exception("FIXME Could not get the checkpoint id, verde is not ready for unpatched gdb")

        checkpoint_id = len(verde_ctx.native_checkpoints) + 1 # +1 to avoid 0
        verde_ctx.native_checkpoints.append(gdb_checkpoint_id)

        for name, m in verde_ctx.monitors.items():
            m.checkpoint(checkpoint_id)
            m.restore_instrumentation()

        print("Checkpoint id: " + str(checkpoint_id))

class CheckpointCriuCommand(CheckExecCommand):
    """ set a checkpoint for the program and each managed monitor using GDB's native checkpoints.

        Usage: verde checkpoint-criu
    """

    cmd_name = "checkpoint-criu"

    def act(self, argv):
        print("Checkpoint id: " + str(criu_checkpoint()))


class CheckpointCommand(CheckpointCriuCommand):
    """ set a checkpoint for the program and each managed monitor using the prefered method.

        Usage: verde checkpoint
    """

    cmd_name = "checkpoint"

class CheckpointNativeRestartCommand(CheckExecCommand):
    """ Restores a checkpoint.

        Usage: verde checkpoint-restart <checkpoint-id>
    """

    cmd_name = "checkpoint-native-restart"

    def act(self, argv):
        if len(argv) != 1:
            raise Exception("Expecting one parameter: the identifier of the checkpoint to restart")

        checkpoint_id = int(argv[0])

        try:
            gdb_checkpoint_id = verde_ctx.native_checkpoints[checkpoint_id - 1]
            # -1 because checkpoint are offset by 1
        except:
            raise Exception("Sorry, I don't have this checkpoint.")

        for name, m in verde_ctx.monitors.items():
            m.restore_checkpoint(checkpoint_id)

            if not m.quiet:
                m.print_current_state()

        gdb.execute("restart " + str(gdb_checkpoint_id))

class CheckpointNativeDeleteCommand(CheckExecCommand):
    """ Restores a checkpoint.

        Usage: verde checkpoint-restart <checkpoint-id>
    """

    cmd_name = "checkpoint-native-delete"

    def act(self, argv):
        if len(argv) != 1:
            raise Exception("Expecting one parameter: the identifier of the checkpoint to delete")

        checkpoint_id = int(argv[0])
        gdb.execute("delete checkpoint " + str(gdb_checkpoint_id))


class CheckpointCriuRestartCommand(CheckExecCommand):
    """ Restores a checkpoint.

        Usage: verde checkpoint-restart <checkpoint-id>
    """

    cmd_name = "checkpoint-criu-restart"

    def act(self, argv):
        if len(argv) != 1:
            raise Exception("Expecting one parameter: the identifier of the checkpoint to restart")

        checkpoint_id = int(argv[0])

        criu_restore_checkpoint(checkpoint_id)

        for name, m in verde_ctx.monitors.items():
            if not m.quiet:
                m.print_current_state()

class CheckpointCriuDeleteCommand(CheckExecCommand):
    """ Delete a checkpoint.

        Usage: verde checkpoint-delete <checkpoint-id>
    """

    cmd_name = "checkpoint-criu-delete"

    def act(self, argv):
        if len(argv) != 1:
            raise Exception("Expecting one parameter: the identifier of the checkpoint to delete")

        checkpoint_id = int(argv[0])

        criu_drop_checkpoint(checkpoint_id)

class CheckpointCriuCleanUpCommand(CheckExecCommand):
    """ Delete all checkpoints.

        Usage: verde checkpoint-clean
    """

    cmd_name = "checkpoint-criu-cleanup"

    def act(self, argv):
        criucheckpoint.criu_cleanup()

class CheckpointRestartCommand(CheckpointCriuRestartCommand):
    """ Restores a checkpoint.

        Usage: verde checkpoint-restart <checkpoint-id>
    """

    cmd_name = "checkpoint-restart"

class CheckpointDeleteCommand(CheckpointCriuDeleteCommand):
    """ Deletes a checkpoint.

        Usage: verde checkpoint-restart <checkpoint-id>
    """

    cmd_name = "checkpoint-delete"

class CheckpointCleanUpCommand(CheckpointCriuCleanUpCommand):
    """ Deletes all checkpoints.

        Usage: verde checkpoint-clean
    """

    cmd_name = "checkpoint-cleanup"

    def act(self, argv):
        criucheckpoint.criu_cleanup()

class LoadPropertyCommand(CheckExecCommand):
    """ Loads a property file and possibly a function file in the given monitor.
        If no monitor is given, use the current monitor if it exists and it
        hasn't a property loaded yet. Otherwize, create a new monitor and use it.

            Usage: verde load-property [monitor id] file.prop [fun.py]

        file.prop is a file containing a property, following the specification
        described in the prop.spec.txt
    """

    cmd_name = "load-property"

    def act(self, argv):
        mon = None
        fun_file = None

        if len(argv) == 1:
            prop_file = argv[0]
        elif len(argv) == 2:
            if argv[0] in verde_ctx.monitors:
                mon = argv[0]
                prop_file = argv[1]
            else:
                prop_file = argv[0]
                fun_file  = argv[1]
        elif len(argv) == 3:
            mon       = argv[0]
            prop_file = argv[1]
            fun_file  = argv[2]
        else:
            return self.error("This command takes one to three arguments")

        if mon:
            if mon not in verde_ctx.monitors:
                self.new_monitor(mon)
        else:
            c = verde_ctx.current_monitor
            if c and not verde_ctx.monitors[c].prop:
                mon = c
            else:
                mon = self.get_unused_monitor_name()
                self.new_monitor(mon)
                verde_ctx.current_monitor = mon

        verde_ctx.monitors[mon].set_property(self.load_property(prop_file))

        if fun_file:
            self.load_functions(mon, fun_file)

    def complete(self, word, text):
        return gdb.COMPLETE_FILE

class LoadScenarioCommand(CheckExecCommand):
    """ Loads a property file and possibly a function file in the given monitor.
        If no monitor is given, use the current monitor if it exists and it
        hasn't a property loaded yet. Otherwize, create a new monitor and use it.

            Usage: verde load-property [monitor id] file.prop [fun.py]

        file.prop is a file containing a property, following the specification
        described in the prop.spec.txt
    """

    cmd_name = "load-scenario"

    def act(self, argv):
        if len(argv) == 1:
            scenario_file = argv[0]
        else:
            return self.error("This command takes one argument")

        mon = verde_ctx.current_monitor
        if not mon :
            return self.error("No current monitor. Please load a property before loading a scenario")

        self.load_scenario(mon, scenario_file)

    def complete(self, word, text):
        return gdb.COMPLETE_FILE

class LoadUserDefinedFunctionsCommand(CheckExecCommand):
    """ Loads a user defined functions file.

            Usage: verde load-functions file.py

        file.py is a python file containing the user-defined functions.
    """

    cmd_name = "load-functions"

    def act(self, argv):
        if len(argv) != 1:
            return self.error("This command takes exactly one argument.")

        self.load_functions(self.check_used_monitor(), argv[0])

    def complete(self, word, text):
        return gdb.COMPLETE_FILE


class RunCheckExecCommand(CheckExecCommand):
    """ Runs the monitor.

            Usage: verde run [monitor-name]

        monitor-name is the monitor to run. If not given, the current monitor
        will be chosen.
    """

    cmd_name = "run"

    def act(self, argv):
        self.run(argv)

    def complete(self, word, text):
        res = []
        for key in verde_ctx.monitors:
            if key.startswith(text):
                res.append(key)
        return res

class RunMonitorAndProgramCommand(CheckExecCommand):
    """ Running the monitor and the program at the same time.

            Usage: verde run-with-program
    """

    cmd_name = "run-with-program"

    def act(self, argv):
        try:
            stdinredirindex = argv.index("<")
            stdinstr = " < " + argv[stdinredirindex + 1]
            del argv[stdinredirindex:stdinredirindex + 2]
        except (ValueError, IndexError):
            stdinstr = ""

        try:
            gdb.newest_frame()
        except:
            gdb.execute("start " + (argv[0] if len(argv) == 1 else "") + stdinstr)

        for i in verde_ctx.monitors:
            self.run([i])

        gdb.execute("continue")

    def complete(self, word, text):
        res = []
        for key in verde_ctx.monitors:
            if key.startswith(text):
                res.append(key)
        return res

class MonitorShowGraphCommand(CheckExecCommand):
    """ Shows the graph of the automaton in a window and animates it as the
        the execution takes place.

            Usage: verde show-graph [monitor-name]

        monitor-name is the name of the monitor. If not given, the current
        monitor will be used.
    """

    cmd_name = "show-graph"

    def act(self, argv):
        self.dont_repeat()
        if len(argv) > 1:
            return self.error("This command takes at most one argument.")

        if len(argv) == 1:
            i = argv[0] if len(argv) > 0 else ""
        else:
            i = verde_ctx.current_monitor

        from graphdisplay import GraphDisplay
        GraphDisplay(verde_ctx.monitors[self.check_used_monitor(i)].interface).start()


class GroupBeginCommand(CheckExecCommand):
    """ Begins a group of commands. If a command fails inside a group, the other
        commands will be ignored.

            Usage: verde cmd-group-begin
    """

    cmd_name = "cmd-group-begin"

    def act(self, argv):
        if len(argv) != 0:
            return self.error("This command doesn't take any arguments.")

        verde_ctx.last_failed  = False
        verde_ctx.inside_group = True

class GroupEndCommand(CheckExecCommand):
    """ Ends a group of command.

            Usage: verde command-group-end [--quit-on-failure]

        --quit-on-failure: if something went wrong in the command group, the
                           monitor will make gdb quit.
    """

    cmd_name = "cmd-group-end"

    def act(self, argv):
        if len(argv) != 0:
            if argv[0] == "--quit-on-failure":
                if verde_ctx.last_failed:
                    gdb.execute("quit")
            else:
                return self.error("This command doesn't take any arguments.")

        verde_ctx.inside_group = False

    def complete(self, word, text):
        if  "--quit-on-failure".startswith(text):
            return ["--quit-on-failure"]
        return []

class NOPCommand(CheckExecCommand):
    """ No OPeration command.

            Usage: verde nop

        Useful to measure the overhead induced by calling a command.
    """

    cmd_name = "nop"

    def act(self, argv):
        pass

gdb.Command(CMD_PREFIX, gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE, True)

(ActivateCommand(verde_ctx)).action({})

gdb.execute("source " + os.path.dirname(os.path.realpath(__file__)) + "/script.gdb")
# WARNING: Verde should be installed in a space-free path
