#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A generalistic lexer to help parsing simple languages.
"""

import re
import traceback

class ParseError(Exception):
    """ This exception should be raised whenever a parse error happens """

    def __init__(self, m):
        """ m is a string bringing an explanation or precision of the error """
        self.message = m
        Exception.__init__(self)

    def __str__(self):
        return repr(self.message)


class EOF(Exception):
    """ This exception should be raised whenever the end of an input is reached
        though the parser tried to read further characters """

    def __init__(self):
        Exception.__init__(self)


class Lexer(object):
    """ This is the main class of the module """

    def __init__(self, string):
        self.cur_line = 1
        self.cur_char = 1
        self.pos = 0
        self.slen = len(string)
        self.input = string

    def advance_cur_pos(self, length):
        """ Sets the current position to current position + length """
        for _ in range(0, length):
            if self.pos >= self.slen:
                return

            if self.input[self.pos] == '\n':
                self.cur_line += 1
                self.cur_char = 0

            self.pos += 1
            self.cur_char += 1

    def getchar(self, advance=True):
        """ Returns the current character and goes forward one character """

        if self.pos >= self.slen:
            raise EOF

        cur_char = self.input[self.pos]

        if advance:
            self.advance_cur_pos(1)

        return cur_char

    def ungetchar(self):
        """ Goes back one character and returns the new current caracter"""
        self.pos -= 1

        if self.input[self.pos] == '\n':
            self.cur_line -= 1
            i = self.pos

            while i >= 0 and self.input[i] != '\n':
                self.cur_char += 1
                i += 1

        return self.input[self.pos]

    def eat_up_to_excluded(self, string):
        """ Finds string from the current pos, returns what is between the
            two and updates the current position (before the position of string
            in the lexer) """

        length = len(string)
        old_pos = self.pos

        while self.pos < self.slen:
            if self.input[self.pos] == string[0]:
                i = 0

                while i < length and self.input[self.pos + i] == self.input[i]:
                    i += 1

                if i == length:
                    return self.input[old_pos:self.pos]
            self.advance_cur_pos(1)

        self.raise_parse_error(
            "Unexpected end of file, expecting '{}'".format(string)
        )

    def eat(self, string, error=""):
        """ Reads string in the lexer and updates the current position """

        length = len(string)

        while self.pos < self.slen and self.input[self.pos].strip() == '':
            if self.input[self.pos] == string[0]:
                i = 0
                while self.input[self.pos + i] == string[i]:
                    i += 1
                    if i == length:
                        self.advance_cur_pos(length)
                        return
            self.advance_cur_pos(1)

        for i in range(0, length):
            if self.pos + i >= self.slen:
                if error:
                    self.raise_parse_error(error)
                else:
                    raise EOF

            if self.input[self.pos + i] != string[i]:
                self.raise_parse_error("Expected '" + string + "'")

        self.advance_cur_pos(length)

    def skip_white(self):
        """ Skips blank characters from the lexer """
        try:
            cur_char = self.getchar()
        except EOF:
            return

        while cur_char.strip() == '':
            try:
                cur_char = self.getchar()
            except EOF:
                return

        self.ungetchar()

    def pos_str(self):
        """ Builds the string which describes current lexer position """
        return (
            " on line " + str(self.cur_line) +
            ", character " + str(self.cur_char) + "."
        )

    def raise_parse_error(self, reason):
        """ raises a ParseError with the reason and the current position """
        raise ParseError(reason + self.pos_str())

    def try_eat(self, string):
        """ Tries to eat the string from the lexer. Returns True on success """
        try:
            self.eat(string)
            return True
        except (EOF, ParseError):
            return False

    def eat_regex(self, regex, skip_white=True):
        """ Tries to eat a string matching the regex from the lexer.
            Returns True on success """

        if skip_white:
            self.skip_white()

        m = re.search("^" + regex, self.input[self.pos:])

        if m is None:
            self.raise_parse_error("Expected '" + regex + "'")

        self.advance_cur_pos(len(m.group(0)))

        return m

    def try_eat_regex(self, regex, skip_white=True):
        try:
            m = self.eat_regex(regex, skip_white)
            return m
        except (EOF, ParseError):
            return None

    def expect_end(self):
        """ Checks whether we are at the end of input """
        if self.pos < self.slen:
            self.raise_parse_error("Expected end of input")

def out_pretty_parse_error(trace, msg, out):
    """ pretty-prints a ParseError from its traceback and its message. """

    calltrace = traceback.extract_tb(trace)
    already_printed = set()

    out.write(
        "\033[91mParse error:\033[0;1m " + msg + "\033[93m\n\npath: "
    )

    for call in calltrace:
        fun = call[2]

        if fun.startswith("parse_remaining"):
            fun = fun[len("parse_remaining"):]
        elif fun.startswith("parse_maybe") or fun.endswith("_content"):
            continue
        elif fun.startswith("parse"):
            fun = fun[len("parse"):]
        else:
            continue

        if fun not in already_printed:
            if already_printed:
                out.write(" > ")

            out.write(fun.replace("_", " "))
            already_printed.add(fun)

    out.write("\033[0;0m\n")
