#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
    A module which displays and animates the graph of a monitor's automaton.
"""

import subprocess
import graphviz as gv
import sys
import os
import re

def id_from_transition_point(trans, point, dest_state):
    return trans.begin_state.name + " " + trans.event.name + ":" + point + " " + dest_state.name


class GraphDisplay(object):
    def send(self, msg):
        try:
            self.p.stdin.write((msg + "\n").encode("utf-8"))
            self.p.stdin.flush()
        except: # if the window is closed, we receive a BrokenPipeError exception.
            pass

    def event_applied(self, runtime_event):
        self.update_current_states(False);

    def update_current_states(self, clear_trans=True):
        if clear_trans:
            self.send("clearTransitions()")

        current_states = [state.name for state in self.mi.get_current_states()]
        self.send("setCurrent(new Set(" + str(current_states) + "), " + ("true" if clear_trans else "false") + ")")

    def transition_taken(self, trans, point, state):
        self.send("takeTransition('" + id_from_transition_point(trans, point, state) + "')")

    def __init__(self, mi):
        super(self.__class__, self).__init__()
        self.mi = mi
        g = self.graph = gv.Digraph("Monitor's automaton", format='svg')

        mi.register_event("event_applied",    self.event_applied)
        mi.register_event("transition_taken", self.transition_taken)
        mi.register_event("checkpoint_restart", self.update_current_states)

        states = mi.get_states()
        self.non_accepting = []

        for state in states:
            if not state.accepting:
                self.non_accepting.append(state.name)

            g.node(state.name, id=state.name)

            for trans in state.transitions:
                for point in ("success", "failure"):
                    p = getattr(trans, point)
                    if p:
                        g.edge(
                            state.name,
                            p.state,
                            label=
                                trans.event.name + ":" +
                                point[0],
                            id=id_from_transition_point(trans, point, mi.monitor.states_by_name[p.state])
                        )


        self.svg = re.sub(
            r"<![^>]+>",
            "",
            re.sub(
                r"<\?[^>]+>",
                "",
                g.pipe(format="svg").decode(),
                flags=re.MULTILINE
            ),
            flags=re.MULTILINE
        )

    def start(self):
        self.p = subprocess.Popen([
            "python3",
            os.path.dirname(os.path.realpath(__file__)) + "/graphdisplayer.py"
        ], stdin=subprocess.PIPE)

        # Using sys.executable is broken in gdb.
        #(
            #sys.executable
            #if os.path.isfile(sys.executable)
            #else "python" + str(sys.version_info.major)
        #),

        self.send(str(self.mi.get_internal_id()) + "\n" + str(self.non_accepting) + "\n" + self.svg + "\nEND")
