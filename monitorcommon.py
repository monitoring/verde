#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2014-2017 Université Grenoble Alpes
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


"""
    A module which contains object shared between the monitor and its interface.
"""

from dict import Dict

class StopExecution(Exception):
    """ This class is used in the monitor whenever the execution of the
        monitored program must be stopped. """
    def __init__(self, msg):
        super(StopExecution, self).__init__(msg)


class MonitorException(Exception):
    def __init__(self, msg):
        self.msg = msg
        super(MonitorException, self).__init__(msg)


class SetCurrentState(Exception):
    """ This class is used in the monitor whenever the current states must be
        changed while the monitor is taking transitions. """

    def __init__(self, state, stop=False):
        """ This constructor takes a set of the new current states and a
            parameter deciding whether the execution of the program must be
            stopped. """

        super(SetCurrentState, self).__init__("")
        self.state = state
        self.stop   = stop

events = Dict({
    "state_changed":    1,
    "transition_taken": 2,
    "event_applied":    4,
    "event_applying":   8,
    "checkpoint_restart": 16
})
