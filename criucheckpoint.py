#!/usr/bin/env python3

import subprocess
import os
import gdb
import signal
import shutil
import breakpoint

TMP_DIR  = "/tmp/verde-checkpoints"

CHECKPOINTS_DIRNAME = "checkpoints"

evts = {
    "beforecp": [], "aftercp": [], "beforerestore": [], "afterrestore": []
}

# Workaround used when running from a docker container on Mac
disable_track_mem = 'VERDE_DISABLE_CRIU_TRACK_MEM' in os.environ

last_cp = 0

def cp_path(pid, cid):
    global TMP_DIR
    return TMP_DIR + "/" + CHECKPOINTS_DIRNAME + "/{}/{}/".format(cid, pid)

def criu_path(path):
    return path + "criu"

def fire_evt(evtname, cid):
    for f in evts[evtname]:
        f(cid)

def register_evt(evtname, f):
    evts[evtname].append(f)

def restore_execution_after_cont(arg=None):
    gdb.events.stop.disconnect(restore_execution_after_cont)
    breakpoint.cont()

def after_criu(pid, stop=False):
    gdb.execute("attach " + str(pid))

    #gdb.execute("source {}".format(bppath))
    if not stop:
        gdb.events.stop.connect(restore_execution_after_cont)

    os.kill(pid, signal.SIGCONT)

def criu_delete(cid):
    (pc, pid, block_workaround) = criu_checkpoint.info_by_cid[cid]
    path = cp_path(pid, cid)

    if not os.path.isdir(path):
        raise Exception("Sorry, I don't have checkpoint #{}.".format(cid))

    shutil.rmtree(path)

def criu_cleanup():
    global TMP_DIR
    cps = TMP_DIR + "/" + CHECKPOINTS_DIRNAME
    pid = gdb.selected_inferior().pid

    try:
        shutil.rmtree(cps + "/{}".format(pid))
        os.rmdir(cps)
        os.rmdir(TMP_DIR)
    except (OSError, FileNotFoundError): # we remove directories only if they are empty
        pass

def criu_restore(cid, stop=False):
    fire_evt("beforerestore", cid)

    (pc, pid, block_workaround) = criu_checkpoint.info_by_cid[cid]

    path = cp_path(pid, cid)

    if not os.path.isdir(path):
        raise Exception("Sorry, I don't have checkpoint #{}.".format(cid))

    dirimg = criu_path(path)

    # FIXME: is this STOP, detach, KILL dance necessary?
    # We want the debugger not to go mad so we might not want to kill
    # the process before detaching. If we detach without stopping, we
    # risk that the process continues its life. We want to get rid of the
    # process so we kill it.

    # Actually, STOP seems to prevent normal operation when waitpid is called.
    # Why?

    try:
        os.kill(pid, signal.SIGSTOP)
        gdb.execute("detach")
        os.kill(pid, signal.SIGKILL)

        try:
            os.wait()
        except ChildProcessError:
            pass

    except ProcessLookupError: # ok, probably the process has quit.
        pass


    # FIXME: is SIGKILL the best way to stop the current process?
    # With SIGTERM, we risk the application to do something before
    # quitting

    criu_cmd = [
        "criu",
        "restore",
        "-d",
        #"-v4", # for debugging
        "--images-dir",
        dirimg,
        "--shell-job",
        "--track-mem"
    ]

    if disable_track_mem:
        criu_cmd.pop()

    if subprocess.call(criu_cmd) != 0:
        raise Exception("Restoring CRIU checkpoint failed")

    after_criu(pid, stop=stop)
    last_cp = cid
    fire_evt("afterrestore", cid)

    return (pc, block_workaround)

def criu_checkpoint(block_workaround=False):
    criu_checkpoint.cid += 1
    cid = criu_checkpoint.cid

    fire_evt("beforecp", cid)

    pid = gdb.selected_inferior().pid


    pc = gdb.newest_frame().pc()
    criu_checkpoint.info_by_cid.append( (pc, pid, block_workaround) )

    path = cp_path(pid, cid)
    dirimg = criu_path(path)

    os.makedirs(dirimg)

    os.kill(pid, signal.SIGSTOP)

    gdb.execute("detach")

    criu_cmd = [
        "criu",
        "dump",
        "-t",
        str(pid),
        "--images-dir",
        dirimg,
        "--leave-running",
        "--shell-job",
        "--track-mem"
    ]

    if disable_track_mem:
        criu_cmd.pop()

    last_path = cp_path(pid, last_cp)
    if os.path.isdir(last_cp):
        criu_cmd.append("--prev-images-dir")
        criu_cmd.append(last_path)


    # FIXME: replace by calls to the C API of criu
    if subprocess.call(criu_cmd) != 0:
        raise Exception("CRIU Checkpoint failed")

    after_criu(pid)

    fire_evt("aftercp", cid)

    return cid

criu_checkpoint.cid = 0
criu_checkpoint.info_by_cid = [None]
